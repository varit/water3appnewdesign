webpackJsonp([5],{

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__topup__ = __webpack_require__(362);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopupPageModule", function() { return TopupPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TopupPageModule = (function () {
    function TopupPageModule() {
    }
    return TopupPageModule;
}());
TopupPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__topup__["a" /* TopupPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__topup__["a" /* TopupPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__topup__["a" /* TopupPage */]
        ]
    })
], TopupPageModule);

//# sourceMappingURL=topup.module.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TopupPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TopupPage = (function () {
    function TopupPage(navCtrl, platform, alertCtrl, loadingCtrl, navParams, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.bottles = [];
        if (this.api.checkSession()) {
            var loading_1 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_1.present();
            this.api.getAccountInfo().subscribe(function (res) {
                //var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                if (body.gender == null || body.birth_date == null) {
                    // localStorage.setItem('alert','true');
                    // localStorage.setItem('alertText','<a (click)="navCtrl.setRoot(/"AccountPage/")"');
                    if (_this.platform.is('ios')) {
                        console.log("iOS alert");
                        _this.alertbox = "It seems like your profile information is missing. Complete your profile for free credits and more offers! Click here!";
                    }
                    else {
                        var alert_1 = _this.alertCtrl.create({
                            title: "Please update your profle",
                            subTitle: "Your profile is missing some information. Please complete your profile to proceed.",
                            buttons: ['OK']
                        });
                        alert_1.present(prompt);
                        _this.navCtrl.setRoot('AccountPage');
                    }
                }
                _this.balance = body.regular_credit_balance + body.subscription_credit_balance;
                _this.balance = _this.balance.toFixed(2);
            }, function (err) {
            });
            this.api.getBottleList().subscribe(function (res) {
                //var status = res.status;
                var body = res.json();
                //console.log("Response : "+status+" BODY "+JSON.stringify(body));
                for (var _i = 0, _a = body.data; _i < _a.length; _i++) {
                    var bottle = _a[_i];
                    var newbottle = {
                        bottle_name: bottle.bottle_name,
                        bottle_no: bottle.bottle_no
                    };
                    _this.bottles.push(newbottle);
                }
                _this.isLoaded = true;
                loading_1.dismiss();
            }, function (err) {
            });
        }
    }
    TopupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TopupPage');
    };
    TopupPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    TopupPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    TopupPage.prototype.toNewBottle = function () {
        this.navCtrl.setRoot('BottleCreatePage');
    };
    TopupPage.prototype.slidePrev = function () {
        this.slides.slidePrev();
    };
    TopupPage.prototype.slideNext = function () {
        this.slides.slideNext();
    };
    TopupPage.prototype.slidePosition = function () {
        if (this.slides.isBeginning()) {
            return 1;
        }
        if (this.slides.isEnd()) {
            return 99;
        }
        return 0;
    };
    TopupPage.prototype.toAmount = function () {
        this.navCtrl.setRoot('AmountPage');
    };
    return TopupPage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */])
], TopupPage.prototype, "slides", void 0);
TopupPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-topup',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/topup/topup.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a3eb9967b791a0001977dff" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>topup1</title>\n  <meta content="topup1" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="assets/images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div class="navbar">\n    <div class="w-container">\n      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar home w-nav">\n        <div class="nav-container"><a href="#" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" width="15" class="image-11"><div class="text-cart">(2)</div></a>\n          <div class="menu-button w-nav-button">\n            <div class="w-icon-nav-menu"></div>\n          </div><a href="#" class="brand-3 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n          <nav role="navigation" class="nav-menu w-nav-menu">\n            <a href="#" class="navlink w-inline-block">\n              <div>Shop</div>\n            </a>\n            <a href="#" class="navlink w-inline-block">\n              <div>Find a kiosk</div>\n            </a>\n            <a href="#" class="navlink w-inline-block">\n              <div>Why Water3</div>\n            </a>\n            <a href="#" class="navlink w-inline-block">\n              <div>FAQ</div>\n            </a>\n            <a href="#" class="navlink w-inline-block">\n              <div>Contact</div>\n            </a><a href="#" class="nav-button register w-button">Register</a><a href="#" class="nav-button login w-button">Login</a><a href="#" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" width="15" class="image-11"><div class="text-cart">(2)</div></a></nav>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ion-content>\n  <div class="topup-section">\n    <div class="topup-head">\n      <div class="topup-row w-row">\n        <div class="column-3 w-col w-col-4 w-col-tiny-tiny-stack"></div>\n        <div class="w-col w-col-4 w-col-tiny-tiny-stack">\n          <div class="step-box">\n            <div class="div-block-35 step-1">\n              <div class="text-block-14 _1">1</div>\n            </div>\n            <div class="line-step"></div>\n            <div class="div-block-35 step2">\n              <div class="text-block-14 _2">2</div>\n            </div>\n            <div class="line-step"></div>\n            <div class="div-block-35 step3">\n              <div class="text-block-14 _3">3</div>\n            </div>\n          </div>\n        </div>\n        <div class="column-4 w-col w-col-4 w-col-tiny-tiny-stack">\n          <div class="canel-box"><a href="#" class="link-3">CANCEL</a></div>\n        </div>\n      </div>\n    </div>\n    <div class="topup-container w-container">\n      <div class="topup-form-1">\n        <h3 class="h3-topup">Select Top Up</h3>\n        <div><a href="#" class="top-up-price-btn normal w-button">$ 5</a><a href="#" class="top-up-price-btn normal w-button">$ 10</a><a href="#" class="top-up-price-btn normal w-button">$ 15</a>\n          <div class="div-block-22"><img src="assets/images/line1.png" width="186"></div>\n        </div>\n        <div class="text-block-15">Or enter a different amount</div>\n        <div class="w-form">\n          <form id="email-form" name="email-form" data-name="Email Form"><label for="Amount">Enter Amount</label><input type="text" class="top-up-filed-form w-input" maxlength="256" name="Amount" data-name="Amount" placeholder="Enter your Amount" id="Amount"><label for="Coupon-code">Add Coupon code</label><input type="text" class="top-up-filed-form w-input" maxlength="256" name="Coupon-code" data-name="Coupon code" placeholder="Enter coupon code" id="Coupon-code"><input type="submit" value="Back" data-wait="Please wait..." class="top-up-back-btn w-button"><a href="#" class="top-up-next-btn w-button">Next</a></form>\n          <div class="w-form-done">\n            <div>Thank you! Your submission has been received!</div>\n          </div>\n          <div class="w-form-fail">\n            <div>Oops! Something went wrong while submitting the form.</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class="footer-section" style="height: 250px;">\n    <div class="w-container">\n      <div class="row w-row">\n        <div class="w-col w-col-3">\n          <div><img src="assets/images/Logo.png" width="121.5" class="image-3">\n            <div class="text-copy-right _1">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n        <div class="w-col w-col-6">\n          <div class="w-row">\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-18">\n                <a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="footer-link">Home</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Shop</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Find a kiosk</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Why Water3</a>\n                <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="footer-link">FAQ</a>\n                <a (click)="navCtrl.setRoot(\'ContactPage\')" class="footer-link">Contact</a></div>\n            </div>\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-19">\n                <a href="#" class="footer-link">Shipping</a>\n                <a (click)="navCtrl.setRoot(\'TermsPage\')" class="footer-link">Term of Services</a>\n                <a (click)="navCtrl.setRoot(\'PrivacyPage\')" class="footer-link">Privacy Policy</a></div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-21"><a href="#" class="social-link w-inline-block"><img src="assets/images/fb-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/ig-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/twitter-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/linkedin-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/youtube-icon.png" class="social-icon"></a><a href="#" class="footer-email">info@water3.com.au</a>\n            <div class="text-block-5">+61 410 000 000</div>\n            <div class="footer-address">12 New Street Name, Suburb<br>QLD 4000, Australia</div>\n            <div class="text-copy-right _2">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content></body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/topup/topup.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], TopupPage);

//# sourceMappingURL=topup.js.map

/***/ })

});
//# sourceMappingURL=5.main.js.map