webpackJsonp([10],{

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__address__ = __webpack_require__(357);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressPageModule", function() { return AddressPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddressPageModule = (function () {
    function AddressPageModule() {
    }
    return AddressPageModule;
}());
AddressPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__address__["a" /* AddressPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__address__["a" /* AddressPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__address__["a" /* AddressPage */]
        ]
    })
], AddressPageModule);

//# sourceMappingURL=address.module.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddressPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AddressPage = (function () {
    function AddressPage(navCtrl, alertCtrl, loadingCtrl, navParams, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        if (this.api.checkSession()) {
            if (localStorage.getItem("isLoggedin") == "1")
                this.isLoggedin = true;
            this.itemAmount = localStorage.getItem("itemAmount");
            if (this.itemAmount == null || this.itemAmount <= 0)
                this.itemAmount = 0;
            else
                this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
            var loading_1 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_1.present();
            this.api.getAccountInfo().subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                //console.log("Response : "+status+" BODY "+JSON.stringify(body));
                _this.accountInfo = body;
                var oldAddress = {
                    address_1: _this.accountInfo.address_1,
                    address_2: _this.accountInfo.address_2,
                    city: _this.accountInfo.city,
                    state_id: _this.accountInfo.state_id,
                    country_id: _this.accountInfo.country_id,
                    postcode: _this.accountInfo.postcode
                };
                _this.sameAccountInfo = oldAddress;
                _this.api.getCountry().subscribe(function (res) {
                    var status = res.status;
                    var body = res.json();
                    //console.log("Response : "+status+" BODY "+JSON.stringify(body));
                    _this.country = body.countries;
                    _this.api.getState(_this.accountInfo.country_id).subscribe(function (res) {
                        var status = res.status;
                        var body = res.json();
                        //console.log("Response : "+status+" BODY "+JSON.stringify(body));
                        _this.state = body.states;
                        loading_1.dismiss();
                        _this.isLoaded = true;
                    }, function (err) {
                    });
                }, function (err) {
                });
            }, function (err) {
            });
        }
    }
    AddressPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TopupPage - Address');
    };
    AddressPage.prototype.postAddress = function () {
        var _this = this;
        if (this.accountInfo.address_1 == "" || this.accountInfo.address_1 == null || this.accountInfo.city == "" || this.accountInfo.city == null || this.accountInfo.postcode == "" || this.accountInfo.postcode == null) {
            var alert_1 = this.alertCtrl.create({
                title: "Error",
                subTitle: "Please check that all the fields are filled and try again.",
                buttons: ['OK']
            });
            alert_1.present(prompt);
        }
        else {
            this.api.updateUserAddress(this.accountInfo).subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                //console.log("Response : "+status+" BODY "+JSON.stringify(body));
                _this.navCtrl.setRoot('PaymentMethodPage');
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "Error",
                    subTitle: "Please check that all the fields are filled and try again.",
                    buttons: ['OK']
                });
                alert.present(prompt);
            });
        }
    };
    AddressPage.prototype.sameAddress = function () {
        this.navCtrl.setRoot('PaymentMethodPage');
    };
    AddressPage.prototype.updateState = function () {
        var _this = this;
        this.api.getState(this.accountInfo.country_id).subscribe(function (res) {
            //var status = res.status;
            var body = res.json();
            var found;
            //console.log("Response : "+status+" BODY "+JSON.stringify(body));
            _this.state = body.states;
            if (_this.state != null && _this.state != "") {
                //this.accountInfo.state_id = this.state[0].id;
                //console.log("check state s="+s.id+" Saved ID="+this.accountInfo.state_id);
                for (var _i = 0, _a = _this.state; _i < _a.length; _i++) {
                    var s = _a[_i];
                    //console.log("check state s="+s.id+" Saved ID="+this.accountInfo.state_id);
                    if (s.id == _this.accountInfo.state_id) {
                        //console.log("break at id "+s.id+" "+this.accountInfo.state_id);
                        found = s.id;
                        break;
                    }
                }
                if (found != "" && found != null) {
                    _this.accountInfo.state_id = found;
                }
            }
            else {
                _this.accountInfo.state_id = 0;
                console.log("Reset State");
            }
        }, function (err) {
        });
    };
    AddressPage.prototype.stateName = function (state_id) {
        for (var _i = 0, _a = this.state; _i < _a.length; _i++) {
            var s = _a[_i];
            if (s.id == this.accountInfo.state_id) {
                return s.name;
            }
        }
    };
    AddressPage.prototype.countryName = function (country_id) {
        for (var _i = 0, _a = this.country; _i < _a.length; _i++) {
            var c = _a[_i];
            if (c.id == this.accountInfo.country_id) {
                return c.name;
            }
        }
    };
    AddressPage.prototype.back = function () {
        this.navCtrl.setRoot('AmountPage');
    };
    AddressPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    AddressPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return AddressPage;
}());
AddressPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-address',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/topup/address.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a3eb9967b791a0001977dff" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>topup1</title>\n  <meta content="topup1" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="assets/images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div class="navbar">\n    <div class="w-container">\n      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar home w-nav">\n        <div class="nav-container"><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a>\n          <div class="menu-button w-nav-button">\n            <div (click)="openMenu()" class="w-icon-nav-menu"></div>\n          </div><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="brand-3 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n          <nav role="navigation" class="nav-menu w-nav-menu">\n            <a (click)="navCtrl.setRoot(\'StorePage\')" class="navlink w-inline-block">\n              <div>Shop</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'KioskPage\')" class="navlink w-inline-block">\n              <div>Find a kiosk</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'WhyPage\')" class="navlink w-inline-block">\n              <div>Why Water3</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="navlink w-inline-block">\n              <div>FAQ</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'ContactPage\')" class="navlink w-inline-block">\n              <div>Contact</div>\n            </a>\n\n            <div class="nav-line"></div>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'RegisterPage\')" class="nav-button register w-button">Register</a>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'LoginPage\')" class="nav-button login w-button">Login</a>\n\n            <a *ngIf="isLoggedin" (click)="navCtrl.setRoot(\'AccountPage\')" class="navlink w-inline-block">My Account</a>\n\n            <a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></nav>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ion-content *ngIf="isLoaded">\n  <div class="topup-section">\n    <div class="topup-head">\n      <div class="topup-row w-row">\n        <div class="column-3 w-col w-col-4 w-col-tiny-tiny-stack"></div>\n        <div class="w-col w-col-4 w-col-tiny-tiny-stack">\n          <div class="step-box">\n            <div class="div-block-35 step-1">\n              <div class="text-block-14 _1">1</div>\n            </div>\n            <div class="line-step past"></div>\n            <div class="div-block-35 step2 current">\n              <div class="text-block-14 current">2</div>\n            </div>\n            <div class="line-step"></div>\n            <div class="div-block-35 step3">\n              <div class="text-block-14 _3">3</div>\n            </div>\n            <div class="line-step"></div>\n            <div class="div-block-35 step3">\n              <div class="text-block-14 _3">4</div>\n            </div>\n          </div>\n        </div>\n        <div class="column-4 w-col w-col-4 w-col-tiny-tiny-stack">\n          <div class="canel-box"><a (click)="navCtrl.setRoot(\'AmountPage\')" class="link-3">BACK</a></div>\n        </div>\n      </div>\n    </div>\n    <div class="topup-container w-container">\n      <div class="topup-form-1">\n        <h3 class="h3-topup">Confirm Billing Address</h3>\n        <div class="text-block-15">Current Address :</div>\n        <div class="text-block-6 username">\n                    {{sameAccountInfo.address_1}}\n                    <br>\n                    {{sameAccountInfo.address_2}}\n                    <br>\n                    {{sameAccountInfo.city}}\n                    <br>\n                    {{stateName(sameAccountInfo.state_id)}}\n                    <br>\n                    {{countryName(sameAccountInfo.country_id)}}, {{sameAccountInfo.postcode}}\n                    <br><br>\n        </div>\n        <div>\n          <a (click)="sameAddress()" class="add-address-btn w-inline-block">\n            <div>Use this address</div>\n          </a>\n          <div class="div-block-22"><img src="assets/images/line1.png" width="186"></div>\n        </div>\n        <div class="text-block-15">Or Enter a different billing address</div>\n        <div class="w-form">\n          <form id="email-form" name="email-form" data-name="Email Form">\n            <label for="Address-line-2">Address line 1</label>\n            <input type="text" class="top-up-filed-form w-input" [(ngModel)]="accountInfo.address_1" maxlength="256" name="Address-line-1" data-name="Address line 1" placeholder="Address line 1" id="Address-line-2">\n            <label for="Address-line">Address line 2</label>\n            <input type="text" class="top-up-filed-form w-input" [(ngModel)]="accountInfo.address_2" maxlength="256" name="Address-line-2" data-name="Address line 2" placeholder="Address line 2" id="Address-line">\n            <label for="City">City</label>\n            <input type="text" class="top-up-filed-form w-input" [(ngModel)]="accountInfo.city" maxlength="256" name="City" data-name="City" placeholder="City" id="City">\n            <div>\n              <div class="w-row">\n                <div class="w-col w-col-6"><label for="State">State</label>\n                  <ion-item *ngIf="state!=null && state!=\'\'">\n                    <ion-select interface="popover" class="addressSelect" name="state" placeholder="Select State" [(ngModel)]="accountInfo.state_id">\n                    <ion-option text-left *ngFor="let s of state" value="{{ s.id }}" selected="true">{{s.name}}</ion-option>\n                    </ion-select>\n                    </ion-item>\n                    <ion-item *ngIf="state==null || state==\'\'">\n                    <ion-select interface="popover" class="addressSelect" name="state" placeholder="--" disabled="true">\n                    </ion-select>\n                    </ion-item>\n                </div>\n                <div class="w-col w-col-6"><label for="Post-Code">Post Code</label>\n                  <input type="text" class="top-up-filed-form w-input" [(ngModel)]="accountInfo.postcode" maxlength="256" name="Post-Code" data-name="Post Code" placeholder="Post Code" id="Post-Code">\n                </div>\n              </div>\n            </div><label for="City-2">Country</label>\n            <ion-item>\n                    <ion-select interface="popover" class="addressSelect" (ionChange)="updateState()" name="country" [(ngModel)]="accountInfo.country_id">\n                    <ion-option text-left value="14" selected="true">Australia</ion-option>\n                    <ion-option text-left value="239" selected="true">United States</ion-option>\n                    </ion-select>\n                    </ion-item>\n            <div style="padding-top: 10px;">\n              <a (click)="postAddress()" class="top-up-next-btn w-button">Next</a>\n            </div>\n          </form>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class="footer-section" style="height: 250px;">\n    <div class="w-container">\n      <div class="row w-row">\n        <div class="w-col w-col-3">\n          <div><img src="assets/images/Logo.png" width="121.5" class="image-3">\n            <div class="text-copy-right _1">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n        <div class="w-col w-col-6">\n          <div class="w-row">\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-18">\n                <a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="footer-link">Home</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Shop</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Find a kiosk</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Why Water3</a>\n                <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="footer-link">FAQ</a>\n                <a (click)="navCtrl.setRoot(\'ContactPage\')" class="footer-link">Contact</a></div>\n            </div>\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-19">\n                <a href="#" class="footer-link">Shipping</a>\n                <a (click)="navCtrl.setRoot(\'TermsPage\')" class="footer-link">Term of Services</a>\n                <a (click)="navCtrl.setRoot(\'PrivacyPage\')" class="footer-link">Privacy Policy</a></div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-21"><a href="#" class="social-link w-inline-block"><img src="assets/images/fb-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/ig-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/twitter-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/linkedin-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/youtube-icon.png" class="social-icon"></a><a href="#" class="footer-email">info@water3.com.au</a>\n            <div class="text-block-5">+61 410 000 000</div>\n            <div class="footer-address">12 New Street Name, Suburb<br>QLD 4000, Australia</div>\n            <div class="text-copy-right _2">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content></body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/topup/address.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], AddressPage);

//# sourceMappingURL=address.js.map

/***/ })

});
//# sourceMappingURL=10.main.js.map