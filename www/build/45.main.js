webpackJsonp([45],{

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__about__ = __webpack_require__(321);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutPageModule", function() { return AboutPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AboutPageModule = (function () {
    function AboutPageModule() {
    }
    return AboutPageModule;
}());
AboutPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__about__["a" /* AboutPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__about__["a" /* AboutPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__about__["a" /* AboutPage */]
        ]
    })
], AboutPageModule);

//# sourceMappingURL=about.module.js.map

/***/ }),

/***/ 321:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AboutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AboutPage = (function () {
    function AboutPage(navCtrl, navParams, menuCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.video = false;
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutPage');
    };
    AboutPage.prototype.toggleVideo = function () {
        if (this.video == true)
            this.video = false;
        else if (this.video == false)
            this.video = true;
    };
    AboutPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    return AboutPage;
}());
AboutPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-about',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/about/about.html"*/'<!DOCTYPE html>\n<html lang="en-AU" >\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <title>Filtered water: a healthier alternative to plastic bottled water</title>\n        <meta name="title" content="water3 | think better. be better. do more">\n        <meta name="description" content="water3 is revolutionising the way we drink water; via a network of innovative kiosks that provide fresh and healthy chilled spring water.">\n        <meta content="width=device-width, initial-scale=1" name="viewport">\n        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:400,700,300%7CUbuntu">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/style-1489479537.css">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/app-1486002319.css">\n        <link rel="shortcut icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <link rel="icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n                <script type="text/javascript">\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n    ga(\'create\', \'UA-83198216-1\', \'auto\');\n    ga(\'send\', \'pageview\');\n</script>\n    </head>\n        <body class="layout-default" overflow-scroll="true">\n        \n        <header class="page">\n        <div class="container">\n        <a class="branding" href="index.html" title="Home">\n            <img src="assets/img/water3-logo.png" alt="Water3">\n        </a>\n\n        <button class="open-navigation" type="button" aria-label="Toggle menu" (click)="openMenu()">\n            <span class="bar top"></span>\n            <span class="bar middle"></span>\n            <span class="bar bottom"></span>\n            <span class="text">Menu</span>\n        </button>\n\n        <a class="btn btn-top-up btn-circle btn-icon" (click)="navCtrl.setRoot(\'TopupPage\')" title="Top up">\n            <span class="icon icon-top-up hidden-hover"></span>\n            <span class="icon icon-top-up-white visible-hover"></span>\n            <span class="text">Top Up</span>\n        </a>\n    </div>\n</header>\n        <ion-content>\n        <main class="page home with-bottom-button">\n        \n        <section id="hero" class="component component-hero">\n            <div class="container">\n                <div class="content">\n                    <h1>Join the water revolution!</h1>\n                    <p>Learn more about the inspiration behind water3.</p>\n\n                    <button class="btn btn-a btn-play" title="Play" type="button" (click)="toggleVideo()">Play</button>\n                                    </div>\n\n                <button class="scroll-to" type="button" data-scroll="#water-revolution">\n                    <span class="text">Scroll to learn more</span>\n                    <span class="icon icon-arrow-down-white"></span>\n                </button>\n            </div>\n\n                <div *ngIf="video" (click)="toggleVideo()" class="container">\n                    <div class="video-outer">\n                        <div class="video-inner">\n                            <div class="video">\n                                <iframe id="player" src="https://www.youtube.com/embed/rlfrwCnBinQ?enablejsapi=1" frameborder="0" allowfullscreen></iframe>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n            <div class="background"></div>\n            <div class="gradient"></div>\n        </section>\n\n        <section id="about" class="component component-text">\n            <div class="container">\n                <div class="row">\n                    <div class="col-xs-12 col-md-10 col-md-offset-1">\n                        <h1 class="text-center">Join the water revolution!</h1>\n\n                        <p>The number one thing you can do to save our planet is to ditch plastic bottled water for good. water3 provides a fast, affordable and eco friendly way to drink water and save the planet.</p>\n                        <p>Every time you refill at a water3 kiosk you are not only reducing the amount of plastic bottle waste in our environment, but you are also directly funding a number of water3 conservation projects including; river and beach cleanups, turtle rehabilitation, and animal rescue.</p>\n                        <p>With the water3 app you can hydrate on the go in 4 simple steps:</p>\n                        <ol>\n                            <li><a (click)="navCtrl.setRoot(\'LoginPage\')" title="Login">Login</a> or <a (click)="navCtrl.setRoot(\'RegisterPage\')" title="Register">register</a> your account and bottle</li>\n                            <li><a (click)="navCtrl.setRoot(\'TopupPage\')" title="Top up">Top up</a> credit</li>\n                            <li>Search for a water3 kiosk near you using our <a (click)="navCtrl.setRoot(\'KioskPage\')" title="Find a kiosk">kiosk locator</a></li>\n                            <li>Refill with still or sparkling water at a conveniently located kiosk near you</li>\n                        </ol>\n                        <p>If you don’t have a water bottle handy, water3 kiosks also provide a state of the art water3 bottle with embedded RFID technology to make refilling even easier. Just swipe and go for a refreshing bottle of ice cold spring water.</p>\n                    </div>\n                </div>\n            </div>\n        </section>\n\n        \n        \n    </main>\n\n        <footer class="page">\n    <div class="container">\n        <div class="row">\n            <div class="col-xs-12 col-md-6 social">\n                <h3>Follow Us</h3>\n\n                <div class="actions">\n                    <a class="btn btn-c btn-circle btn-icon" href="https://www.facebook.com/water3global" title="Facebook" target="_blank">\n                        <span class="icon icon-facebook hidden-hover"></span>\n                        <span class="icon icon-facebook-white visible-hover"></span>\n                        <span class="text">Facebook</span>\n                    </a>\n\n                    <a class="btn btn-c btn-circle btn-icon" href="https://twitter.com/water3_" title="Twitter" target="_blank">\n                        <span class="icon icon-twitter hidden-hover"></span>\n                        <span class="icon icon-twitter-white visible-hover"></span>\n                        <span class="text">Twitter</span>\n                    </a>\n\n                    <a class="btn btn-c btn-circle btn-icon" href="https://www.linkedin.com/company/water3" title="LinkedIn" target="_blank">\n                        <span class="icon icon-linkedin hidden-hover"></span>\n                        <span class="icon icon-linkedin-white visible-hover"></span>\n                        <span class="text">LinkedIn</span>\n                    </a>\n\n                    <a class="btn btn-c btn-circle btn-icon" href="https://www.instagram.com/_water3_" title="Instagram" target="_blank">\n                        <span class="icon icon-instagram hidden-hover"></span>\n                        <span class="icon icon-instagram-white visible-hover"></span>\n                        <span class="text">Instagram</span>\n                    </a>\n\n                    <a class="btn btn-c btn-circle btn-icon" href="https://www.youtube.com/channel/UCCPqmAd8l2dCmH2iGXbSFvg" title="YouTube" target="_blank">\n                        <span class="icon icon-youtube hidden-hover"></span>\n                        <span class="icon icon-youtube-white visible-hover"></span>\n                        <span class="text">YouTube</span>\n                    </a>\n                </div>\n            </div>\n\n            <div class="col-xs-12 col-md-6 subscribe">\n                <h3>Subscribe</h3>\n\n                <form method="POST" action="https://water3.com.au/subscribe#subscribe" accept-charset="UTF-8" id="subscribe"><input name="_session_key" type="hidden" value="eUawDN3pvQfIycwYiJkiaxytd1gS29WbCAYC0Pn2"><input name="_token" type="hidden" value="fzjkFcCSHFdpYLYa4Iogpix7ogBm5Cuw1DLYfo30">\n                    <div class="pre-alerts">\n                                                                    </div>\n\n                    <div class="field-group">\n                        <div class="form-group">\n                            <label for="email" class="sr-only">Email address</label>\n                            <span class="icon icon-envelope"></span>\n                            <input class="form-control" placeholder="Email address" name="email" type="email" id="email">\n                        </div>\n                    </div>\n\n                    <button class="btn btn-c" type="submit">Submit</button>\n\n                    <div class="post-alerts">\n                                                                    </div>\n                </form>\n            </div>\n        </div>\n\n        <div class="divider"></div>\n\n        <div class="row">\n            <div class="col-xs-12">\n                <ul class="navigation">\n                    <li><a (click)="navCtrl.setRoot(\'KioskPage\')" title="Find kiosks">Find kiosks</a></li>\n                    <li><a (click)="navCtrl.setRoot(\'FaqsPage\')"  title="Frequently asked questions">FAQs</a></li>\n                    <li><a (click)="navCtrl.setRoot(\'TermsPage\')"  title="Terms of service">Terms of service</a></li>\n                    <li><a (click)="navCtrl.setRoot(\'PrivacyPage\')"  title="Privacy policy">Privacy policy</a></li>\n                </ul>\n\n                <p class="question">Have a question? <a (click)="navCtrl.setRoot(\'ContactPage\')" title="Contact us">Contact us</a></p>\n\n                <p class="copyright">\n                    Copyright &copy; 2017 Vending Machines International PTE LTD. All Rights Reserved.<br>\n                    Designed by <a href="https://www.littlegiant.co.nz/" title="Little Giant">Little Giant</a>.\n                </p>\n            </div>\n        </div>\n    </div>\n</footer>\n</ion-content>\n        <section class="component component-bottom-button responsive">\n    <div class="button-container">\n        <a class="btn btn-icon-inline btn-square btn-facebook-messenger" href="https://m.me/water3global" target="_blank">\n            <span class="icon icon-facebook-messenger"></span>\n            Message water3\n        </a>\n    </div>\n</section>\n    </body>\n\n    <script type="text/javascript" src="assets/js/vendor-1486002319.js"></script>\n        <script type="text/javascript" src="assets/js/app-1486002319.js"></script>\n\n</html>\n'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/about/about.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ })

});
//# sourceMappingURL=45.main.js.map