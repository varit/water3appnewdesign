webpackJsonp([26],{

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__request__ = __webpack_require__(339);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestPageModule", function() { return RequestPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RequestPageModule = (function () {
    function RequestPageModule() {
    }
    return RequestPageModule;
}());
RequestPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__request__["a" /* RequestPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__request__["a" /* RequestPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__request__["a" /* RequestPage */]
        ]
    })
], RequestPageModule);

//# sourceMappingURL=request.module.js.map

/***/ }),

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RequestPage = (function () {
    function RequestPage(navCtrl, alertCtrl, loadingCtrl, navParams, menuCtrl, api, event) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.api = api;
        this.event = event;
    }
    RequestPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    RequestPage.prototype.postRequest = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({ content: "Please wait..." });
        loading.present();
        this.api.AdminLogin().subscribe(function (res) {
            var body = res.json();
            localStorage.setItem("token", body.access_token);
            _this.api.requestResetPassword(_this.email).subscribe(function (res) {
                var body = res.json();
                var data = {
                    email_to: _this.email,
                    email_from: "noreply@water3.com.au",
                    subject: "Reset Password for your water3 account",
                    template_file: "reset_password",
                    params: { token: body.token }
                };
                _this.api.sendEmail(data).subscribe(function (res) {
                    console.log(res.json());
                });
                var alert = _this.alertCtrl.create({
                    title: "Success",
                    subTitle: "An email with reset password instructions has been sent",
                    buttons: ['OK']
                });
                alert.present(prompt);
                loading.dismiss();
                _this.navCtrl.setRoot('HelloIonicPage');
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "Error",
                    subTitle: "Incorrect email or the email does not exist",
                    buttons: ['OK']
                });
                alert.present(prompt);
                loading.dismiss();
            });
        });
    };
    RequestPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return RequestPage;
}());
RequestPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/request/request.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a4f4c3f5c751a000134a95a" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>Login</title>\n  <meta content="Login" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">\n    <div class="w-container">\n      <div class="menu-button w-nav-button">\n        <div class="w-icon-nav-menu"></div>\n      </div><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="brand-5 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n      <nav role="navigation" class="nav-menu-2 w-nav-menu">\n        <div class="div-block">Already have an account?</div><a (click)="navCtrl.setRoot(\'LoginPage\')" class="nav-button w-button">Login</a><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></nav><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png"  class="image-11"><div class="text-cart">({{itemAmount}})</div></a></div>\n  </div>\n  <ion-content>\n  <div class="section-main-hero login">\n    <div class="div-block-41"><img src="assets/images/500_F_115215766_xSCOJkKThtRgbtTaYOmEcB3Z05fIjk9X.png" class="image-10">\n    </div>\n    <div class="register-container w-container">\n      <div class="sub-nav back"><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="back-btn _2 w-inline-block w-clearfix"><img src="assets/images/Shape.png" width="15" class="image"><div class="text-back _2">Back</div></a></div>\n      <div class="w-row">\n        <div class="column-6 w-col w-col-4 w-col-medium-6 w-col-small-small-stack w-col-tiny-tiny-stack">\n          <div class="form-block-3 w-form">\n            <form id="email-form" name="email-form" data-name="Email Form" class="from">\n              <h3 class="h3-register">Reset your password</h3>\n              <div class="div-text">\n                <div class="w-row">\n                  <div class="w-col w-col-3 w-col-small-4 w-col-tiny-4">\n                    <div class="line"></div>\n                  </div>\n                  <div class="w-col w-col-6 w-col-small-4 w-col-tiny-4">\n                    <div class="text-block">enter your water3 account email below</div>\n                  </div>\n                  <div class="column w-col w-col-3 w-col-small-4 w-col-tiny-4">\n                    <div class="line"></div>\n                  </div>\n                </div>\n              </div>\n              <label for="email-3" class="field-label">Email Address:</label>\n              <input [(ngModel)]="email"  type="email" class="w-input" maxlength="256" name="email" data-name="email" placeholder="Enter your email" id="email-3">\n\n              <input (click)="postRequest()" type="submit" value="Reset Password" data-wait="Please wait..." class="login-btn w-button">\n              </form>\n          </div>\n        </div>\n        <div class="column-5 w-col w-col-8 w-col-medium-6 w-col-small-small-stack w-col-tiny-tiny-stack"></div>\n      </div>\n    </div>\n  </div>\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content>\n</body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/request/request.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Events */]])
], RequestPage);

//# sourceMappingURL=request.js.map

/***/ })

});
//# sourceMappingURL=26.main.js.map