webpackJsonp([9],{

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__amount__ = __webpack_require__(358);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AmountPageModule", function() { return AmountPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AmountPageModule = (function () {
    function AmountPageModule() {
    }
    return AmountPageModule;
}());
AmountPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__amount__["a" /* AmountPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__amount__["a" /* AmountPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__amount__["a" /* AmountPage */]
        ]
    })
], AmountPageModule);

//# sourceMappingURL=amount.module.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AmountPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AmountPage = (function () {
    function AmountPage(navCtrl, alertCtrl, loadingCtrl, navParams, api, menuCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.api.checkSession();
    }
    AmountPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TopupPage - Amount');
    };
    AmountPage.prototype.postAmount = function () {
        console.log("POST amount");
        if (this.amount != null && this.amount != "" && this.amount >= 5) {
            localStorage.setItem("amount", parseFloat(this.amount).toFixed(2));
            console.log("Set Amount = " + this.amount);
            this.navCtrl.setRoot('AddressPage');
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: "Amount error",
                subTitle: "Please input a valid amount (Minimum $5.00)",
                buttons: ['OK']
            });
            alert_1.present(prompt);
        }
    };
    AmountPage.prototype.setAmount = function (amount) {
        this.amount = amount;
    };
    AmountPage.prototype.setDecimal = function () {
        this.amount = parseFloat(this.amount).toFixed(2);
    };
    AmountPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    AmountPage.prototype.back = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    AmountPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return AmountPage;
}());
AmountPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-amount',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/topup/amount.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a3eb9967b791a0001977dff" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>topup1</title>\n  <meta content="topup1" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="assets/images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div class="navbar">\n    <div class="w-container">\n      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar home w-nav">\n        <div class="nav-container"><a href="#" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" width="15" class="image-11"><div class="text-cart">(2)</div></a>\n          <div class="menu-button w-nav-button">\n            <div class="w-icon-nav-menu"></div>\n          </div><a href="#" class="brand-3 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n          <nav role="navigation" class="nav-menu w-nav-menu">\n            <a href="#" class="navlink w-inline-block">\n              <div>Shop</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'KioskPage\')" class="navlink w-inline-block">\n              <div>Find a kiosk</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'WhyPage\')" class="navlink w-inline-block">\n              <div>Why Water3</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="navlink w-inline-block">\n              <div>FAQ</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'ContactPage\')" class="navlink w-inline-block">\n              <div>Contact</div>\n            </a>\n            <div class="nav-line"></div>\n            <div data-delay="0" class="w-dropdown">\n            <a href="#" class="navlink w-inline-block">Sign Out</a>\n            </div><a href="#" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">(2)</div></a></nav>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ion-content>\n  <div class="topup-section">\n    <div class="topup-head">\n      <div class="topup-row w-row">\n        <div class="column-3 w-col w-col-4 w-col-tiny-tiny-stack"></div>\n        <div class="w-col w-col-4 w-col-tiny-tiny-stack">\n          <div class="step-box">\n            <div class="div-block-35 step-1">\n              <div class="text-block-14 _1">1</div>\n            </div>\n            <div class="line-step"></div>\n            <div class="div-block-35 step2">\n              <div class="text-block-14 _2">2</div>\n            </div>\n            <div class="line-step"></div>\n            <div class="div-block-35 step3">\n              <div class="text-block-14 _3">3</div>\n            </div>\n          </div>\n        </div>\n        <div class="column-4 w-col w-col-4 w-col-tiny-tiny-stack">\n          <div class="canel-box"><a (click)="navCtrl.setRoot(\'AccountPage\')" class="link-3">CANCEL</a></div>\n        </div>\n      </div>\n    </div>\n    <div class="topup-container w-container">\n      <div class="topup-form-1">\n        <h3 class="h3-topup">Select Top Up</h3>\n        <div>\n            <a (click)="setAmount(5)" class="top-up-price-btn normal w-button">$ 5</a>\n            <a (click)="setAmount(10)" class="top-up-price-btn normal w-button">$ 10</a>\n            <a (click)="setAmount(15)" class="top-up-price-btn normal w-button">$ 15</a>\n          <div class="div-block-22"><img src="assets/images/line1.png" width="186"></div>\n        </div>\n        <div class="text-block-15">Or enter a different amount</div>\n        <div class="w-form">\n          <form id="email-form" name="email-form" data-name="Email Form"><label for="Amount">Enter Amount</label>\n            <input type="text" [(ngModel)]="amount" (change)="setDecimal()" class="top-up-filed-form w-input" maxlength="256" name="Amount" data-name="Amount" placeholder="Enter your Amount" id="Amount">\n            <label for="Coupon-code">Add Coupon code</label>\n            <input type="text" class="top-up-filed-form w-input" maxlength="256" name="Coupon-code" data-name="Coupon code" placeholder="Enter coupon code" id="Coupon-code">\n            <input type="submit" value="Back" data-wait="Please wait..." class="top-up-back-btn w-button">\n            <a (click)="postAmount()" class="top-up-next-btn w-button">Next</a>\n        </form>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class="footer-section" style="height: 250px;">\n    <div class="w-container">\n      <div class="row w-row">\n        <div class="w-col w-col-3">\n          <div><img src="assets/images/Logo.png" width="121.5" class="image-3">\n            <div class="text-copy-right _1">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n        <div class="w-col w-col-6">\n          <div class="w-row">\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-18">\n                <a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="footer-link">Home</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Shop</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Find a kiosk</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Why Water3</a>\n                <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="footer-link">FAQ</a>\n                <a (click)="navCtrl.setRoot(\'ContactPage\')" class="footer-link">Contact</a></div>\n            </div>\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-19">\n                <a href="#" class="footer-link">Shipping</a>\n                <a (click)="navCtrl.setRoot(\'TermsPage\')" class="footer-link">Term of Services</a>\n                <a (click)="navCtrl.setRoot(\'PrivacyPage\')" class="footer-link">Privacy Policy</a></div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-21"><a href="#" class="social-link w-inline-block"><img src="assets/images/fb-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/ig-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/twitter-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/linkedin-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/youtube-icon.png" class="social-icon"></a><a href="#" class="footer-email">info@water3.com.au</a>\n            <div class="text-block-5">+61 410 000 000</div>\n            <div class="footer-address">12 New Street Name, Suburb<br>QLD 4000, Australia</div>\n            <div class="text-copy-right _2">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content></body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/topup/amount.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], AmountPage);

//# sourceMappingURL=amount.js.map

/***/ })

});
//# sourceMappingURL=9.main.js.map