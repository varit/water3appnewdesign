webpackJsonp([2],{

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__why__ = __webpack_require__(365);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WhyPageModule", function() { return WhyPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var WhyPageModule = (function () {
    function WhyPageModule() {
    }
    return WhyPageModule;
}());
WhyPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__why__["a" /* WhyPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__why__["a" /* WhyPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__why__["a" /* WhyPage */]
        ]
    })
], WhyPageModule);

//# sourceMappingURL=why.module.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WhyPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the FaqsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var WhyPage = (function () {
    function WhyPage(navCtrl, platform, navParams, menuCtrl) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
    }
    WhyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad WhyPage');
    };
    WhyPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    return WhyPage;
}());
WhyPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-why',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/whywater3/why.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a4b4f093472ef00014c811b" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>whywater3-impact</title>\n  <meta content="whywater3-impact" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n<div class="navbar">\n    <div class="w-container">\n      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar home w-nav">\n        <div class="nav-container"><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a>\n          <div class="menu-button w-nav-button">\n            <div (click)="openMenu()" class="w-icon-nav-menu"></div>\n          </div><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="brand-3 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n          <nav role="navigation" class="nav-menu w-nav-menu">\n            <a (click)="navCtrl.setRoot(\'StorePage\')" class="navlink w-inline-block">\n              <div>Shop</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'KioskPage\')" class="navlink w-inline-block">\n              <div>Find a kiosk</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'WhyPage\')" class="navlink w-inline-block">\n              <div>Why Water3</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="navlink w-inline-block">\n              <div>FAQ</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'ContactPage\')" class="navlink w-inline-block">\n              <div>Contact</div>\n            </a>\n\n            <div class="nav-line"></div>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'RegisterPage\')" class="nav-button register w-button">Register</a>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'LoginPage\')" class="nav-button login w-button">Login</a>\n\n            <a *ngIf="isLoggedin" (click)="navCtrl.setRoot(\'AccountPage\')" class="navlink w-inline-block">My Account</a>\n\n            <a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></nav>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ion-content>\n  <div class="section-main-hero impact">\n    <div><img src="assets/images/Group-2.png" srcset="assets/images/Group-2-p-500.png 500w, assets/images/Group-2.png 1440w" sizes="100vw" class="image-7"><img src="assets/images/Mask.png" class="image-8"></div>\n  </div>\n  <div class="section-feature">\n    <div class="w-container">\n      <h3 class="heading h3-feature">Convenient spring water without the environmental cost.<br>Better for you, better for the planet.</h3>\n      <div class="w-row">\n        <div class="w-col w-col-3">\n          <div class="div-block-6"><img src="assets/images/feature-image01.png" width="90">\n            <div class="text-feature">Purified water on the go without the waste ipsum dolor</div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-5"><img src="assets/images/feature-image02.png" width="90">\n            <div class="text-feature">We make it easy ipsum dolor sit consectetur adipising</div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-4"><img src="assets/images/feature-image03.png" width="90">\n            <div class="text-feature">Cutting edge technolgy dolor sit amet consectetur</div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-3"><img src="assets/images/feature-image04.png" width="90">\n            <div class="text-feature">Environmentally friendly over 2 lines ipsum dolor</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class="section-impact2">\n    <div class="container-4 w-container">\n      <h3 class="heading h3-feature impact">The Water3 impact</h3>\n      <p class="paragraph-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do<br>eiusmod tempor incididunt ut labore et dolore.</p>\n      <div>\n        <div class="w-row">\n          <div class="w-col w-col-4 w-col-small-4">\n            <div class="div-block-37">\n              <h1 class="heading-3 impact">57%</h1>\n              <p class="paragraph-3">Statistic relating to damage<br>caused by plastic</p>\n            </div>\n          </div>\n          <div class="w-col w-col-4 w-col-small-4">\n            <div class="div-block-37">\n              <h1 class="heading-3 impact">65k</h1>\n              <p class="paragraph-3">Statistic relating to plastic<br>footprint</p>\n            </div>\n          </div>\n          <div class="w-col w-col-4 w-col-small-4">\n            <div class="div-block-37">\n              <h1 class="heading-3 impact">15L</h1>\n              <p class="paragraph-3">Statistic relating to<br>sustainable\n                <br>impact water3 is having</p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class="container-3 w-container">\n      <h3 class="heading h3-feature impact">The Water3 impact</h3>\n      <p class="paragraph-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do<br>eiusmod tempor incididunt ut labore et dolore.</p>\n      <div>\n        <div data-delay="4000" data-animation="slide" data-autoplay="1" data-duration="500" data-infinite="1" class="imapact-slider w-slider">\n          <div class="w-slider-mask">\n            <div class="w-slide">\n              <div class="div-block-37">\n                <h1 class="heading-3 impact">57%</h1>\n                <p class="paragraph-3">Statistic relating to damage<br>caused by plastic</p>\n              </div>\n            </div>\n            <div class="w-slide">\n              <div class="div-block-37">\n                <h1 class="heading-3 impact">65k</h1>\n                <p class="paragraph-3">Statistic relating to plastic<br>footprint</p>\n              </div>\n            </div>\n            <div class="w-slide">\n              <div class="div-block-37">\n                <h1 class="heading-3 impact">15L</h1>\n                <p class="paragraph-3">Statistic relating to<br>sustainable\n                  <br>impact water3 is having</p>\n              </div>\n            </div>\n          </div>\n          <div class="left-arrow-4 w-slider-arrow-left">\n            <div class="w-icon-slider-left"></div>\n          </div>\n          <div class="right-arrow-4 w-slider-arrow-right">\n            <div class="w-icon-slider-right"></div>\n          </div>\n          <div class="slide-nav-4 w-slider-nav w-round"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class="footer-section" style="height: 250px;">\n    <div class="w-container">\n      <div class="row w-row">\n        <div class="w-col w-col-3">\n          <div><img src="assets/images/Logo.png" width="121.5" class="image-3">\n            <div class="text-copy-right _1">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n        <div class="w-col w-col-6">\n          <div class="w-row">\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-18">\n                <a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="footer-link">Home</a>\n                <a (click)="navCtrl.setRoot(\'StorePage\')" class="footer-link">Shop</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Find a kiosk</a>\n                <a (click)="navCtrl.setRoot(\'WhyPage\')" class="footer-link">Why Water3</a>\n                <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="footer-link">FAQ</a>\n                <a (click)="navCtrl.setRoot(\'ContactPage\')" class="footer-link">Contact</a></div>\n            </div>\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-19">\n                <a href="#" class="footer-link">Shipping</a>\n                <a (click)="navCtrl.setRoot(\'TermsPage\')" class="footer-link">Term of Services</a>\n                <a (click)="navCtrl.setRoot(\'PrivacyPage\')" class="footer-link">Privacy Policy</a></div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-21">\n            <a href="https://www.facebook.com/water3global" class="social-link w-inline-block"><img src="assets/images/fb-icon.png" class="social-icon"></a>\n            <a href="https://www.instagram.com/_water3_" class="social-link w-inline-block"><img src="assets/images/ig-icon.png" class="social-icon"></a>\n            <a href="https://twitter.com/water3_" class="social-link w-inline-block"><img src="assets/images/twitter-icon.png" class="social-icon"></a>\n            <a href="https://www.linkedin.com/company/water3" class="social-link w-inline-block"><img src="assets/images/linkedin-icon.png" class="social-icon"></a>\n            <a href="https://www.youtube.com/channel/UCCPqmAd8l2dCmH2iGXbSFvg" class="social-link w-inline-block"><img src="assets/images/youtube-icon.png" class="social-icon"></a>\n            <a href="mailto:info@water3.com.au" class="footer-email">info@water3.com.au</a>\n            <div class="text-block-5">+61 410 000 000</div>\n            <div class="footer-address">12 New Street Name, Suburb<br>QLD 4000, Australia</div>\n            <div class="text-copy-right _2">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content></body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/whywater3/why.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], WhyPage);

//# sourceMappingURL=why.js.map

/***/ })

});
//# sourceMappingURL=2.main.js.map