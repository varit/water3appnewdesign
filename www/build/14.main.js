webpackJsonp([14],{

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sub_payment__ = __webpack_require__(353);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubPaymentPageModule", function() { return SubPaymentPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SubPaymentPageModule = (function () {
    function SubPaymentPageModule() {
    }
    return SubPaymentPageModule;
}());
SubPaymentPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__sub_payment__["a" /* SubPaymentPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sub_payment__["a" /* SubPaymentPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__sub_payment__["a" /* SubPaymentPage */]
        ]
    })
], SubPaymentPageModule);

//# sourceMappingURL=sub-payment.module.js.map

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubPaymentPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SubPaymentPage = (function () {
    function SubPaymentPage(navCtrl, loadingCtrl, alertCtrl, navParams, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.save = false;
        this.Stripe_param = { name: '', number: '', exp_month: '', exp_year: '', cvc: '' };
        if (this.api.checkSession()) {
            this.amount = localStorage.getItem('subPlan');
            this.amount = this.amount.substr(5);
            console.log("Amount due", this.amount);
            var loading_1 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_1.present();
            if (localStorage.getItem("card_id") != null || localStorage.getItem("card_id") != "")
                this.api.getUserCreditCards().subscribe(function (res) {
                    var body = res.json();
                    console.log("Response : " + status + " BODY " + JSON.stringify(body));
                    body.cards.forEach(function (element) {
                        if (element.stripe_card_id == localStorage.getItem("card_id")) {
                            _this.card = element;
                            console.log(_this.card.stripe_card_id);
                        }
                    });
                    _this.isLoaded = true;
                    loading_1.dismiss();
                }, function (err) {
                    var alert = _this.alertCtrl.create({
                        title: "Error " + err.status,
                        subTitle: "Cannot retrieve card data",
                        buttons: ['OK']
                    });
                    alert.present(prompt);
                });
            this.stripe_pk = this.api.getStripePK();
        }
    }
    SubPaymentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Subscription - payment');
        Stripe.setPublishableKey(this.stripe_pk);
    };
    SubPaymentPage.prototype.postPayment = function () {
        var _this = this;
        console.log(this.Stripe_param);
        console.log(this.save);
        if (!this.card) {
            if (this.Stripe_param.number.length > 16) {
                var alert_1 = this.alertCtrl.create({
                    title: "Validation Error",
                    subTitle: "You credit card number exceeded digit limit",
                    buttons: ['OK']
                });
                alert_1.present(prompt);
            }
            else if (+this.Stripe_param.exp_month > 12 || +this.Stripe_param.exp_month < 1) {
                var alert_2 = this.alertCtrl.create({
                    title: "Date Error",
                    subTitle: "Invalid expiry month",
                    buttons: ['OK']
                });
                alert_2.present(prompt);
            }
            else if (+this.Stripe_param.exp_year < 2016) {
                var alert_3 = this.alertCtrl.create({
                    title: "Date Error",
                    subTitle: "Invalid expiry year",
                    buttons: ['OK']
                });
                alert_3.present(prompt);
            }
            else if (+this.Stripe_param.cvc > 999 || +this.Stripe_param.cvc < 100) {
                var alert_4 = this.alertCtrl.create({
                    title: "CVC Error",
                    subTitle: "Invalid CVC",
                    buttons: ['OK']
                });
                alert_4.present(prompt);
            }
            else {
                Stripe.card.createToken({
                    name: this.Stripe_param.name,
                    number: this.Stripe_param.number,
                    exp_month: this.Stripe_param.exp_month,
                    exp_year: this.Stripe_param.exp_year,
                    cvc: this.Stripe_param.cvc
                }, function (status, response) { return _this.stripeResponseHandler(status, response); });
            }
        }
        else {
            var loading_2 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_2.present();
            var payment = { source: this.card.stripe_card_id, plan: localStorage.getItem("subPlan") };
            this.api.subscribe(payment).subscribe(function (res) {
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                if (body.status == "406") {
                    localStorage.setItem("SubPending", "true");
                    localStorage.setItem("SubActive", body.activedate.iso);
                }
                loading_2.dismiss();
                _this.navCtrl.setRoot('SubConfirmPage');
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "Error " + err.status,
                    subTitle: err.message,
                    buttons: ['OK']
                });
                alert.present(prompt);
                loading_2.dismiss();
            });
        }
    };
    SubPaymentPage.prototype.stripeResponseHandler = function (status, response) {
        var _this = this;
        if (response.error) {
            var alert_5 = this.alertCtrl.create({
                title: "Payment Error",
                subTitle: response.error.message,
                buttons: ['OK']
            });
            alert_5.present(prompt);
        }
        else {
            var token = response.id;
            //this.Stripe_token = response.id;
            console.log(token);
            var loading_3 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_3.present();
            var payment = { source: token, plan: localStorage.getItem("subPlan") };
            this.api.subscribe(payment).subscribe(function (res) {
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                if (body.status == "406") {
                    localStorage.setItem("SubPending", "true");
                    localStorage.setItem("SubActive", body.activedate.iso);
                }
                loading_3.dismiss();
                _this.navCtrl.setRoot('SubConfirmPage');
            }, function (err) {
                console.log(err);
                loading_3.dismiss();
            });
        }
    };
    SubPaymentPage.prototype.back = function () {
        this.navCtrl.setRoot('SubPaymentMethodPage');
    };
    SubPaymentPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    SubPaymentPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return SubPaymentPage;
}());
SubPaymentPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-address',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/subscription/sub-payment.html"*/'\n<!DOCTYPE html>\n<html lang="en-AU" >\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <title>Filtered water: a healthier alternative to plastic bottled water</title>\n        <meta name="title" content="water3 |     Top Up | Amount\n">\n        <meta name="description" content="water3 is revolutionising the way we drink water; via a network of innovative kiosks that provide fresh and healthy chilled spring water.">\n        <meta content="width=device-width, initial-scale=1" name="viewport">\n        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:400,700,300%7CUbuntu">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/style-1489479537.css">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/app-1486002319.css">\n        <link rel="shortcut icon" href="../favicon-1495700985.ico" type="image/x-icon">\n        <link rel="icon" href="../favicon-1495700985.ico" type="image/x-icon">\n        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n                <script type="text/javascript">\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n    ga(\'create\', \'UA-83198216-1\', \'auto\');\n    ga(\'send\', \'pageview\');\n</script>\n    </head>\n        <body class="layout-portal" overflow-scroll="true">\n        \n        <header class="page">\n        <div class="container">\n        <a class="branding" href="index.html" title="Home">\n            <img src="assets/img/water3-logo.png" alt="Water3">\n        </a>\n\n        <button class="open-navigation" type="button" aria-label="Toggle menu" (click)="openMenu()">\n            <span class="bar top"></span>\n            <span class="bar middle"></span>\n            <span class="bar bottom"></span>\n            <span class="text">Menu</span>\n        </button>\n\n        <a class="btn btn-top-up btn-circle btn-icon" (click)="toBottle()" title="Top up">\n            <span class="icon icon-top-up hidden-hover"></span>\n            <span class="icon icon-top-up-white visible-hover"></span>\n            <span class="text">Top Up</span>\n        </a>\n    </div>\n</header>\n<ion-content style="background-color: #368ba1">\n           <main class="page payment">\n        <section class="component component-section">\n            <div class="container">\n                <h1>Payment</h1>\n\n                <a class="back" (click)="back()" title="Back">\n                    <span class="icon icon-arrow-left-white"></span>\n                    <span class="text">Back</span>\n                </a>\n\n                <ol class="progression">\n                    <li class="complete"><span class="icon icon-success-white"></span></li>\n                    <li class="complete"><span class="icon icon-success-white"></span></li>\n                    <li class="complete"><span class="icon icon-success-white"></span></li>\n                    <li class="current">4</li>\n                </ol>\n            </div>\n        </section>\n\n        <div *ngIf="isLoaded && card!=null && card!=\'\'" class="content narrow">\n            <section class="component component-form">\n                                                <div class="alert alert-warning stripe" style="display: none;">\n                    <p>\n                        <span class="icon icon-warning"></span>\n                        <span class="message"></span>\n                    </p>\n                </div>\n\n                \n                    <form (ngSubmit)="postPayment()" accept-charset="UTF-8" autocomplete="off" novalidate="novalidate"><input name="_session_key" type="hidden" value="9eapdg3HQymKtxY9qQWBcPOZyA3YJbgHdrpzd65C"><input name="_token" type="hidden" value="cjloJVUWEe0mFRfYF8UH75YAPOL3iObnv5t6BZtI">\n                        <fieldset>\n                            <legend>Credit card details</legend>\n\n                            <div class="field-group">\n                                <div class="form-group">\n                                    <label for="card_number" class="sr-only">Card number</label>\n                                    <span class="icon icon-payment"></span>\n                                    <div class="form-control">\n                                        <span>**** **** **** {{card.last4}}</span>\n                                        <a class="pull-right" (click)="navCtrl.setRoot(\'SubPaymentMethodPage\')" title="Change password">Change</a>\n                                    </div>\n                                </div>\n                            </div>\n                        </fieldset>\n\n                        <fieldset>\n                            <div class="field-group emphasise">\n                                <div class="form-group">\n                                    <label for="amount">Subscription Amount</label>\n                                    <div class="total pull-right">{{ amount }}</div>\n                                </div>\n                            </div>\n                        </fieldset>\n                        <input type="hidden" name="stripe_token" [(ngModel)]="Stripe_token" value="{{card.stripe_card_id}}">\n                        <div class="form-submit-group">\n                            <button class="btn btn-d btn-block" type="submit">Confirm</button>\n                        </div>\n\n                    </form>\n\n                            </section>\n        </div>\n\n        <div *ngIf="isLoaded && (card==null || card==\'\')" class="content narrow">\n            <section class="component component-form">\n                                                <div class="alert alert-warning stripe" style="display: none;">\n                    <p>\n                        <span class="icon icon-warning"></span>\n                        <span class="message"></span>\n                    </p>\n                </div>\n\n                \n                    <form (ngSubmit)="postPayment()" accept-charset="UTF-8" id="payment" autocomplete="off" novalidate="novalidate"><input name="_session_key" type="hidden" value="yNYQcrMekutdrIyan3lL52EA1kk43LIa2ttOCOP7"><input name="_token" type="hidden" value="WzE47nwZhrOooMgmytCQVq9O6Zj1WAbkYeeYOwtU">\n                        <fieldset>\n                            <legend>Credit card details</legend>\n\n                            <div class="field-group">\n                                <div class="form-group">\n                                    <label for="cardholder_name" class="sr-only">Cardholder name</label>\n                                    <span class="icon icon-account"></span>\n                                    <ion-input [(ngModel)]="Stripe_param.name" name="name" placeholder="Cardholder Name" class="form-control" no-margin></ion-input>\n                                </div>\n\n                                <div class="form-group">\n                                    <label for="card_number" class="sr-only">Card number</label>\n                                    <span class="icon icon-payment"></span>\n                                    <ion-input [(ngModel)]="Stripe_param.number" name="number" type="number" class="form-control" placeholder="**** **** **** ****" no-margin></ion-input>\n                                </div>\n                            </div>\n                        </fieldset>\n\n                        <fieldset>\n                            <legend>Expiry date</legend>\n\n                            <div class="field-group">\n                                <div class="row">\n                                    <div class="col-xs-6">\n                                        <div class="form-group">\n                                            <label for="expiry_month" class="sr-only">Expiry month</label>\n                                            <ion-input [(ngModel)]="Stripe_param.exp_month"  name="expiry_month" class="form-control" min="1" max="12" placeholder="MM" type="number" no-margin></ion-input>\n                                        </div>\n                                    </div>\n                                    <div class="col-xs-6">\n                                        <div class="form-group">\n                                            <label for="expiry_year" class="sr-only">Expiry year</label>\n                                            <ion-input [(ngModel)]="Stripe_param.exp_year"  name="expiry_year" class="form-control" min="2017" max="2037" placeholder="YYYY" type="number" no-margin></ion-input>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </fieldset>\n\n                        <fieldset>\n                            <legend>CVC</legend>\n\n                            <div class="field-group split">\n                                <div class="form-group">\n                                    <label for="cvc" class="sr-only">CVC</label>\n                                    <ion-input [(ngModel)]="Stripe_param.cvc"  name="cvc" class="form-control" placeholder="XXX" type="text" no-margin></ion-input>\n                                </div>\n                            </div>\n                        </fieldset>\n\n                      \n                            <fieldset>\n                                <div class="field-group">\n                                    <div class="form-group toggle">\n                                        <label for="save_card">Save credit card details</label>\n                                        <input [(ngModel)]="save"  id="save_card" name="save_card" type="checkbox" value="1">\n                                        <label for="save_card" data-toggle-off="No" data-toggle-on="Yes"></label>\n                                    </div>\n                                </div>\n                            </fieldset>\n                      \n                        <fieldset>\n                            <div class="field-group emphasise">\n                                <div class="form-group">\n                                    <label for="amount">Subscription Amount</label>\n                                    <div class="total pull-right">{{ amount }}</div>\n                                </div>\n                            </div>\n                        </fieldset>\n\n                        <div class="form-submit-group">\n                            <input type="hidden" name="tokenID" [(ngModel)]="tokenID" value="">\n                            <button class="btn btn-d btn-block" type="submit">Confirm</button>\n                        </div>\n                    </form>\n\n                            </section>\n        </div>\n    </main>\n    </ion-content>\n    </body>\n\n    <script type="text/javascript" src="../themes/water3/assets/js/vendor-1495700986.js"></script>\n    <script type="text/javascript" src="../themes/water3/assets/js/app-1498189486.js"></script>\n\n</html>\n'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/subscription/sub-payment.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], SubPaymentPage);

//# sourceMappingURL=sub-payment.js.map

/***/ })

});
//# sourceMappingURL=14.main.js.map