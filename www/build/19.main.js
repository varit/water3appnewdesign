webpackJsonp([19],{

/***/ 301:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store__ = __webpack_require__(347);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorePageModule", function() { return StorePageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var StorePageModule = (function () {
    function StorePageModule() {
    }
    return StorePageModule;
}());
StorePageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__store__["a" /* StorePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__store__["a" /* StorePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__store__["a" /* StorePage */]
        ]
    })
], StorePageModule);

//# sourceMappingURL=store.module.js.map

/***/ }),

/***/ 347:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_env__ = __webpack_require__(101);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorePage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { HTTP } from '@ionic-native/http';
var StorePage = (function () {
    function StorePage(navCtrl, popCtrl, alertCtrl, platform, env, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.popCtrl = popCtrl;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.env = env;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.itemID = "1";
        this.isLoaded = false;
        this.itemAmount = localStorage.getItem("itemAmount");
        if (this.itemAmount == null || this.itemAmount <= 0)
            this.itemAmount = 0;
        else
            this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
        console.log(this.itemAmount);
        this.api.getProducts().subscribe(function (res) {
            var body = res.json();
            console.log("Response : " + status + " BODY " + JSON.stringify(body));
            _this.products = body;
            _this.isLoaded = true;
        }, function (err) {
        });
    }
    StorePage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    StorePage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    StorePage.prototype.addToBag = function () {
        console.log("Added id " + this.itemID);
        if (localStorage.getItem(this.itemID) == null) {
            localStorage.setItem(this.itemID, "1");
            console.log("item id " + this.itemID + " is qty " + localStorage.getItem(this.itemID));
            var amount = this.itemAmount + 1;
            this.itemAmount = amount;
            localStorage.setItem("itemAmount", amount.toString());
        }
        else {
            var temp = parseInt(localStorage.getItem(this.itemID));
            temp = temp + 1;
            localStorage.setItem(this.itemID, temp.toString());
            console.log("item id " + this.itemID + " is qty " + localStorage.getItem(this.itemID));
            var amount = this.itemAmount + 1;
            this.itemAmount = amount;
            localStorage.setItem("itemAmount", amount.toString());
        }
        var alert = this.alertCtrl.create({
            title: "Added to cart",
            subTitle: "You have added this item to your cart",
            buttons: ['OK']
        });
        alert.present(prompt);
    };
    StorePage.prototype.toKiosk = function () {
        this.navCtrl.setRoot('KioskPage');
    };
    StorePage.prototype.scrollTo = function () {
        this.content.scrollTo(0, 2500, 1500);
    };
    return StorePage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Content */])
], StorePage.prototype, "content", void 0);
StorePage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])({
        segment: 'store'
    }),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-hello-ionic',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/store/store.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a3f8b5a8192d400018cbf62" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>product detail</title>\n  <meta content="product detail" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="assets/images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div class="navbar">\n    <div class="w-container">\n      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar home w-nav">\n        <div class="nav-container"><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a>\n          <div class="menu-button w-nav-button">\n            <div (click)="openMenu()" class="w-icon-nav-menu"></div>\n          </div><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="brand-3 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n          <nav role="navigation" class="nav-menu w-nav-menu">\n            <a (click)="navCtrl.setRoot(\'StorePage\')" class="navlink w-inline-block">\n              <div>Shop</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'KioskPage\')" class="navlink w-inline-block">\n              <div>Find a kiosk</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'WhyPage\')" class="navlink w-inline-block">\n              <div>Why Water3</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="navlink w-inline-block">\n              <div>FAQ</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'ContactPage\')" class="navlink w-inline-block">\n              <div>Contact</div>\n            </a>\n\n            <div class="nav-line"></div>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'RegisterPage\')" class="nav-button register w-button">Register</a>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'LoginPage\')" class="nav-button login w-button">Login</a>\n\n            <a *ngIf="isLoggedin" (click)="navCtrl.setRoot(\'AccountPage\')" class="navlink w-inline-block">My Account</a>\n\n            <a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></nav>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ion-content *ngIf="isLoaded">\n  <div class="shop-rank-section">\n    <div class="w-container">\n      <div class="sub-nav"><a href="#" class="back-btn w-inline-block w-clearfix"><img src="assets/images/back-icon2.png" width="19.5" class="image"><div class="textback2">All bottles</div></a></div>\n      <div>\n        <div>\n          <div class="w-row">\n            <div class="w-col w-col-6">\n              <div class="product-image-gallery">\n                <div class="image-showcase"><img src="assets/images/p6.png" width="282" srcset="assets/images/p6-p-500.png 500w, assets/images/p6.png 556w" sizes="(max-width: 479px) 92vw, 282px"></div>\n                <div class="div-block-32"><a href="#" class="thumb-link w-inline-block"><img src="assets/images/p6.png" srcset="assets/images/p6-p-500.png 500w, assets/images/p6.png 556w" sizes="100vw"></a><a href="#" class="thumb-link w-inline-block"><img src="assets/images/ig02.png" srcset="images/ig02-p-500.png 500w, assets/images/ig02.png 556w" sizes="100vw"></a><a href="#" class="thumb-link w-inline-block"><img src="images/ig04.png" srcset="assets/images/ig04-p-500.png 500w, assets/images/ig04.png 556w" sizes="100vw"></a></div>\n              </div>\n            </div>\n            <div class="w-col w-col-6">\n              <div class="product-information">\n                <h3 class="h3-product-name">Blue Blush</h3>\n                <div>\n                  <h4 class="h4-price">${{products[itemID].salePrice}}</h4>\n                  <div class="text-block-10">DELIVERY</div>\n                </div>\n                <div data-delay="0" class="select-color-btn w-dropdown">\n                  <div class="dropdown-toggle-2 w-dropdown-toggle">\n                  <ion-item style="background: transparent;">\n                    <ion-select [(ngModel)]="itemID" interface="popover" class="addressSelect">\n                        <ion-option *ngFor="let p of products" value="{{p.id}}" >{{p.model}}</ion-option>\n                    </ion-select>\n                </ion-item>\n                </div>\n                </div><a (click)="addToBag()" class="button-3 w-button">Add to bag</a></div>\n                <div>\n\n<b>It’s all about the bottle.</b><br><br>\n\nOur double-walled, vacuum sealed and insulated stainless steel bottles are perfect for keeping your water cold (or hot) for up to 12 hours.<br>\n-       They are made of tough reusable (and recyclable) stainless steel and won’t get damp condensation on the exterior. So your dry backpack and the stuff in it stays dry.<br>\n-       Each bottle also features an intuitive RFID chip in the lid, allowing you to refill quickly and easily with one easy swipe.<br>\n-       Every refill stops another plastic bottle from entering the environment. Save the whales! (and all the other fish too. And the reef. And narwhals. And...)<br>\n-       Best of all, they’re a high quality design, functional and ready to be exposed to your next adventure. <br><br>\n\n<b>What else?</b><br><br>\n\n-      Made of stainless steel and sustainable bamboo construction. Naturally.<br>\n-    The bamboo lidis leak proof and hides some seriously high tech contactless payment system (ie the RFID chip you use to pay for your refills).<br>\n-      Any plastic? No way (as if you had to ask), our bottles are BPA free and made of non-toxic materials.<br>\n-      Large 44mm (1.75”) opening for easy filling, pouring and super gulps. <br>\n-      You’ll only ever taste the water. <br>\n<br>\n<b>Look after your bottle, and it will look after you.</b><br>\n<br>\nFollow these handy tips:<br>\n1.  Hand wash with warm soapy water.<br>\n2.  Do not put the lid in dishwasher, remember it contains technology.<br>\n3.  Avoid direct sunlight if you can - to keep your water icy cold.<br>\n<br>\n<b>Why Water3?</b><br>\n<br>\nWater3 wants to help change your old plastic water bottle habits. So we’ve made everything as simple as possible. Our refill stations are in all the places you’ll need it most. How do you like your water? Still? Or sparkling? Water3 can also supply your very own reusable stainless steel water bottle, if you need one. Oh, and we nearly forgot to mention, spring water refills start from just $1!\n\n\n\n                </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class="container-2 w-container">\n      <div class="w-clearfix">\n        <h6 class="h6">View bottle on instagram</h6><a href="#" class="ig-hastag-link">#unbottledlife</a></div>\n      <div><a href="#" class="link-block w-inline-block"><img src="assets/images/ig01.png" width="278" srcset="assets/images/ig01-p-500.png 500w, assets/images/ig01.png 556w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 23vw, (max-width: 991px) 172px, 225px"></a><a href="#" class="link-block w-inline-block"><img src="assets/images/ig03.png" width="278" srcset="assets/images/ig03-p-500.png 500w, assets/images/ig03.png 556w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 23vw, (max-width: 991px) 172px, 225px"></a><a href="#" class="link-block w-inline-block"><img src="assets/images/ig02.png" width="278" srcset="assets/images/ig02-p-500.png 500w, assets/images/ig02.png 556w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 23vw, (max-width: 991px) 172px, 225px"></a><a href="#" class="link-block w-inline-block"><img src="assets/images/ig04.png" width="278" srcset="assets/images/ig04-p-500.png 500w, assets/images/ig04.png 556w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 23vw, (max-width: 991px) 172px, 225px"></a></div>\n    </div>\n  </div>\n  <div class="footer-section" style="height: 250px;">\n    <div class="w-container">\n      <div class="row w-row">\n        <div class="w-col w-col-3">\n          <div><img src="assets/images/Logo.png" width="121.5" class="image-3">\n            <div class="text-copy-right _1">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n        <div class="w-col w-col-6">\n          <div class="w-row">\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-18">\n                <a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="footer-link">Home</a>\n                <a (click)="navCtrl.setRoot(\'StorePage\')" class="footer-link">Shop</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Find a kiosk</a>\n                <a (click)="navCtrl.setRoot(\'WhyPage\')" class="footer-link">Why Water3</a>\n                <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="footer-link">FAQ</a>\n                <a (click)="navCtrl.setRoot(\'ContactPage\')" class="footer-link">Contact</a></div>\n            </div>\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-19">\n                <a href="#" class="footer-link">Shipping</a>\n                <a (click)="navCtrl.setRoot(\'TermsPage\')" class="footer-link">Term of Services</a>\n                <a (click)="navCtrl.setRoot(\'PrivacyPage\')" class="footer-link">Privacy Policy</a></div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-21">\n            <a href="https://www.facebook.com/water3global" class="social-link w-inline-block"><img src="assets/images/fb-icon.png" class="social-icon"></a>\n            <a href="https://www.instagram.com/_water3_" class="social-link w-inline-block"><img src="assets/images/ig-icon.png" class="social-icon"></a>\n            <a href="https://twitter.com/water3_" class="social-link w-inline-block"><img src="assets/images/twitter-icon.png" class="social-icon"></a>\n            <a href="https://www.linkedin.com/company/water3" class="social-link w-inline-block"><img src="assets/images/linkedin-icon.png" class="social-icon"></a>\n            <a href="https://www.youtube.com/channel/UCCPqmAd8l2dCmH2iGXbSFvg" class="social-link w-inline-block"><img src="assets/images/youtube-icon.png" class="social-icon"></a>\n            <a href="mailto:info@water3.com.au" class="footer-email">info@water3.com.au</a>\n            <div class="text-block-5">+61 410 000 000</div>\n            <div class="footer-address">12 New Street Name, Suburb<br>QLD 4000, Australia</div>\n            <div class="text-copy-right _2">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="assets/js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content>\n</body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/store/store.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__services_env__["a" /* ENV */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], StorePage);

//# sourceMappingURL=store.js.map

/***/ })

});
//# sourceMappingURL=19.main.js.map