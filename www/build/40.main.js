webpackJsonp([40],{

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__update__ = __webpack_require__(326);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BottleUpdatePageModule", function() { return BottleUpdatePageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BottleUpdatePageModule = (function () {
    function BottleUpdatePageModule() {
    }
    return BottleUpdatePageModule;
}());
BottleUpdatePageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__update__["a" /* BottleUpdatePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__update__["a" /* BottleUpdatePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__update__["a" /* BottleUpdatePage */]
        ]
    })
], BottleUpdatePageModule);

//# sourceMappingURL=update.module.js.map

/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BottleUpdatePage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the BottlePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var BottleUpdatePage = (function () {
    function BottleUpdatePage(navCtrl, loadingCtrl, alertCtrl, navParams, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.action = "edit";
        if (this.api.checkSession()) {
            var loading_1 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_1.present();
            this.api.getUserBalance().subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                _this.balance = body.regular_credit_balance + body.subscription_credit_balance;
                _this.balance = _this.balance.toFixed(2);
            }, function (err) {
            });
            this.api.getBottle(localStorage.getItem("bottle_no")).subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                _this.bottle = body;
                _this.isLoaded = true;
                loading_1.dismiss();
            }, function (err) {
            });
        }
    }
    BottleUpdatePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TopupPage');
    };
    BottleUpdatePage.prototype.toDelete = function () {
        this.action = "delete";
    };
    BottleUpdatePage.prototype.toEdit = function () {
        this.action = "edit";
    };
    BottleUpdatePage.prototype.performUpdate = function () {
        var _this = this;
        this.api.updateBottle(this.bottle).subscribe(function (res) {
            var status = res.status;
            var body = res.json();
            console.log("Response : " + status + " BODY " + JSON.stringify(body));
            var alert = _this.alertCtrl.create({
                title: "Success",
                subTitle: "New bottle name has been saved",
                buttons: ['OK']
            });
            alert.present(prompt);
        }, function (err) {
        });
    };
    BottleUpdatePage.prototype.performDelete = function () {
        var _this = this;
        this.api.deleteBottle(this.bottle).subscribe(function (res) {
            var status = res.status;
            var body = res.json();
            console.log("Response : " + status + " BODY " + JSON.stringify(body));
            var alert = _this.alertCtrl.create({
                title: "Success",
                subTitle: "The bottle has been removed",
                buttons: ['OK']
            });
            alert.present(prompt);
            _this.navCtrl.setRoot('BottlePage');
        }, function (err) {
        });
    };
    BottleUpdatePage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    BottleUpdatePage.prototype.back = function () {
        this.navCtrl.setRoot('BottlePage');
    };
    BottleUpdatePage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return BottleUpdatePage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */])
], BottleUpdatePage.prototype, "slides", void 0);
BottleUpdatePage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-bottle',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/bottle/update.html"*/'<!DOCTYPE html>\n<html lang="en-AU" >\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <title>Filtered water: a healthier alternative to plastic bottled water</title>\n        <meta name="title" content="water3 | think better. be better. do more">\n        <meta name="description" content="water3 is revolutionising the way we drink water; via a network of innovative kiosks that provide fresh and healthy chilled spring water.">\n        <meta content="width=device-width, initial-scale=1" name="viewport">\n        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:400,700,300%7CUbuntu">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/style-1489479537.css">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/app-1486002319.css">\n        <link rel="shortcut icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <link rel="icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n                <script type="text/javascript">\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n    ga(\'create\', \'UA-83198216-1\', \'auto\');\n    ga(\'send\', \'pageview\');\n</script>\n    </head>\n        <body class="layout-portal" overflow-scroll="true">\n        \n        <header class="page">\n        <div class="container">\n        <a class="branding" href="index.html" title="Home">\n            <img src="assets/img/water3-logo.png" alt="Water3">\n        </a>\n\n        <button class="open-navigation" type="button" aria-label="Toggle menu" (click)="openMenu()">\n            <span class="bar top"></span>\n            <span class="bar middle"></span>\n            <span class="bar bottom"></span>\n            <span class="text">Menu</span>\n        </button>\n\n        <a class="btn btn-top-up btn-circle btn-icon" (click)="toBottle()" title="Top up">\n            <span class="icon icon-top-up hidden-hover"></span>\n            <span class="icon icon-top-up-white visible-hover"></span>\n            <span class="text">Top Up</span>\n        </a>\n    </div>\n</header>\n        <ion-content style="background-color: #368ba1">\n        <main class="page bottle" *ngIf="isLoaded">\n        <section class="component component-section">\n            <div class="container">\n                <h1>Edit Bottle</h1>\n\n                <a class="back" (click)="back()" title="Back">\n                    <span class="icon icon-arrow-left-white"></span>\n                    <span class="text">Back</span>\n                </a>\n            </div>\n        </section>\n\n        <div *ngIf="action==\'edit\'" class="content narrow">\n            <section class="component component-form">\n                <img src="assets/img/bottle-large.png" alt="water3 bottle">\n\n                <form (ngSubmit)="performUpdate()" accept-charset="UTF-8" novalidate="novalidate" #loginForm="ngForm">\n                    <div class="field-group">\n                        <div class="form-group">\n                            <label for="bottle_name" class="sr-only">Bottle Name</label>\n                            <input class="form-control" placeholder="Enter your bottle name" name="bottle_name" type="text" [(ngModel)]="bottle.bottle_name" id="bottle_name">\n                        </div>\n                    </div>\n\n                    <div class="form-submit-group">\n                        <button class="btn btn-d btn-block" type="submit">Save</button>\n                    </div>\n\n                    <p class="secondary-action"><a (click)="toDelete()" title="Remove bottle" data-toggle="modal" data-target="#modal-bottle-delete">Remove bottle?</a></p>\n                </form>\n            </section>\n        </div>\n\n\n        <div *ngIf="action==\'delete\'" id="content narrow">\n            <form accept-charset="UTF-8" novalidate="novalidate" #loginForm="ngForm">\n\n                <div class="modal-dialog">\n                    <div class="modal-content">\n                        <div class="modal-header">\n                            <h4 class="modal-title">Confirm Removal</h4>\n                        </div>\n\n                        <div class="modal-body">\n                            <p>Are you sure you want to remove this bottle from your account?</p>\n                        </div>\n\n                        <div class="modal-footer">\n                            <button (click)="performDelete()" class="btn btn-sm btn-c">Remove</button>\n                            <button (click)="toEdit()" class="btn btn-sm btn-c">Cancel</button>\n                        </div>\n                    </div>\n                </div>\n            </form>\n        </div>\n    </main>\n</ion-content>\n    </body>\n\n    <script type="text/javascript" src="assets/js/vendor-1486002319.js"></script>\n        <script type="text/javascript" src="assets/js/app-1486002319.js"></script>\n\n</html>\n'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/bottle/update.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], BottleUpdatePage);

//# sourceMappingURL=update.js.map

/***/ })

});
//# sourceMappingURL=40.main.js.map