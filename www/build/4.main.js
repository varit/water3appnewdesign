webpackJsonp([4],{

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__show__ = __webpack_require__(363);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowPageModule", function() { return ShowPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ShowPageModule = (function () {
    function ShowPageModule() {
    }
    return ShowPageModule;
}());
ShowPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__show__["a" /* ShowPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__show__["a" /* ShowPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__show__["a" /* ShowPage */]
        ]
    })
], ShowPageModule);

//# sourceMappingURL=show.module.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_helper__ = __webpack_require__(200);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShowPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TransactionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ShowPage = (function () {
    function ShowPage(navCtrl, navParams, helper) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.helper = helper;
        this.transaction = navParams.get('transaction');
        console.log(this.transaction);
        this.transaction.balance = parseFloat(this.transaction.balance).toFixed(2);
    }
    ShowPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ShowPage');
    };
    ShowPage.prototype.pop = function () {
        this.navCtrl.pop();
    };
    return ShowPage;
}());
ShowPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-transaction',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/transaction/show.html"*/'<!DOCTYPE html>\n<html lang="en-AU" >\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <title>Filtered water: a healthier alternative to plastic bottled water</title>\n        <meta name="title" content="water3 | think better. be better. do more">\n        <meta name="description" content="water3 is revolutionising the way we drink water; via a network of innovative kiosks that provide fresh and healthy chilled spring water.">\n        <meta content="width=device-width, initial-scale=1" name="viewport">\n        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:400,700,300%7CUbuntu">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/style-1489479537.css">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/app-1486002319.css">\n        <link rel="shortcut icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <link rel="icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n                <script type="text/javascript">\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n    ga(\'create\', \'UA-83198216-1\', \'auto\');\n    ga(\'send\', \'pageview\');\n</script>\n    </head>\n        <body class="layout-default" overflow-scroll="true">\n        <ion-content>\n        <main class="page white">\n        <section class="component-section">\n            <div class="container">\n                <h1>Transaction History</h1>\n\n                <a class="back" (click)="pop()" title="Back">\n                    <span class="icon icon-arrow-left-white"></span>\n                    <span class="text">Back</span>\n                </a>\n            </div>\n        </section>\n\n        <div class="content narrow">\n            <section class="component component-transaction">\n                <h2 style="color:black;">{{ helper.convert_tz(transaction.date.text,true) }}</h2>\n\n                <div class="transaction-status">\n                    <div class="list-group">\n                        <div class="list-group-item">\n                            <div class="title">\n                                <p>Details</p>\n                            </div>\n                            <div class="value">\n                                <p>{{ transaction.type }}</p>\n                            </div>\n                        </div>\n                        <div class="list-group-item">\n                            <div class="title">\n                                <p>Description</p>\n                            </div>\n                            <div class="value">\n                                <p>{{ transaction.description }}</p>\n                            </div>\n                        </div>\n                        <div class="list-group-item">\n                            <div class="title">\n                                <p>Amount</p>\n                            </div>\n                            <div class="value">\n                                <p>${{transaction.credit}}</p>\n                            </div>\n                        </div>\n\n                        <div class="list-group-item">\n                            <div class="title">\n                                <p>Status</p>\n                            </div>\n                            <div class="value"><p>\n                                <span *ngIf="transaction.status==\'ACCEPTED\'" class="icon icon-checked inline"></span>\n                                 <span *ngIf="transaction.status!=\'ACCEPTED\'" class="icon icon-failure-circle-gradient inline"></span>\n                                {{transaction.status}}</p>\n                            </div>\n                        </div>\n			<div class="list-group-item">\n                            <div class="title">\n                                <p>Balance after topup</p>\n                            </div>\n                            <div class="value"><p>${{transaction.balance}}</p>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </section>\n        </div>\n    </main>\n</ion-content>\n        <section class="component component-bottom-button responsive">\n    <div class="button-container">\n        <a class="btn btn-icon-inline btn-square btn-facebook-messenger" href="https://m.me/water3global" target="_blank">\n            <span class="icon icon-facebook-messenger"></span>\n            Message water3\n        </a>\n    </div>\n</section>\n    </body>\n\n    <script type="text/javascript" src="assets/js/vendor-1486002319.js"></script>\n        <script type="text/javascript" src="assets/js/app-1486002319.js"></script>\n\n</html>\n'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/transaction/show.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_helper__["a" /* Helper */]])
], ShowPage);

//# sourceMappingURL=show.js.map

/***/ })

});
//# sourceMappingURL=4.main.js.map