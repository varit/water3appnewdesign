webpackJsonp([27],{

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register__ = __webpack_require__(338);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegisterPageModule = (function () {
    function RegisterPageModule() {
    }
    return RegisterPageModule;
}());
RegisterPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */]
        ]
    })
], RegisterPageModule);

//# sourceMappingURL=register.module.js.map

/***/ }),

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**

/**
 * Generated class for the AccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(navCtrl, platform, event, alertCtrl, loadingCtrl, navParams, api, menuCtrl) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.event = event;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.checked = false;
        this.accountInfo = {
            first_name: '',
            last_name: '',
            email: '',
            phone: '',
            password: '',
            currency: '',
            sex: '',
            birth_date: ''
        };
        this.birth = { day: 0, month: 0, year: 0 };
        this.referCode = this.navParams.get('referral');
        if (this.referCode == 'standard' || this.referCode == ":referral") {
            this.referCode = null;
        }
        else if (this.referCode != null && this.referCode != 'standard') {
            this.performCheck(true);
        }
        this.itemAmount = localStorage.getItem("itemAmount");
        if (this.itemAmount == null || this.itemAmount <= 0)
            this.itemAmount = 0;
        else
            this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Register');
    };
    RegisterPage.prototype.performRegister = function () {
        var _this = this;
        if (this.validate()) {
            this.accountInfo.birth_date = this.birth.day + "/" + this.birth.month + "/" + this.birth.year;
            var loading_1 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_1.present();
            this.api.AdminLogin().subscribe(function (res) {
                var body = res.json();
                localStorage.setItem("token", body.access_token);
                _this.api.register(_this.accountInfo).subscribe(function (res) {
                    var body = res.json();
                    console.log("Response : " + status + " BODY " + JSON.stringify(body));
                    localStorage.setItem('email', _this.accountInfo.email);
                    localStorage.setItem('password', _this.accountInfo.password);
                    var data = {
                        email_to: _this.accountInfo.email,
                        email_from: "noreply@water3.com.au",
                        subject: "Welcome to the water revolution!",
                        template_file: "new_account",
                        params: { custoner_name: _this.accountInfo.first_name + " " + _this.accountInfo.last_name }
                    };
                    _this.api.useReferCode(_this.referCode).subscribe(function (res) {
                        console.log("referral updated");
                    });
                    _this.api.sendEmail(data).subscribe(function (res) {
                        console.log(res.json());
                    });
                    _this.api.authenticate().subscribe(function (res) {
                        var status = res.status;
                        var body = res.json();
                        console.log("Response : " + status + " BODY " + JSON.stringify(body));
                        localStorage.setItem("token", body.access_token);
                        localStorage.setItem("username", body.username);
                        localStorage.setItem("isLoggedin", "1");
                        var expiry = new Date();
                        localStorage.setItem("expiry", new Date(expiry.getTime() + (_this.api.expiry * 1000 * 60)).toString());
                        _this.event.publish('tokenUpdated', body.access_token);
                        _this.event.publish('isLoggedIn', "true");
                        console.log("Token = " + localStorage.getItem('token'));
                        console.log("Registered = " + expiry.toString());
                        console.log("Expire = " + localStorage.getItem('expiry'));
                        _this.navCtrl.setRoot('AccountPage');
                    }, function (err) {
                        var alert = _this.alertCtrl.create({
                            title: "Error " + err.status,
                            subTitle: "Error logging in, please try again",
                            buttons: ['OK']
                        });
                        alert.present(prompt);
                        _this.navCtrl.setRoot('HelloIonicPage');
                    });
                    loading_1.dismiss();
                }, function (err) {
                    var message;
                    if (err.statis == 409) {
                        message = "This email address is already registered.";
                    }
                    else {
                        message = "An error was occured trying to register you account, please try again later.";
                    }
                    var alert = _this.alertCtrl.create({
                        title: "Error " + err.status,
                        subTitle: message,
                        buttons: ['OK']
                    });
                    alert.present(prompt);
                    loading_1.dismiss();
                });
            });
        }
    };
    RegisterPage.prototype.back = function () {
    };
    RegisterPage.prototype.performCheck = function (url) {
        var _this = this;
        if (url === void 0) { url = false; }
        if (this.referCode != null) {
            this.api.AdminLogin().subscribe(function (res) {
                var body = res.json();
                var body = res.json();
                localStorage.setItem("token", body.access_token);
                _this.api.checkReferCode(_this.referCode).subscribe(function (res) {
                    var body = res.json();
                    _this.checked = true;
                    if (!url) {
                        var alert_1 = _this.alertCtrl.create({
                            title: "Success",
                            subTitle: "Referral code is accepted",
                            buttons: ['OK']
                        });
                        alert_1.present(prompt);
                    }
                }, function (err) {
                    var body = err.json();
                    var alert = _this.alertCtrl.create({
                        title: "Error " + err.status,
                        subTitle: body.message,
                        buttons: ['OK']
                    });
                    alert.present(prompt);
                });
                localStorage.clear();
            });
        }
    };
    RegisterPage.prototype.validate = function () {
        if (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(this.accountInfo.email)) {
        }
        else {
            console.log("Email invalid");
            var alert_2 = this.alertCtrl.create({
                title: "Validation failed",
                subTitle: "Email address is invalid.",
                buttons: ['OK']
            });
            alert_2.present(prompt);
            return false;
        }
        if (this.accountInfo.password != this.confirm_pass) {
            var alert_3 = this.alertCtrl.create({
                title: "Validation failed",
                subTitle: "Your passwords do not match",
                buttons: ['OK']
            });
            alert_3.present(prompt);
            return false;
        }
        if (this.accountInfo.first_name == "" || this.accountInfo.last_name == "" || this.accountInfo.phone == "" || this.accountInfo.password == "") {
            var alert_4 = this.alertCtrl.create({
                title: "Validation failed",
                subTitle: "Please make sure you filled every field and try again.",
                buttons: ['OK']
            });
            alert_4.present(prompt);
            return false;
        }
        var year = new Date();
        if (!this.platform.is('ios')) {
            console.log("Not from iOS");
            if (this.birth.year < 1900 || this.birth.year > year.getUTCFullYear()) {
                var alert_5 = this.alertCtrl.create({
                    title: "Birth date invalid",
                    subTitle: "Invalid 'year'",
                    buttons: ['OK']
                });
                alert_5.present(prompt);
                return false;
            }
            else if (this.birth.month < 1 || this.birth.month > 12) {
                var alert_6 = this.alertCtrl.create({
                    title: "Birth date invalid",
                    subTitle: "Invalid 'month'",
                    buttons: ['OK']
                });
                alert_6.present(prompt);
                return false;
            }
            else if (this.birth.day < 1 || this.birth.day > 31) {
                var alert_7 = this.alertCtrl.create({
                    title: "Birth date invalid",
                    subTitle: "Invalid 'day'",
                    buttons: ['OK']
                });
                alert_7.present(prompt);
                return false;
            }
            else if (this.birth.month == 2 && this.birth.year % 4 == 0 && this.birth.day > 29) {
                var alert_8 = this.alertCtrl.create({
                    title: "Birth date invalid",
                    subTitle: "Invalid 'day'",
                    buttons: ['OK']
                });
                alert_8.present(prompt);
            }
            else if (this.birth.month == 2 && this.birth.year % 4 != 0 && this.birth.day > 28) {
                var alert_9 = this.alertCtrl.create({
                    title: "Birth date invalid",
                    subTitle: "Invalid 'day'",
                    buttons: ['OK']
                });
                alert_9.present(prompt);
            }
        }
        else if (this.platform.is('ios')) {
            console.log("From iOS");
            if (this.birth.day != 0 || this.birth.month != 0 || this.birth.year != 0) {
                if (this.birth.year < 1900 || this.birth.year > year.getUTCFullYear()) {
                    var alert_10 = this.alertCtrl.create({
                        title: "Birth date invalid",
                        subTitle: "Invalid 'year'",
                        buttons: ['OK']
                    });
                    alert_10.present(prompt);
                    return false;
                }
                else if (this.birth.month < 1 || this.birth.month > 12) {
                    var alert_11 = this.alertCtrl.create({
                        title: "Birth date invalid",
                        subTitle: "Invalid 'month'",
                        buttons: ['OK']
                    });
                    alert_11.present(prompt);
                    return false;
                }
                else if (this.birth.day < 1 || this.birth.day > 31) {
                    var alert_12 = this.alertCtrl.create({
                        title: "Birth date invalid",
                        subTitle: "Invalid 'day'",
                        buttons: ['OK']
                    });
                    alert_12.present(prompt);
                    return false;
                }
            }
        }
        return true;
    };
    RegisterPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    RegisterPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return RegisterPage;
}());
RegisterPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])({
        segment: 'register/:referral'
    }),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-account',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/register/register.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a3e79c280557a00017738c4" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>register</title>\n  <meta content="register" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">\n    <div class="w-container">\n      <div class="menu-button w-nav-button">\n        <div class="w-icon-nav-menu"></div>\n      </div><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="brand-5 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n      <nav role="navigation" class="nav-menu-2 w-nav-menu">\n        <div class="div-block">Already have an account?</div><a (click)="navCtrl.setRoot(\'LoginPage\')" class="nav-button w-button">Login</a>\n        \n        <a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></nav>\n\n        <a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></div>\n  </div>\n  <ion-content>\n  <div class="section-main-hero">\n    <div class="register-container w-container">\n      <div class="sub-nav"><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="back-btn w-inline-block w-clearfix"><img src="assets/images/back-icon.png" width="19.5" class="image"><div class="text-back">Back</div></a></div>\n      <div class="w-row">\n        <div class="w-col w-col-4">\n          <div class="w-form">\n            <form id="email-form" name="email-form" data-name="Email Form" class="from">\n              <h3 class="h3-register">Register to Water3</h3>\n              <div class="div-text">\n                <div class="w-row">\n                  <div class="w-col w-col-2">\n                    <div class="line"></div>\n                  </div>\n                  <div class="w-col w-col-8">\n                    <div class="text-block">enter register details below</div>\n                  </div>\n                  <div class="column w-col w-col-2">\n                    <div class="line"></div>\n                  </div>\n                </div>\n              </div><label for="email-3" class="field-label">Email Address:</label>\n              <input [(ngModel)]="accountInfo.email" type="email" class="w-input" maxlength="256" name="email" data-name="email" placeholder="Enter your email" id="email-3">\n              <label for="Mobile" class="field-label">Name :</label>\n              <input [(ngModel)]="accountInfo.first_name" type="text" class="w-input" maxlength="256" name="Mobile" data-name="Mobile" placeholder="First Name" id="first_name">\n              <input [(ngModel)]="accountInfo.last_name" type="text" class="w-input" maxlength="256" name="Mobile" data-name="Mobile" placeholder="Last Name" id="last_name">\n              <label for="Mobile" class="field-label">Mobile:</label>\n              <input [(ngModel)]="accountInfo.phone" type="text" class="w-input" maxlength="256" name="Mobile" data-name="Mobile" placeholder="Enter your mobile number" id="Mobile">\n              <label for="Password" class="field-label">Password:</label>\n              <input [(ngModel)]="accountInfo.password" type="password" class="w-input" maxlength="256" name="Password" data-name="Password" placeholder="Password" id="Password">\n              <label for="Confirm-Password" class="field-label">Confirm Password:</label>\n              <input [(ngModel)]="confirm_pass" type="password" class="w-input" maxlength="256" name="Confirm-Password" data-name="Confirm Password:" placeholder="Confirm Password:" id="Confirm-Password">\n              <label for="Confirm-Password" class="field-label">Birthdate:</label>\n              <select class="w-input" style="width: 80px; display:initial;" name="day" [(ngModel)]="birth.day">\n                <option value="0" selected>Day</option>\n                <option value="1" >1</option>\n                <option value="2" >2</option>\n                <option value="3" >3</option>\n                <option value="4" >4</option>\n                <option value="5" >5</option>\n                <option value="6" >6</option>\n                <option value="7" >7</option>\n                <option value="8" >8</option>\n                <option value="9" >9</option>\n                <option value="10" >10</option>\n                <option value="11" >11</option>\n                <option value="12" >12</option>\n                <option value="13" >13</option>\n                <option value="14" >14</option>\n                <option value="15" >15</option>\n                <option value="16" >16</option>\n                <option value="17" >17</option>\n                <option value="18" >18</option>\n                <option value="19" >19</option>\n                <option value="20" >20</option>\n                <option value="21" >22</option>\n                <option value="22" >23</option>\n                <option value="23" >24</option>\n                <option value="25" >25</option>\n                <option value="26" >26</option>\n                <option value="27" >27</option>\n                <option value="28" >28</option>\n                <option *ngIf="birth.month ==2 && birth.year%4==0" value="29" >29</option>\n                <option *ngIf="birth.month !=2" value="29" >29</option>\n                <option *ngIf="birth.month !=2" value="30" >30</option>\n                <option *ngIf="birth.month==1 || birth.month==3 || birth.month==5 || birth.month==7 || birth.month==8 || birth.month==10 || birth.month==12" value="31" >31</option>\n              </select>\n              <select class="w-input" style="width: 80px; display:initial;" name="month" [(ngModel)]="birth.month">\n                <option value="0" selected>Month</option>\n                <option value="1" >Jan</option>\n                <option value="2" >Feb</option>\n                <option value="3" >Mar</option>\n                <option value="4" >Apr</option>\n                <option value="5" >May</option>\n                <option value="6" >Jun</option>\n                <option value="7" >Jul</option>\n                <option value="8" >Aug</option>\n                <option value="9" >Sep</option>\n                <option value="10" >Oct</option>\n                <option value="11" >Nov</option>\n                <option value="12" >Dec</option>\n              </select>\n              <select class="w-input" style="width: 80px; display:initial;" name="year" [(ngModel)]="birth.year">\n                <option value="0" selected>Year</option>\n                <option value="1950" >1950</option>\n                <option value="1951" >1951</option>\n                <option value="1952" >1952</option>\n                <option value="1953" >1953</option>\n                <option value="1954" >1954</option>\n                <option value="1955" >1955</option>\n                <option value="1956" >1956</option>\n                <option value="1957" >1957</option>\n                <option value="1958" >1958</option>\n                <option value="1959" >1959</option>\n                <option value="1960" >1960</option>\n                <option value="1961" >1961</option>\n                <option value="1962" >1962</option>\n                <option value="1963" >1963</option>\n                <option value="1964" >1964</option>\n                <option value="1965" >1965</option>\n                <option value="1966" >1966</option>\n                <option value="1967" >1967</option>\n                <option value="1968" >1968</option>\n                <option value="1969" >1969</option>\n                <option value="1970" >1970</option>\n                <option value="1971" >1971</option>\n                <option value="1972" >1972</option>\n                <option value="1973" >1973</option>\n                <option value="1974" >1974</option>\n                <option value="1975" >1975</option>\n                <option value="1976" >1976</option>\n                <option value="1977" >1977</option>\n                <option value="1978" >1978</option>\n                <option value="1979" >1979</option>\n                <option value="1980" >1980</option>\n                <option value="1981" >1981</option>\n                <option value="1982" >1982</option>\n                <option value="1983" >1983</option>\n                <option value="1984" >1984</option>\n                <option value="1985" >1985</option>\n                <option value="1986" >1986</option>\n                <option value="1987" >1987</option>\n                <option value="1988" >1988</option>\n                <option value="1989" >1989</option>\n                <option value="1990" >1990</option>\n                <option value="1991" >1991</option>\n                <option value="1992" >1992</option>\n                <option value="1993" >1993</option>\n                <option value="1994" >1994</option>\n                <option value="1995" >1995</option>\n                <option value="1996" >1996</option>\n                <option value="1997" >1997</option>\n                <option value="1998" >1998</option>\n                <option value="1999" >1999</option>\n                <option value="2000" >2000</option>\n                <option value="2001" >2001</option>\n                <option value="2002" >2002</option>\n                <option value="2003" >2003</option>\n                <option value="2004" >2004</option>\n                <option value="2005" >2005</option>\n                <option value="2006" >2006</option>\n                <option value="2007" >2007</option>\n                <option value="2008" >2008</option>\n                <option value="2009" >2009</option>\n                <option value="2010" >2010</option>\n                <option value="2011" >2011</option>\n                <option value="2012" >2012</option>\n                <option value="2013" >2013</option>\n                <option value="2014" >2014</option>\n                <option value="2015" >2015</option>\n                <option value="2016" >2016</option>\n                <option value="2017" >2017</option>\n                <option value="2018" >2018</option>\n\n              </select>\n              <label for="Gender" class="field-label">Gender:</label>\n              <input id="gender_male" name="gender" type="radio" value="MALE" [(ngModel)]="accountInfo.sex">&nbsp; Male\n              <input id="gender_female" name="gender" type="radio" value="FEMALE" [(ngModel)]="accountInfo.sex">&nbsp; Female\n\n              <label for="Currency" class="field-label">Default currency:</label>\n              <select class="w-input" id="currency" name="currency" [(ngModel)]="accountInfo.currency">\n                <option value="AUD">AUD</option>\n                <option value="USD">USD</option>\n              </select>\n\n              <input type="submit" (click)="performRegister()" value="Create an account" data-wait="Please wait..." class="resgister-btn w-button">\n              <div class="text-term-condition">By signing up I agree to the terms of <br> service and privacy policy</div>\n            </form>\n            <div class="w-form-done">\n              <div>Thank you! Your submission has been received!</div>\n            </div>\n            <div class="w-form-fail">\n              <div>Oops! Something went wrong while submitting the form.</div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-8">\n          <div class="register-image"><img src="assets/images/image01.png" srcset="assets/images/image01-p-500.png 500w, assets/images/image01-p-800.png 800w, assets/images/image01-p-1080.png 1080w, assets/images/image01.png 1736w" sizes="(max-width: 767px) 100vw, (max-width: 991px) 478.65625px, 619.984375px"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n</ion-content>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/register/register.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], RegisterPage);

//# sourceMappingURL=register.js.map

/***/ })

});
//# sourceMappingURL=27.main.js.map