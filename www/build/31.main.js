webpackJsonp([31],{

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment__ = __webpack_require__(334);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentsPageModule", function() { return PaymentsPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaymentsPageModule = (function () {
    function PaymentsPageModule() {
    }
    return PaymentsPageModule;
}());
PaymentsPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__payment__["a" /* PaymentsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__payment__["a" /* PaymentsPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__payment__["a" /* PaymentsPage */]
        ]
    })
], PaymentsPageModule);

//# sourceMappingURL=payment.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentsPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/* Generated class for the PaymentPage page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
var PaymentsPage = (function () {
    function PaymentsPage(navCtrl, loadingCtrl, alertCtrl, navParams, api, menuCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.mode = "view";
        this.refresh();
        this.backPage = this.navParams.get('back');
    }
    PaymentsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PaymentPage');
    };
    PaymentsPage.prototype.back = function () {
        this.navCtrl.setRoot(this.backPage);
    };
    PaymentsPage.prototype.refresh = function () {
        var _this = this;
        if (this.api.checkSession()) {
            var loading_1 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_1.present();
            this.api.getUserCreditCards().subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                _this.cards = body.cards;
                _this.mode = "view";
                _this.isLoaded = true;
                loading_1.dismiss();
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "Error : " + err.status,
                    subTitle: "An error was occured",
                    buttons: ['OK']
                });
                alert.present(prompt);
                loading_1.dismiss();
                _this.back();
            });
        }
    };
    PaymentsPage.prototype.switchMode = function () {
        if (this.card_id != null && this.card_id != '' && this.mode == 'view')
            this.mode = "delete";
        else if (this.card_id != null && this.card_id != '' && this.mode == 'delete')
            this.mode = 'view';
    };
    PaymentsPage.prototype.performDelete = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({ content: "Please wait..." });
        loading.present();
        this.api.deleteCard(this.card_id).subscribe(function (res) {
            var status = res.status;
            var body = res.json();
            console.log("Response : " + status + " BODY " + JSON.stringify(body));
            var alert = _this.alertCtrl.create({
                title: "Success",
                subTitle: "Card removal successful",
                buttons: ['OK']
            });
            alert.present(prompt);
            loading.dismiss();
            _this.refresh();
        }, function (err) {
            var alert = _this.alertCtrl.create({
                title: "Error : " + err.status,
                subTitle: "An error was occured",
                buttons: ['OK']
            });
            alert.present(prompt);
            loading.dismiss();
        });
    };
    PaymentsPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    PaymentsPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return PaymentsPage;
}());
PaymentsPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])({
        segment: 'payments'
    }),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-payment',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/payment/payment.html"*/'\n<!DOCTYPE html>\n<html lang="en-AU" >\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <title>Filtered water: a healthier alternative to plastic bottled water</title>\n        <meta name="title" content="water3 |     Top Up | Amount\n">\n        <meta name="description" content="water3 is revolutionising the way we drink water; via a network of innovative kiosks that provide fresh and healthy chilled spring water.">\n        <meta content="width=device-width, initial-scale=1" name="viewport">\n        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:400,700,300%7CUbuntu">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/style-1489479537.css">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/app-1486002319.css">\n        <link rel="shortcut icon" href="../favicon-1495700985.ico" type="image/x-icon">\n        <link rel="icon" href="../favicon-1495700985.ico" type="image/x-icon">\n        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n                <script type="text/javascript">\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n    ga(\'create\', \'UA-83198216-1\', \'auto\');\n    ga(\'send\', \'pageview\');\n</script>\n    </head>\n        <body class="layout-portal" overflow-scroll="true">\n        \n        <header class="page">\n        <div class="container">\n        <a class="branding" href="index.html" title="Home">\n            <img src="assets/img/water3-logo.png" alt="Water3">\n        </a>\n\n        <button class="open-navigation" type="button" aria-label="Toggle menu" (click)="openMenu()">\n            <span class="bar top"></span>\n            <span class="bar middle"></span>\n            <span class="bar bottom"></span>\n            <span class="text">Menu</span>\n        </button>\n\n        <a class="btn btn-top-up btn-circle btn-icon" (click)="toBottle()" title="Top up">\n            <span class="icon icon-top-up hidden-hover"></span>\n            <span class="icon icon-top-up-white visible-hover"></span>\n            <span class="text">Top Up</span>\n        </a>\n    </div>\n</header>\n<ion-content style="background-color: #368ba1">\n <main class="page">\n    <section class="component component-section">\n        <div class="container">\n            <h1>Payment</h1>\n\n            <a class="back" (click)="back()" title="Back">\n                    <span class="icon icon-arrow-left-white"></span>\n                    <span class="text">Back</span>\n                </a>\n        </div>\n    </section>\n\n    <div *ngIf="(isLoaded) && mode==\'view\'" class="content narrow" style="max-width: 350px;"> \n        <section class="component component-form">\n             <div class="alert alert-warning stripe" style="display: none;">\n                <p>\n                    <span class="icon icon-warning"></span>\n                    <span class="message"></span>\n                </p>\n             </div>\n\n             <form *ngIf="cards!=null && cards!=\'\' && mode==\'view\'" (ngSubmit)="switchMode()" accept-charset="UTF-8"><input name="_session_key" type="hidden" value="5dEXSIiFZTVry2ow0SxlsbaPZkxlcuxOyb4yJVe9"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="Dvxd4l6Xx1Mn6Igp5Ux7i7IIFQGChcxsgl1ITAG5">\n                 <div class="methods">\n                        <div class="row">\n                            <div class="col-md-12 ">\n\n                    			<fieldset>\n                        		<legend>Credit card details</legend>\n                          \n\n                                <div class="field-group">\n                                                                                    \n                                    <div *ngFor="let c of cards" class="form-group radio">\n                                        <span class="icon icon-payment"></span>\n                                        <input id="{{c.stripe_card_id}}" name="card_id" [(ngModel)]="card_id" type="radio" value="{{c.stripe_card_id}}">\n                                        <label for="{{c.stripe_card_id}}">**** **** **** {{c.last4}} EXPIRE {{c.exp_month}}/{{c.exp_year}}</label>\n                                    </div>\n                                            \n                                </div>\n\n                    			</fieldset>\n\n                   				<div class="modal-footer">\n                        			<button type="submit" class="btn btn-sm btn-c">Remove</button>\n                    			</div>\n                    		</div>\n                    	</div>\n                </div>\n            </form>\n            <div *ngIf="cards==null || cards==\'\'" class="transactions-empty">\n	            <h2> No credit card</h2>\n	            <p> Sorry you don\'t have any credit card </p>\n	            <p> You can save your new credit card while topup </p>\n			</div>\n        </section>\n     </div>\n\n     <div *ngIf="mode==\'delete\'" class="content narrow">\n        <form accept-charset="UTF-8" novalidate="novalidate" #loginForm="ngForm">\n\n                <div class="modal-dialog">\n                    <div class="modal-content">\n                        <div class="modal-header">\n                            <h4 class="modal-title">Confirm Removal</h4>\n                        </div>\n\n                        <div class="modal-body">\n                            <p>Are you sure you want to remove this card from your payment method?</p>\n                        </div>\n\n                        <div class="modal-footer">\n                            <button (click)="performDelete()" class="btn btn-sm btn-c">Remove</button>\n                            <button (click)="switchMode()" class="btn btn-sm btn-c">Cancel</button>\n                        </div>\n                    </div>\n                </div>\n            </form>\n    </div>\n    </main>\n    </ion-content>\n    </body>\n\n    <script type="text/javascript" src="../themes/water3/assets/js/vendor-1495700986.js"></script>\n        <script type="text/javascript" src="../themes/water3/assets/js/app-1498189486.js"></script>\n\n</html>\n'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/payment/payment.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], PaymentsPage);

//# sourceMappingURL=payment.js.map

/***/ })

});
//# sourceMappingURL=31.main.js.map