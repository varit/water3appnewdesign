webpackJsonp([23],{

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cart__ = __webpack_require__(342);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartPageModule", function() { return CartPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CartPageModule = (function () {
    function CartPageModule() {
    }
    return CartPageModule;
}());
CartPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__cart__["a" /* CartPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cart__["a" /* CartPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__cart__["a" /* CartPage */]
        ]
    })
], CartPageModule);

//# sourceMappingURL=cart.module.js.map

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_env__ = __webpack_require__(101);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { HTTP } from '@ionic-native/http';
var CartPage = (function () {
    function CartPage(navCtrl, event, popCtrl, platform, env, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.event = event;
        this.popCtrl = popCtrl;
        this.platform = platform;
        this.env = env;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.itemList = [];
        this.totalPrice = 0;
        this.isLoaded = false;
        this.message = "Waiting command";
        this.token = "0";
        this.displayLog = "0";
        if (localStorage.getItem("isLoggedin") == "1")
            this.isLoggedin = true;
        else
            this.isLoggedin = false;
        this.itemAmount = localStorage.getItem("itemAmount");
        if (this.itemAmount == null || this.itemAmount <= 0)
            this.itemAmount = 0;
        else
            this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
        console.log(this.itemAmount);
        for (var i = 0; i < 99; i++) {
            if (localStorage.getItem(i.toString()) != null) {
                var item = i;
                this.api.getProductsByID(item).subscribe(function (res) {
                    var body = res.json();
                    console.log(body);
                    var item_id = body.id;
                    var item_qty = localStorage.getItem(item_id.toString());
                    var item_type = body.type;
                    var item_model = body.model;
                    var item_price = body.salePrice;
                    console.log("Getting item id ", item_id);
                    _this.totalPrice = _this.totalPrice + (item_price * parseInt(item_qty));
                    console.log("Total ", _this.totalPrice);
                    localStorage.setItem("cartAmount", _this.totalPrice.toString());
                    var new_item = {
                        id: item_id.toString(),
                        type: item_type,
                        model: item_model,
                        price: item_price,
                        qty: item_qty
                    };
                    _this.itemList.push(new_item);
                    console.log("Cart item id " + item_id + " is qty " + item_qty);
                }, function (err) {
                });
            }
        }
        console.log(this.itemList);
        this.isLoaded = true;
    }
    CartPage.prototype.removeItem = function (itemID) {
        var newqty = parseInt(localStorage.getItem("itemAmount")) - parseInt(localStorage.getItem(itemID));
        console.log("newqty = " + parseInt(localStorage.getItem("itemAmount")) + " - " + localStorage.getItem(itemID));
        localStorage.setItem("itemAmount", newqty.toString());
        localStorage.removeItem(itemID);
        console.log("Removed ", itemID);
        this.event.publish('refreshPage', "true");
    };
    CartPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    CartPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    CartPage.prototype.toKiosk = function () {
        this.navCtrl.setRoot('KioskPage');
    };
    CartPage.prototype.scrollTo = function () {
        this.content.scrollTo(0, 2500, 1500);
    };
    return CartPage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Content */])
], CartPage.prototype, "content", void 0);
CartPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])({
        segment: 'cart'
    }),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-hello-ionic',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/store/cart.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a3f8b5a8192d400018cbf62" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>product detail</title>\n  <meta content="product detail" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="assets/images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div class="navbar">\n    <div class="w-container">\n      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar home w-nav">\n        <div class="nav-container"><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a>\n          <div class="menu-button w-nav-button">\n            <div (click)="openMenu()" class="w-icon-nav-menu"></div>\n          </div><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="brand-3 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n          <nav role="navigation" class="nav-menu w-nav-menu">\n            <a (click)="navCtrl.setRoot(\'StorePage\')" class="navlink w-inline-block">\n              <div>Shop</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'KioskPage\')" class="navlink w-inline-block">\n              <div>Find a kiosk</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'WhyPage\')" class="navlink w-inline-block">\n              <div>Why Water3</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="navlink w-inline-block">\n              <div>FAQ</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'ContactPage\')" class="navlink w-inline-block">\n              <div>Contact</div>\n            </a>\n\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'RegisterPage\')" class="nav-button register w-button">Register</a>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'LoginPage\')" class="nav-button login w-button">Login</a>\n\n            <a *ngIf="isLoggedin" (click)="navCtrl.setRoot(\'AccountPage\')" class="navlink w-inline-block">My Account</a>\n\n            <a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></nav>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ion-content *ngIf="isLoaded">\n  <div class="myaccount-section"><a class="link-4 cart-item">Your Cart</a>\n    <div class="w-container">\n      <div>\n        <div class="row-1 w-row">\n          <div class="w-col w-col-7 w-col-tiny-6 w-col-small-6">\n            <h3 class="h3-account">Your Cart</h3>\n          </div>\n          <div class="w-col w-col-5 w-col-tiny-6 w-col-small-6"></div>\n        </div>\n      </div>\n      <div>\n        <div *ngIf="itemList.length>0" class="row-2 w-row">\n          <div class="w-col w-col-7 w-col-small-small-stack">\n            <div class="cart-detail-box">\n              <div class="div-block-23 w-clearfix">\n                <h5 class="h5-account">2 Items Cart</h5>\n              </div>\n              <div *ngFor="let item of itemList" class="div-block-24">\n                <div class="w-row">\n                  <div class="w-col w-col-4 w-col-small-4 w-col-tiny-4"><img src="assets/images/Product-2.png"></div>\n                  <div class="w-col w-col-8 w-col-small-8 w-col-tiny-8">\n                    <div class="product-in-cart-detail">\n                      <div class="product-user-name">{{item.type}}</div>\n                      <div class="text-block-8">{{item.model}}</div>\n                      <div class="text-block-9">Qty : <strong>{{item.qty}}</strong></div>\n                      <div >$ {{item.price}}</div>\n                      <div class="div-block-48"><a (click)="removeItem(item.id)" class="remove-btn w-button">Remove</a></div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class="div-block-44 continue-box"><a (click)="navCtrl.setRoot(\'StorePage\')" class="button-4 w-button">Continue to shopping</a><br><br></div>\n          </div>\n          <div class="column-2 w-col w-col-5 w-col-small-small-stack">\n            <div class="summary-box">\n              <div class="div-block-23 w-clearfix">\n                <h5 class="h5-account">Summary</h5>\n              </div>\n              <div class="div-block-24">\n                <div class="w-row">\n                  <div class="w-col w-col-6">\n                    <div class="text-block-20">Sub-Total</div>\n                  </div>\n                  <div class="w-col w-col-6">\n                    <div class="text-block-18">$ {{totalPrice}}</div>\n                  </div>\n                </div>\n                <div>\n                  <div class="w-form">\n                    <form id="email-form" name="email-form" data-name="Email Form">\n                      <div class="w-row">\n                        <div class="w-col w-col-6">\n                          <div><strong class="bold-text">Shipping</strong></div>\n                        </div>\n                        <div class="w-col w-col-6">\n                          <div class="text-block-19">Standard : Free (Arrives 3-11 days)</div>\n                        </div>\n                      </div><label for="coupon-code">Coupon code</label><input type="text" class="w-input" maxlength="256" name="coupon-code" data-name="coupon code" placeholder="xxx" id="coupon-code">\n                      <div class="w-row">\n                        <div class="w-col w-col-6">\n                          <div class="text-block-22"><strong class="bold-text-3">Total</strong></div>\n                        </div>\n                        <div class="w-col w-col-6">\n                          <div class="text-block-21"><strong class="bold-text-2">$ {{totalPrice}}</strong></div>\n                        </div>\n                      </div>\n                      <input *ngIf="!isLoggedin" type="submit" (click)="navCtrl.setRoot(\'LoginPage\')" value="LogIn to Check Out" class="checkout-btn1 unlogin w-button">\n                      <input *ngIf="isLoggedin" type="submit" (click)="navCtrl.setRoot(\'BillingPage\')" value="Check Out" class="checkout-btn1 unlogin w-button">\n                    </form>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div *ngIf="itemList.length==0 || itemList==null" class="div-block-50">\n        <h1>So...Sad</h1>\n        <div class="text-block-27">Look like your card is empty</div><a (click)="navCtrl.setRoot(\'StorePage\')" class="continue-shopping-btn w-button">Continue to shopping</a></div>\n      </div>\n    </div>\n  </div>\n  <div class="footer-section">\n    <div class="w-container">\n      <div class="row w-row">\n        <div class="w-col w-col-3">\n          <div><img src="assets/images/Logo.png" width="121.5" class="image-3">\n            <div class="text-copy-right _1">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n        <div class="w-col w-col-6">\n          <div class="w-row">\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-18"><a href="#" class="footer-link">Home</a><a href="#" class="footer-link">Shop</a><a href="#" class="footer-link">Find a kiosk</a><a href="#" class="footer-link">Why Water3</a><a href="#" class="footer-link">FAQ</a><a href="#" class="footer-link">Contact</a></div>\n            </div>\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-19"><a href="#" class="footer-link">Shipping</a><a href="#" class="footer-link">Term of Services</a><a href="#" class="footer-link">Privacy Policy</a></div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-21"><a href="#" class="social-link w-inline-block"><img src="assets/images/fb-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/ig-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/twitter-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/linkedin-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/youtube-icon.png" class="social-icon"></a><a href="#" class="footer-email">info@watwr3.com.au</a>\n            <div class="text-block-5">+61 410 000 000</div>\n            <div class="footer-address">12 New Street Name, Suburb<br>QLD 4000, Australia</div>\n            <div class="text-copy-right _2">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="assets/js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content>\n</body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/store/cart.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__services_env__["a" /* ENV */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], CartPage);

//# sourceMappingURL=cart.js.map

/***/ })

});
//# sourceMappingURL=23.main.js.map