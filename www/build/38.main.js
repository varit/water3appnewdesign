webpackJsonp([38],{

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact__ = __webpack_require__(328);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPageModule", function() { return ContactPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ContactPageModule = (function () {
    function ContactPageModule() {
    }
    return ContactPageModule;
}());
ContactPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__contact__["a" /* ContactPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contact__["a" /* ContactPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__contact__["a" /* ContactPage */]
        ]
    })
], ContactPageModule);

//# sourceMappingURL=contact.module.js.map

/***/ }),

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ContactPage = (function () {
    function ContactPage(navCtrl, loadingCtrl, alertCtrl, navParams, api, menuCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoggedin = false;
        this.data = {
            first_name: '',
            last_name: '',
            phone: '',
            email: '',
            message: '',
            type: 'Account'
        };
        if (localStorage.getItem("isLoggedin") == "1")
            this.isLoggedin = true;
        this.itemAmount = localStorage.getItem("itemAmount");
        if (this.itemAmount == null || this.itemAmount <= 0)
            this.itemAmount = 0;
        else
            this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
    }
    ContactPage.prototype.sentEnquiry = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({ content: "Please wait..." });
        loading.present();
        if (localStorage.getItem("token") == "" || localStorage.getItem("token") == null) {
            this.api.AdminLogin().subscribe(function (res) {
                var body = res.json();
                localStorage.setItem("token", body.access_token);
                var param = {
                    email_to: "hello@water3.com.au",
                    email_from: _this.data.email,
                    subject: "New Enquiry for water3 support!",
                    template_file: "enquiry",
                    params: {
                        enquirytype: _this.data.type,
                        customer_name: _this.data.first_name + " " + _this.data.last_name,
                        phone: _this.data.phone,
                        email: _this.data.email,
                        message: _this.data.message
                    }
                };
                _this.api.sendEmail(param).subscribe(function (res) {
                    console.log(res.json());
                });
                var alert = _this.alertCtrl.create({
                    title: "Success",
                    subTitle: "We have received your enquiry. Thank you for contacting water3",
                    buttons: ['OK']
                });
                alert.present(prompt);
                _this.navCtrl.setRoot('HelloIonicPage');
                loading.dismiss();
            });
        }
        else {
            var param = {
                email_to: "hello@water3.com.au",
                email_from: this.data.email,
                subject: "New Enquiry for water3 support!",
                template_file: "enquiry",
                params: {
                    enquirytype: this.data.type,
                    customer_name: this.data.first_name + " " + this.data.last_name,
                    phone: this.data.phone,
                    email: this.data.email,
                    message: this.data.message
                }
            };
            this.api.sendEmail(param).subscribe(function (res) {
                console.log(res.json());
            });
            var alert_1 = this.alertCtrl.create({
                title: "Success",
                subTitle: "We have received your enquiry. Thank you for contacting water3",
                buttons: ['OK']
            });
            alert_1.present(prompt);
            this.navCtrl.setRoot('HelloIonicPage');
            loading.dismiss();
        }
    };
    ContactPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    return ContactPage;
}());
ContactPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-contact',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/contact/contact.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a3eb8ac7b791a0001977dda" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>contact</title>\n  <meta content="contact" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="assets/images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div class="navbar">\n    <div class="w-container">\n      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar home w-nav">\n        <div class="nav-container"><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a>\n          <div class="menu-button w-nav-button">\n            <div (click)="openMenu()" class="w-icon-nav-menu"></div>\n          </div><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="brand-3 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n          <nav role="navigation" class="nav-menu w-nav-menu">\n            <a (click)="navCtrl.setRoot(\'StorePage\')" class="navlink w-inline-block">\n              <div>Shop</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'KioskPage\')" class="navlink w-inline-block">\n              <div>Find a kiosk</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'WhyPage\')" class="navlink w-inline-block">\n              <div>Why Water3</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="navlink w-inline-block">\n              <div>FAQ</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'ContactPage\')" class="navlink w-inline-block">\n              <div>Contact</div>\n            </a>\n\n            <div class="nav-line"></div>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'RegisterPage\')" class="nav-button register w-button">Register</a>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'LoginPage\')" class="nav-button login w-button">Login</a>\n\n            <a *ngIf="isLoggedin" (click)="navCtrl.setRoot(\'AccountPage\')" class="navlink w-inline-block">My Account</a>\n\n            <a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></nav>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ion-content>\n  <div class="section-main-hero contact"><img src="assets/images/contact-hero-image.png" srcset="assets/images/contact-hero-image-p-500.png 500w, assets/images/contact-hero-image-p-800.png 800w, assets/images/contact-hero-image-p-1600.png 1600w, assets/images/contact-hero-image-p-2000.png 2000w, assets/images/contact-hero-image-p-2600.png 2600w, assets/images/contact-hero-image.png 2880w" sizes="100vw" class="image-4"></div>\n  <div class="contact-section">\n    <div class="w-container">\n      <h3 class="heading h3-contact">Contact</h3>\n      <div>\n        <div class="w-row">\n          <div class="w-col w-col-4">\n            <div>\n              <h5 class="h5-contact">CUSTOMER SUPPORT</h5>\n              <div class="text-contact">+61 2 0000 0000</div>\n            </div>\n          </div>\n          <div class="w-col w-col-4">\n            <div>\n              <h5 class="h5-contact">SALES</h5>\n              <div class="text-contact">email@water3.com</div>\n            </div>\n          </div>\n          <div class="w-col w-col-4">\n            <div>\n              <h5 class="h5-contact">PRODUCT ENQUIRY</h5>\n              <div class="text-contact">email@water3.com</div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class="div-block-22"><img src="assets/images/line1.png" width="186"></div>\n      <h3 class="heading h3-contact">Send us a message</h3>\n      <div class="text-contact">If you would like more information about Water3 please send us a message</div>\n      <div>\n        <div class="form-block w-form">\n          <form (ngSubmit)="sentEnquiry()" id="email-form" name="email-form" data-name="Email Form" class="from contact">\n            <label for="Name" class="field-label">First Name</label>\n            <input [(ngModel)]="data.first_name" type="text" class="w-input" maxlength="256" name="Name" data-name="Name" id="Name">\n            <label for="Name" class="field-label">Last Name</label>\n            <input [(ngModel)]="data.last_name" type="text" class="w-input" maxlength="256" name="Name" data-name="Name" id="Name">\n            <label for="Email" class="field-label">Email Address</label>\n            <input [(ngModel)]="data.email" type="text" class="w-input" maxlength="256" name="Email" data-name="Email" id="Email">\n            <label  for="Phone" class="field-label">Phone number</label>\n            <input [(ngModel)]="data.phone" type="text" class="w-input" maxlength="256" name="Phone" data-name="Phone" id="Phone">\n            <ion-item>\n              <ion-label>Enquiry Type</ion-label>\n                  <ion-select interface="popover" name="type" [(ngModel)]="data.type" class="customSelect">\n                    <ion-option value="Account" selected="true">Account issues</ion-option>\n                    <ion-option value="Report">Report a faulty machine</ion-option>\n                    <ion-option value="Lost">Lost or stolen bottle</ion-option>\n                    <ion-option value="Join">Join the water3 team</ion-option>\n                    <ion-option value="Suggestion">Suggest a new kiosk location</ion-option>\n                    <ion-option value="Other">Other</ion-option>\n                  </ion-select>\n            </ion-item><br>\n            <label for="Message" class="field-label">Message</label>\n            <textarea id="Message" name="Message" data-name="Message" maxlength="5000" class="w-input"></textarea>\n            <input type="submit" value="Submit" data-wait="Please wait..." class="contact-submit-btn w-button"></form>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class="footer-section" style="height: 250px;">\n    <div class="w-container">\n      <div class="row w-row">\n        <div class="w-col w-col-3">\n          <div><img src="assets/images/Logo.png" width="121.5" class="image-3">\n            <div class="text-copy-right _1">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n        <div class="w-col w-col-6">\n          <div class="w-row">\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-18">\n                <a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="footer-link">Home</a>\n                <a (click)="navCtrl.setRoot(\'StorePage\')" class="footer-link">Shop</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Find a kiosk</a>\n                <a (click)="navCtrl.setRoot(\'WhyPage\')" class="footer-link">Why Water3</a>\n                <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="footer-link">FAQ</a>\n                <a (click)="navCtrl.setRoot(\'ContactPage\')" class="footer-link">Contact</a></div>\n            </div>\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-19">\n                <a href="#" class="footer-link">Shipping</a>\n                <a (click)="navCtrl.setRoot(\'TermsPage\')" class="footer-link">Term of Services</a>\n                <a (click)="navCtrl.setRoot(\'PrivacyPage\')" class="footer-link">Privacy Policy</a></div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-21">\n            <a href="https://www.facebook.com/water3global" class="social-link w-inline-block"><img src="assets/images/fb-icon.png" class="social-icon"></a>\n            <a href="https://www.instagram.com/_water3_" class="social-link w-inline-block"><img src="assets/images/ig-icon.png" class="social-icon"></a>\n            <a href="https://twitter.com/water3_" class="social-link w-inline-block"><img src="assets/images/twitter-icon.png" class="social-icon"></a>\n            <a href="https://www.linkedin.com/company/water3" class="social-link w-inline-block"><img src="assets/images/linkedin-icon.png" class="social-icon"></a>\n            <a href="https://www.youtube.com/channel/UCCPqmAd8l2dCmH2iGXbSFvg" class="social-link w-inline-block"><img src="assets/images/youtube-icon.png" class="social-icon"></a>\n            <a href="mailto:info@water3.com.au" class="footer-email">info@water3.com.au</a>\n            <div class="text-block-5">+61 410 000 000</div>\n            <div class="footer-address">12 New Street Name, Suburb<br>QLD 4000, Australia</div>\n            <div class="text-copy-right _2">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n</ion-content>  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/contact/contact.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], ContactPage);

//# sourceMappingURL=contact.js.map

/***/ })

});
//# sourceMappingURL=38.main.js.map