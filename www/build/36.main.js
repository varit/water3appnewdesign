webpackJsonp([36],{

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__hello_ionic__ = __webpack_require__(320);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelloIonicPageModule", function() { return HelloIonicPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HelloIonicPageModule = (function () {
    function HelloIonicPageModule() {
    }
    return HelloIonicPageModule;
}());
HelloIonicPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__hello_ionic__["a" /* HelloIonicPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__hello_ionic__["a" /* HelloIonicPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__hello_ionic__["a" /* HelloIonicPage */]
        ]
    })
], HelloIonicPageModule);

//# sourceMappingURL=hello-ionic.module.js.map

/***/ }),

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_env__ = __webpack_require__(101);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelloIonicPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { HTTP } from '@ionic-native/http';
var HelloIonicPage = (function () {
    function HelloIonicPage(navCtrl, platform, env, api, menuCtrl) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.env = env;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoggedin = false;
        this.message = "Waiting command";
        this.token = "0";
        this.displayLog = "0";
        this.itemAmount = localStorage.getItem("itemAmount");
        if (this.itemAmount == null || this.itemAmount <= 0)
            this.itemAmount = 0;
        else
            this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
        if (localStorage.getItem("isLoggedin") == "1")
            this.isLoggedin = true;
    }
    HelloIonicPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    HelloIonicPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    HelloIonicPage.prototype.toKiosk = function () {
        this.navCtrl.setRoot('KioskPage');
    };
    HelloIonicPage.prototype.scrollTo = function () {
        this.content.scrollTo(0, 2500, 1500);
    };
    return HelloIonicPage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Content */])
], HelloIonicPage.prototype, "content", void 0);
HelloIonicPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])({
        segment: 'index'
    }),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-hello-ionic',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/hello-ionic/hello-ionic.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a3e479780557a0001772e76" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>Vending</title>\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div class="navbar">\n    <div class="w-container">\n      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar home w-nav">\n        <div class="nav-container"><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a>\n          <div class="menu-button w-nav-button">\n            <div (click)="openMenu()" class="w-icon-nav-menu"></div>\n          </div><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="brand-3 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n          <nav role="navigation" class="nav-menu w-nav-menu">\n            <a (click)="navCtrl.setRoot(\'StorePage\')" class="navlink w-inline-block">\n              <div>Shop</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'KioskPage\')" class="navlink w-inline-block">\n              <div>Find a kiosk</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'WhyPage\')" class="navlink w-inline-block">\n              <div>Why Water3</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="navlink w-inline-block">\n              <div>FAQ</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'ContactPage\')" class="navlink w-inline-block">\n              <div>Contact</div>\n            </a>\n\n            <div class="nav-line"></div>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'RegisterPage\')" class="nav-button register w-button">Register</a>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'LoginPage\')" class="nav-button login w-button">Login</a>\n\n            <a *ngIf="isLoggedin" (click)="navCtrl.setRoot(\'AccountPage\')" class="navlink w-inline-block">My Account</a>\n\n            <a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></nav>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ion-content>\n  <div class="section-main-hero home">\n    <div class="container w-container">\n      <p class="paragraph">Water3. Way more than water.</p><a (click)="navCtrl.setRoot(\'KioskPage\')" class="filnd-btn w-button">Find a kiosk</a><a (click)="navCtrl.setRoot(\'StorePage\')" class="buy-btn w-button">Buy Bottle</a></div>\n  </div>\n  <div class="section-feature">\n    <div class="w-container">\n      <h3 class="heading h3-feature">What’s Water3 all about?</h3>\n      <div style="padding-bottom: 20px;">Hey, you’re mostly made of water. The planet is mostly water. \nIsn’t it time we treated water with a little more respect? \nNow you can get natural spring water on the go, without harming the planet.\nBetter for you, better for sea life and wildlife. Just better all round.\n</div>\n      <div class="w-row">\n        <div class="w-col w-col-3">\n          <div class="div-block-6"><img src="assets/images/feature-image01.png" width="90">\n            <div class="text-feature">Fresh and healthy natural spring water</div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-5"><img src="assets/images/feature-image02.png" width="90">\n            <div class="text-feature">Still or sparkling</div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-4"><img src="assets/images/feature-image03.png" width="90">\n            <div class="text-feature">Chilled to perfection</div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-3"><img src="assets/images/feature-image04.png" width="90">\n            <div class="text-feature">Refill with your own bottle, or ours</div>\n          </div>\n        </div>\n      </div>\n      <div class="rehydrate-box1"><img src="assets/images/new2.png" srcset="assets/images/new2-p-500.png 500w, assets/images/new2-p-800.png 800w, assets/images/new2.png 1440w" sizes="(max-width: 767px) 100vw, (max-width: 991px) 728px, 940px" class="rehydrate">\n        <div><a href="#" class="button w-button">Shop Bottles</a></div><a (click)="navCtrl.setRoot(\'AccountPage\')" class="link rehydrate-btn">Register Bottle</a></div>\n      <div class="rehydrate-box2"><img src="assets/images/Group-2_2.png" width="250" class="rehydrate">\n        <div class="div-block-8"><a href="#" class="button w-button">Shop Bottles</a></div><a href="#" class="link rehydrate-btn">Register Bottle</a></div>\n    </div>\n  </div>\n  <div class="refill-section">\n    <div class="w-container">\n      <h3 class="heading h3-feature">Water3 is in all places you need it most. Shopping Centres, Universities, Health & Fitness Centres, you name it. </h3>\n      <div class="div-block-9"><img src="assets/images/Group.png" width="450" srcset="assets/images/Group-p-800.png 800w, assets/images/Group-p-1080.png 1080w, assets/images/Group-p-1600.png 1600w, assets/images/Group.png 1962w" sizes="(max-width: 479px) 100vw, 450px" class="refill-image"></div>\n      <div class="div-block-11"><img src="assets/images/Group-2_1.png" width="250"></div>\n      <div class="div-block-12">\n        <div data-delay="4000" data-animation="slide" data-autoplay="1" data-duration="500" data-infinite="1" class="refill-slide w-slider">\n          <div class="w-slider-mask">\n            <div class="w-slide">\n              <h4 class="h4-refill">A cleaner, greener planet</h4>\n              <div class="text-feature">Every Water3 refill stops another plastic bottle from entering the environment. You’ll be doing good without even trying. </div>\n            </div>\n            <div class="w-slide">\n              <h4 class="h4-refill">Easy peasy</h4>\n              <div class="text-feature">Filling up your bottle is ridiculously simple. Our Water3 refill stations have chilled still or sparkling spring water - just like a swish restaurant. Plus, you can get a great deal on one of our high quality reusable stainless steel water bottles, or simply BYO bottle to replenish your thirst.</div>\n            </div>\n            <div class="w-slide">\n              <h4 class="h4-refill">Super high tech</h4>\n              <div class="text-feature">Each Water3 bottle features an intuitive RFID chip in the lid, which allows you to tap and pay for refills quickly and easily, you can do it with one hand.\n \n</div>\n            </div>\n          </div>\n          <div class="left-arrow w-slider-arrow-left">\n            <div class="w-icon-slider-left"></div>\n          </div>\n          <div class="right-arrow w-slider-arrow-right">\n            <div class="w-icon-slider-right"></div>\n          </div>\n          <div class="slide-nav w-slider-nav w-round"></div>\n        </div>\n      </div>\n      <div class="div-block-13">\n        <div class="w-row">\n          <div class="w-col w-col-4">\n            <h4 class="h4-refill">A cleaner, greener planet</h4>\n            <div class="text-feature">Every Water3 refill stops another plastic bottle from entering the environment. You’ll be doing good without even trying. </div>\n          </div>\n          <div class="w-col w-col-4">\n            <h4 class="h4-refill">Easy peasy</h4>\n            <div class="text-feature">Filling up your bottle is ridiculously simple. Our Water3 refill stations have chilled still or sparkling spring water - just like a swish restaurant. Plus, you can get a great deal on one of our high quality reusable stainless steel water bottles, or simply BYO bottle to replenish your thirst.</div>\n          </div>\n          <div class="w-col w-col-4">\n            <h4 class="h4-refill">Super high tech</h4>\n            <div class="text-feature">Each Water3 bottle features an intuitive RFID chip in the lid, which allows you to tap and pay for refills quickly and easily, you can do it with one hand.</div>\n          </div>\n        </div>\n      </div>\n      <div class="div-block-10"><a href="#" class="filnd-btn w-button">Find a kiosk</a></div>\n    </div>\n  </div>\n  <div *ngIf="env.platform!=\'mobile\'" class="repeat-section">\n    <div class="w-container">\n      <h3 class="heading h3-feature repeat">Top up and go with the Water3 app</h3>\n      <div style="padding-bottom: 20px; color:white;">Why do you need an app? We’re glad you asked. The Water3 app lets you top up on your account, buy one of our classic colour bottles and, drumroll... find the nearest water refill station. \n</div>\n      <div class="div-block-14"><img src="assets/images/Group-13.png" width="500" srcset="assets/images/Group-13-p-500.png 500w, assets/images/Group-13.png 797w" sizes="(max-width: 767px) 100vw, 500px">\n        <div class="div-block-16"><a href="https://play.google.com/store/apps/details?id=com.water3.testpush" class="android-btn w-inline-block"><img src="assets/images/android-icon.png" width="24.5" class="image-2"><div class="text-block-3">Get it on Android</div></a>\n        <a href="https://itunes.apple.com/app/water3/id1139988955" class="play-store-btn w-inline-block"><img src="assets/images/iOS-icon.png" width="24.5" class="image-2"><div class="text-block-3">Get it on iOS</div></a></div>\n      </div>\n      <div class="div-block-15"><img src="assets/images/keyvisual-app.png" width="250">\n        <div class="div-block-16"><a href="https://play.google.com/store/apps/details?id=com.water3.testpush" class="android-btn w-inline-block"><img src="assets/images/android-icon.png" width="24.5" class="image-2"><div class="text-block-3">Get it on Android</div></a>\n        <a href="https://itunes.apple.com/app/water3/id1139988955" class="play-store-btn w-inline-block"><img src="assets/images/iOS-icon.png" width="24.5" class="image-2"><div class="text-block-3">Get it on iOS</div></a></div>\n      </div>\n    </div>\n  </div>\n  <div class="subscript-section">\n    <div class="w-container">\n      <h3 class="heading h3-feature">Live Life Unbottled</h3>\n      <div class="div-block-17">\n        <div class="subscript-form w-form">\n          <form id="email-form" name="email-form" data-name="Email Form" class="form"><input type="text" class="text-field-5 w-input" maxlength="256" name="email" data-name="email" placeholder="Enter your email address" id="email"><input type="submit" value="Submit" data-wait="Please wait..." class="submit-button email w-button"></form>\n          <div class="w-form-done">\n            <div>Thank you! Your submission has been received!</div>\n          </div>\n          <div class="w-form-fail">\n            <div>Oops! Something went wrong while submitting the form.</div>\n          </div>\n        </div>\n        <div class="text-block-4">We promise not to spam you</div>\n      </div>\n    </div>\n  </div>\n  <div class="footer-section" style="height: 250px;">\n    <div class="w-container">\n      <div class="row w-row">\n        <div class="w-col w-col-3">\n          <div><img src="assets/images/Logo.png" width="121.5" class="image-3">\n            <div class="text-copy-right _1">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n        <div class="w-col w-col-6">\n          <div class="w-row">\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-18">\n                <a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="footer-link">Home</a>\n                <a (click)="navCtrl.setRoot(\'StorePage\')" class="footer-link">Shop</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Find a kiosk</a>\n                <a (click)="navCtrl.setRoot(\'WhyPage\')" class="footer-link">Why Water3</a>\n                <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="footer-link">FAQ</a>\n                <a (click)="navCtrl.setRoot(\'ContactPage\')" class="footer-link">Contact</a></div>\n            </div>\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-19">\n                <a href="#" class="footer-link">Shipping</a>\n                <a (click)="navCtrl.setRoot(\'TermsPage\')" class="footer-link">Term of Services</a>\n                <a (click)="navCtrl.setRoot(\'PrivacyPage\')" class="footer-link">Privacy Policy</a></div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-21">\n            <a href="https://www.facebook.com/water3global" class="social-link w-inline-block"><img src="assets/images/fb-icon.png" class="social-icon"></a>\n            <a href="https://www.instagram.com/_water3_" class="social-link w-inline-block"><img src="assets/images/ig-icon.png" class="social-icon"></a>\n            <a href="https://twitter.com/water3_" class="social-link w-inline-block"><img src="assets/images/twitter-icon.png" class="social-icon"></a>\n            <a href="https://www.linkedin.com/company/water3" class="social-link w-inline-block"><img src="assets/images/linkedin-icon.png" class="social-icon"></a>\n            <a href="https://www.youtube.com/channel/UCCPqmAd8l2dCmH2iGXbSFvg" class="social-link w-inline-block"><img src="assets/images/youtube-icon.png" class="social-icon"></a>\n            <a href="mailto:info@water3.com.au" class="footer-email">info@water3.com.au</a>\n            <div class="text-block-5">+61 410 000 000</div>\n            <div class="footer-address">12 New Street Name, Suburb<br>QLD 4000, Australia</div>\n            <div class="text-copy-right _2">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content></body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/hello-ionic/hello-ionic.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__services_env__["a" /* ENV */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], HelloIonicPage);

//# sourceMappingURL=hello-ionic.js.map

/***/ })

});
//# sourceMappingURL=36.main.js.map