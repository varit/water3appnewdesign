webpackJsonp([43],{

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__changepassword__ = __webpack_require__(323);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangePasswordPageModule", function() { return ChangePasswordPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ChangePasswordPageModule = (function () {
    function ChangePasswordPageModule() {
    }
    return ChangePasswordPageModule;
}());
ChangePasswordPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__changepassword__["a" /* ChangePasswordPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__changepassword__["a" /* ChangePasswordPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__changepassword__["a" /* ChangePasswordPage */]
        ]
    })
], ChangePasswordPageModule);

//# sourceMappingURL=changepassword.module.js.map

/***/ }),

/***/ 323:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**

/**
 * Generated class for the AccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ChangePasswordPage = (function () {
    function ChangePasswordPage(navCtrl, alertCtrl, loadingCtrl, navParams, api, menuCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.data = { current_password: '', new_password: '', confirm_password: '' };
        if (this.api.checkSession()) {
            this.isLoaded = true;
        }
    }
    ChangePasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AccountPage');
    };
    ChangePasswordPage.prototype.postAddress = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({ content: "Please wait..." });
        loading.present();
        this.api.changeUserPassword(this.data).subscribe(function (res) {
            var alert = _this.alertCtrl.create({
                title: "Success",
                subTitle: "New Password has been updated.",
                buttons: ['OK']
            });
            alert.present(prompt);
            loading.dismiss();
            _this.navCtrl.setRoot('AccountPage');
        });
    };
    ChangePasswordPage.prototype.back = function () {
        this.navCtrl.setRoot('AccountPage');
    };
    ChangePasswordPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    ChangePasswordPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return ChangePasswordPage;
}());
ChangePasswordPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-account',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/account/changepassword.html"*/'<!DOCTYPE html>\n<html lang="en-AU" >\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <title>Filtered water: a healthier alternative to plastic bottled water</title>\n        <meta name="title" content="water3 |     Top Up | Amount\n">\n        <meta name="description" content="water3 is revolutionising the way we drink water; via a network of innovative kiosks that provide fresh and healthy chilled spring water.">\n        <meta content="width=device-width, initial-scale=1" name="viewport">\n        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:400,700,300%7CUbuntu">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/style-1489479537.css">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/app-1486002319.css">\n        <link rel="shortcut icon" href="../favicon-1495700985.ico" type="image/x-icon">\n        <link rel="icon" href="../favicon-1495700985.ico" type="image/x-icon">\n        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n                <script type="text/javascript">\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n    ga(\'create\', \'UA-83198216-1\', \'auto\');\n    ga(\'send\', \'pageview\');\n</script>\n    </head>\n        <body class="layout-portal" overflow-scroll="true">\n        \n        <header class="page">\n        <div class="container">\n        <a class="branding" href="index.html" title="Home">\n            <img src="assets/img/water3-logo.png" alt="Water3">\n        </a>\n\n        <button class="open-navigation" type="button" aria-label="Toggle menu" (click)="openMenu()">\n            <span class="bar top"></span>\n            <span class="bar middle"></span>\n            <span class="bar bottom"></span>\n            <span class="text">Menu</span>\n        </button>\n\n        <a class="btn btn-top-up btn-circle btn-icon" (click)="toBottle()" title="Top up">\n            <span class="icon icon-top-up hidden-hover"></span>\n            <span class="icon icon-top-up-white visible-hover"></span>\n            <span class="text">Top Up</span>\n        </a>\n    </div>\n</header>\n     <ion-content style="background-color: #368ba1">\n     <main *ngIf="isLoaded" class="page account">\n        <section class="component component-section">\n            <div class="container">\n                <h1>Account</h1>\n            </div>\n        </section>\n\n        <div class="content narrow">\n            <section class="component component-form">\n\n                <form (ngSubmit)="postAddress()" accept-charset="UTF-8" novalidate="novalidate"><input name="_session_key" type="hidden" value="UmMp95y7UsIKq0wFv9HybanFbFQKZllT2yfyALOa"><input name="_token" type="hidden" value="fzjkFcCSHFdpYLYa4Iogpix7ogBm5Cuw1DLYfo30">\n                    <fieldset>\n                        <legend>Change Password</legend>\n\n                        <div class="field-group">\n\n                            <div class="form-group">\n                                <label for="old_password" class="sr-only">Old Password</label>\n                                <span class="icon icon-lock"></span>\n                                <input class="form-control" placeholder="Old Password" name="old_password" type="password" [(ngModel)]="data.current_password" id="old_password">\n                            </div>\n\n                            <div class="form-group">\n                                <label for="password" class="sr-only">Password</label>\n                                <span class="icon icon-lock"></span>\n                                <input class="form-control" placeholder="New Password" name="password" type="password" [(ngModel)]="data.new_password" id="password">\n                            </div>\n\n                            <div class="form-group">\n                                <label for="confirm_pass" class="sr-only">Confirm Password</label>\n                                <span class="icon icon-lock"></span>\n                                <input class="form-control" placeholder="Confirm Password" name="confirm_pass" type="password" [(ngModel)]="data.confirm_password" id="confirm_pass">\n                            </div>\n                        </div>\n                    </fieldset>\n\n                    <div align="center" class="form-submit-group">\n                        <button class="btn btn-d" type="submit">Save</button>\n                        <button class="btn btn-d" (click)="back()">Cancel</button>\n                    </div>\n                </form>\n\n		\n            </section>\n        </div>\n    </main>\n     </ion-content>\n    </body>\n\n    <script type="text/javascript" src="../themes/water3/assets/js/vendor-1495700986.js"></script>\n        <script type="text/javascript" src="../themes/water3/assets/js/app-1498189486.js"></script>\n\n</html>\n'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/account/changepassword.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], ChangePasswordPage);

//# sourceMappingURL=changepassword.js.map

/***/ })

});
//# sourceMappingURL=43.main.js.map