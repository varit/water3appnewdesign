webpackJsonp([44],{

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__account__ = __webpack_require__(322);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountPageModule", function() { return AccountPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AccountPageModule = (function () {
    function AccountPageModule() {
    }
    return AccountPageModule;
}());
AccountPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__account__["a" /* AccountPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__account__["a" /* AccountPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__account__["a" /* AccountPage */]
        ]
    })
], AccountPageModule);

//# sourceMappingURL=account.module.js.map

/***/ }),

/***/ 322:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_helper__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_clipboard__ = __webpack_require__(202);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the AccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AccountPage = (function () {
    function AccountPage(navCtrl, helper, event, clipboard, alertCtrl, loadingCtrl, navParams, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.helper = helper;
        this.event = event;
        this.clipboard = clipboard;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.birth = { day: 0, month: 0, year: 0 };
        this.newBottle = { bottle_name: '', bottle_number: '' };
        this.bottles = [];
        if (this.api.checkSession()) {
            this.mode = "view";
            var loading_1 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_1.present();
            this.itemAmount = localStorage.getItem("itemAmount");
            if (this.itemAmount == null || this.itemAmount <= 0)
                this.itemAmount = 0;
            else
                this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
            this.api.getBottleList().subscribe(function (res) {
                //var status = res.status;
                var body = res.json();
                //console.log("Response : "+status+" BODY "+JSON.stringify(body));
                for (var _i = 0, _a = body.data; _i < _a.length; _i++) {
                    var bottle = _a[_i];
                    var newbottle = {
                        bottle_name: bottle.bottle_name,
                        bottle_no: bottle.bottle_no
                    };
                    _this.bottles.push(newbottle);
                }
            }, function (err) {
            });
            this.api.getAccountInfo().subscribe(function (res) {
                //var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                _this.accountInfo = body;
                _this.balance = body.regular_credit_balance + body.subscription_credit_balance;
                _this.balance = _this.balance.toFixed(2);
                if (localStorage.getItem("facebook") == "true") {
                    _this.type = "facebook";
                }
                if (body.gender == null || body.birth_date == null) {
                    _this.alertbox = "It seems like your profile information is missing. Complete your profile for free credits and more offers!";
                }
                if (body.birth_date != null) {
                    var bd = new Date(body.birth_date.iso);
                    _this.birth.day = bd.getUTCDate();
                    _this.birth.month = bd.getUTCMonth() + 1;
                    _this.birth.year = bd.getUTCFullYear();
                }
                _this.api.getReferCode().subscribe(function (res) {
                    var code = res.json();
                    _this.referCode = code.code;
                    _this.referCount = code.usedCount;
                    console.log("refercode", _this.referCode);
                });
                _this.api.getCountry().subscribe(function (res) {
                    //var status = res.status;
                    var body = res.json();
                    //console.log("Response : "+status+" BODY "+JSON.stringify(body));
                    _this.country = body.countries;
                    _this.api.getState(_this.accountInfo.country_id).subscribe(function (res) {
                        //var status = res.status;
                        var body = res.json();
                        //console.log("Response : "+status+" BODY "+JSON.stringify(body));
                        _this.state = body.states;
                        if (_this.state == null) {
                            _this.state = { "": "" };
                        }
                        var currentDate = new Date();
                        var endDate = _this.helper.dateQuery(currentDate);
                        var startDate = _this.helper.dateQuery(currentDate, 0, 1);
                        _this.api.getTransactions(startDate, endDate).subscribe(function (res) {
                            var status = res.status;
                            var body = res.json();
                            console.log("Response : " + status + " BODY " + JSON.stringify(body));
                            _this.transactions = body.transactions;
                            loading_1.dismiss();
                            _this.isLoaded = true;
                        }, function (err) {
                        });
                    }, function (err) {
                    });
                }, function (err) {
                });
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "Error",
                    subTitle: "Some error occured or the server took too long to respond.",
                    buttons: ['OK']
                });
                alert.present(prompt);
                loading_1.dismiss();
                _this.navCtrl.setRoot('HelloIonicPage');
            });
        }
    }
    AccountPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AccountPage');
    };
    AccountPage.prototype.changeMonth = function () {
        this.birth.day = 1;
    };
    AccountPage.prototype.copyCode = function () {
        console.log("Copy Code");
        this.clipboard.copy(this.referCode);
    };
    AccountPage.prototype.copyURL = function () {
        console.log("Copy URL");
        this.clipboard.copy("https://www.water3.com.au/#/register/" + this.referCode);
        var alert = this.alertCtrl.create({
            title: "Copied to clipboard",
            buttons: ['OK']
        });
        alert.present(prompt);
    };
    AccountPage.prototype.popReferral = function () {
        var alert = this.alertCtrl.create({
            title: "Referral Program",
            subTitle: "Invite your friends to join our water revolution and receive special rewards!",
            buttons: ['OK']
        });
        alert.present(prompt);
    };
    AccountPage.prototype.performAddBottle = function () {
        var _this = this;
        this.api.addBottle(this.newBottle).subscribe(function (res) {
            var status = res.status;
            var body = res.json();
            console.log("Response : " + status + " BODY " + JSON.stringify(body));
            var data = {
                email_to: localStorage.getItem("username"),
                email_from: "noreply@water3.com.au",
                subject: "You have added a new bottle!",
                template_file: "new_bottle",
                params: {
                    custoner_name: localStorage.getItem("fullname"),
                    designation: _this.newBottle.bottle_name,
                    bottle_no: _this.newBottle.bottle_number,
                    balance: _this.balance
                }
            };
            _this.api.sendEmail(data).subscribe(function (res) {
                console.log(res.json());
            });
            _this.event.publish("refreshPage", "1");
            var alert = _this.alertCtrl.create({
                title: "Success",
                subTitle: "Add New Bottle Successful",
                buttons: ['OK']
            });
            alert.present(prompt);
        }, function (err) {
            if (err.status == 404) {
                console.log(err);
                var alert_1 = _this.alertCtrl.create({
                    title: "Error ",
                    subTitle: "Your bottle number is invalid",
                    buttons: ['OK']
                });
                alert_1.present(prompt);
            }
            if (err.status == 409) {
                var alert_2 = _this.alertCtrl.create({
                    title: "Error ",
                    subTitle: "This bottle has already been registered.",
                    buttons: ['OK']
                });
                alert_2.present(prompt);
            }
        });
    };
    AccountPage.prototype.switchCurrency = function () {
        var _this = this;
        var cur;
        var exTo;
        this.api.getCurrency().subscribe(function (res) {
            var body = res.json();
            cur = body.currency;
            if (cur == "AUD")
                exTo = "USD";
            else if (cur == "USD")
                exTo = "AUD";
            _this.api.exchangeCurrency("test", exTo).subscribe(function (res) {
                var body = res.json();
                var alert = _this.alertCtrl.create({
                    title: "Notice ",
                    message: "Exchanging " + cur + " to " + exTo + " at the rate of " + body.rate,
                    buttons: [
                        {
                            text: "Cancel",
                            role: "cancel",
                            handler: function () { }
                        },
                        {
                            text: "Confirm",
                            handler: function () {
                                _this.api.exchangeCurrency("real", exTo).subscribe(function (res) {
                                    var alert = _this.alertCtrl.create({
                                        title: "Success",
                                        subTitle: "You have now switch to use " + exTo,
                                        buttons: ['OK']
                                    });
                                    alert.present(prompt);
                                    _this.api.getCurrency().subscribe(function (res) {
                                        _this.event.publish('refreshPage', "true");
                                    });
                                });
                            }
                        }
                    ]
                });
                alert.present();
            });
        });
    };
    AccountPage.prototype.postAddress = function () {
        var _this = this;
        this.accountInfo.birth_date = this.birth.day + "/" + this.birth.month + "/" + this.birth.year;
        if (this.birth.month == 2 && this.birth.year % 4 == 0 && this.birth.day > 29) {
            var alert_3 = this.alertCtrl.create({
                title: "Birth date invalid",
                subTitle: "Invalid 'day'",
                buttons: ['OK']
            });
            alert_3.present(prompt);
        }
        else if (this.birth.month == 2 && this.birth.day > 28) {
            var alert_4 = this.alertCtrl.create({
                title: "Birth date invalid",
                subTitle: "Invalid 'day'",
                buttons: ['OK']
            });
            alert_4.present(prompt);
        }
        else if ((this.birth.month == 2 || this.birth.month == 4 || this.birth.month == 6 || this.birth.month == 9 || this.birth.month == 11) && this.birth.day > 30) {
            var alert_5 = this.alertCtrl.create({
                title: "Birth date invalid",
                subTitle: "Invalid 'day'",
                buttons: ['OK']
            });
            alert_5.present(prompt);
        }
        else if (this.accountInfo.address_1 == null || this.accountInfo.address_1 == "" || this.accountInfo.postcode == null || this.accountInfo.postcode == "" || this.accountInfo.state_id == null || this.accountInfo.country_id == null) {
            var alert_6 = this.alertCtrl.create({
                title: "Address invalid",
                subTitle: "Please complete the address section",
                buttons: ['OK']
            });
            alert_6.present(prompt);
        }
        else if (this.accountInfo.gender == null || this.accountInfo.gender == "") {
            var alert_7 = this.alertCtrl.create({
                title: "Gender invalid",
                subTitle: "Please select a gender",
                buttons: ['OK']
            });
            alert_7.present(prompt);
        }
        else if (this.birth.day == 0 || this.birth.day == null || this.birth.month == 0 || this.birth.month == null || this.birth.year == 0 || this.birth.year == null) {
            var alert_8 = this.alertCtrl.create({
                title: "Birthdate invalid",
                subTitle: "Please make sure you have input a birthdate and try again.",
                buttons: ['OK']
            });
            alert_8.present(prompt);
        }
        else {
            console.log(this.accountInfo);
            this.api.updateUserAddress(this.accountInfo).subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                var alert = _this.alertCtrl.create({
                    title: "Success",
                    subTitle: "Update User data successful",
                    buttons: ['OK']
                });
                alert.present(prompt);
            }, function (err) {
                var message;
                if (err.status == 404)
                    message = "Required fields do not met";
                else if (err.status == 500)
                    message = "Internal server error. Please try again later or contact our support.";
                else
                    message = "Technial error, please contact our support.";
                var alert = _this.alertCtrl.create({
                    title: "Error " + err.status,
                    subTitle: message,
                    buttons: ['OK']
                });
                alert.present(prompt);
            });
        }
        this.mode = "view";
    };
    AccountPage.prototype.updateState = function () {
        var _this = this;
        this.api.getState(this.accountInfo.country_id).subscribe(function (res) {
            //var status = res.status;
            var body = res.json();
            var found;
            //console.log("Response : "+status+" BODY "+JSON.stringify(body));
            _this.state = body.states;
            if (_this.state != null && _this.state != "") {
                //this.accountInfo.state_id = this.state[0].id;
                //console.log("check state s="+s.id+" Saved ID="+this.accountInfo.state_id);
                for (var _i = 0, _a = _this.state; _i < _a.length; _i++) {
                    var s = _a[_i];
                    console.log("check state s=" + s.id + " Saved ID=" + _this.accountInfo.state_id);
                    if (s.id == _this.accountInfo.state_id) {
                        console.log("break at id " + s.id + " " + _this.accountInfo.state_id);
                        found = s.id;
                        break;
                    }
                }
                if (found != "" && found != null) {
                    _this.accountInfo.state_id = found;
                }
            }
            else {
                _this.accountInfo.state_id = 0;
                console.log("Reset State");
            }
        }, function (err) {
        });
    };
    AccountPage.prototype.stateName = function (state_id) {
        if (state_id != null || state_id != "") {
            for (var _i = 0, _a = this.state; _i < _a.length; _i++) {
                var s = _a[_i];
                if (s.id == this.accountInfo.state_id) {
                    return s.name;
                }
            }
        }
        return "";
    };
    AccountPage.prototype.countryName = function (country_id) {
        if (country_id != null || country_id != "") {
            for (var _i = 0, _a = this.country; _i < _a.length; _i++) {
                var c = _a[_i];
                if (c.id == this.accountInfo.country_id) {
                    return c.name;
                }
            }
        }
        return "";
    };
    AccountPage.prototype.performDelete = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Removing bottle ",
            message: "Are you sure to remove this bottle from your account? You can add it back at anytime",
            buttons: [
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: function () { }
                },
                {
                    text: "Confirm",
                    handler: function () {
                        var loading = _this.loadingCtrl.create({ content: "Please wait..." });
                        loading.present();
                        _this.api.getBottle(_this.index).subscribe(function (res) {
                            var status = res.status;
                            var body = res.json();
                            console.log("Response : " + status + " BODY " + JSON.stringify(body));
                            _this.bottle = body;
                            _this.api.deleteBottle(_this.bottle).subscribe(function (res) {
                                var status = res.status;
                                var body = res.json();
                                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                                var alert = _this.alertCtrl.create({
                                    title: "Success",
                                    subTitle: "The bottle has been removed",
                                    buttons: ['OK']
                                });
                                alert.present(prompt);
                                _this.event.publish("refreshPage", "1");
                            }, function (err) {
                            });
                            loading.dismiss();
                        }, function (err) {
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    AccountPage.prototype.back = function () {
        this.navCtrl.setRoot('AmountPage');
    };
    AccountPage.prototype.toChange = function () {
        this.navCtrl.setRoot('ChangePasswordPage');
    };
    AccountPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    AccountPage.prototype.getIndex = function () {
        var activeIndex = this.slides.getActiveIndex();
        if (activeIndex >= this.bottles.length)
            activeIndex -= 1;
        console.log("Bottle id = " + activeIndex, this.bottles[activeIndex]["bottle_no"]);
        this.index = this.bottles[activeIndex]['bottle_no'];
    };
    AccountPage.prototype.slidePrev = function () {
        this.slides.slidePrev();
    };
    AccountPage.prototype.slideNext = function () {
        this.slides.slideNext();
    };
    AccountPage.prototype.logout = function () {
        this.event.publish('logout', "1");
    };
    return AccountPage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */])
], AccountPage.prototype, "slides", void 0);
AccountPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-account',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/account/account.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a3eb9858192d400018c88c3" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>my account</title>\n  <meta content="my account" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div class="navbar">\n    <div class="w-container">\n      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar home w-nav">\n        <div class="nav-container w-clearfix"><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a>\n          <div class="menu-button w-nav-button">\n            <div class="w-icon-nav-menu"></div>\n          </div><a href="#" class="brand-4 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n          <nav role="navigation" class="nav-menu w-nav-menu">\n            <a (click)="navCtrl.setRoot(\'StorePage\')" class="navlink w-inline-block">\n              <div>Shop</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'KioskPage\')" class="navlink w-inline-block">\n              <div>Find a kiosk</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'WhyPage\')" class="navlink w-inline-block">\n              <div>Why Water3</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="navlink w-inline-block">\n              <div>FAQ</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'ContactPage\')" class="navlink w-inline-block">\n              <div>Contact</div>\n            </a>\n            <div class="nav-line"></div>\n            <div data-delay="0" class="w-dropdown">\n            <a (click)="logout()" class="navlink w-inline-block">Sign Out</a>\n            </div><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></nav>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ion-content *ngIf="isLoaded">\n  <div *ngIf="isLoaded" class="myaccount-section">\n    <div class="w-container">\n      <div>\n        <div class="row-1 w-row">\n          <div class="w-col w-col-7 w-col-tiny-6 w-col-small-small-stack">\n            <h3 class="h3-account">My Account</h3>\n            <div class="div-block-28 w-clearfix">\n              <h5 class="h5-account balance">Account Balance:</h5>\n              <div class="text-block-7">${{ balance }}</div>\n            </div>\n          </div>\n          <div class="w-col w-col-5 w-col-tiny-6 w-col-small-small-stack">\n            <div class="div-block-26 w-clearfix">\n              <div class="div-block-29 w-clearfix">\n                <h5 class="h5-account balance">Account Balance:</h5>\n                <div class="text-block-7">${{ balance }}</div>\n              </div><a (click)="navCtrl.setRoot(\'AmountPage\')" class="top-up-btn w-button">Top Up</a></div>\n          </div>\n        </div>       \n      </div>\n      <div>\n        <div class="row-2 w-row">\n\n\n          <div class=" bottle-desktop w-col w-col-5" style="float:left; display:initial; min-width: 1%">\n            <div *ngIf="bottles.length>=1 && mode!=\'add\'">\n                        <div>\n                                <ion-slides (ionSlideDidChange)="getIndex()" loop="false">\n                                    <ion-slide  *ngFor="let bottle of bottles" data-bottle-id="ng-bind: bottle.bottle_no">\n                                        <div class="circle">\n                                            <h4>\n                                                &nbsp;\n                                                <span class="balance"></span>\n                                            </h4>\n                                        </div>\n\n                                        <img src="assets/images/product1.png" alt="water3 bottle">\n\n                                        <h3 style="color:black;" class="number">{{ bottle.bottle_no }}</h3>\n                                        <p style="color:black;" >{{ bottle.bottle_name }}</p>\n                                    </ion-slide>\n                                </ion-slides>\n                        </div>\n\n                        <div class="left-arrow-2 w-slider-arrow-left">\n                          <div (click)="slidePrev()" class="w-icon-slider-left"></div>\n                        </div>\n                        <div class="right-arrow-2 w-slider-arrow-right">\n                          <div (click)="slideNext()" class="w-icon-slider-right"></div>\n                        </div>\n                        <div class="mini-detail">\n                        <a class="add-new-btn bottledetail w-button">Edit bottle details</a>\n                        <a (click)="performDelete()" class="add-new-btn bottledetail w-button">Remove bottle</a>\n                        <a (click)="mode=\'add\'" class="add-new-btn bottledetail w-button" style="width:80%;">Add another bottle</a>\n                        </div>\n                    </div>\n\n                    <div *ngIf="mode==\'add\' || bottles.length<1">\n                    <div  data-animation="slide" data-duration="500" data-infinite="1" class="slide-product w-slider">\n                      <div class="w-slider-mask">\n                        <div class="w-slide"><img src="assets/images/product1.png"></div>\n                      </div>\n                      <div class="slide-nav-2 w-slider-nav w-round"></div>\n                    </div>\n                    <div class="mini-detail">\n                      <div class="text-block-8">\n                          <input type="text" [(ngModel)]="newBottle.bottle_number" name="bottle_number" placeholder="#enter bottle number">\n                      </div>\n                      <div class="product-user-name">\n                        <input type="text" [(ngModel)]="newBottle.bottle_name" name="bottle_name" placeholder="Enter Bottle Name">\n                      </div>\n                      <a (click)="performAddBottle()" class="add-new-btn bottledetail w-button">Register bottle details</a>\n                      <a *ngIf="bottles.length>=1" (click)="mode=\'view\'" class="add-new-btn bottledetail w-button">Cancel</a></div>\n                </div>\n          </div>\n\n\n\n          <div class="w-col w-col-7" style="float: right;">\n            <div class="unbottled-impack">\n              <div class="div-block-42 w-clearfix">\n                <h5 class="h5-account">UNBOTTLED IMPACT</h5>\n              </div>\n              <div class="div-block-43">\n                <h2 class="h2-account">0 Refills</h2>\n                <div>0x of plastic bottles saved from consumption</div>\n              </div>\n              <div class="div-block-43">\n                <h2 class="h2-account">0 m<sup>3</sup></h2>\n                <div>of landfills you have saved this year with Unbottled</div>\n              </div>\n            </div>\n            <div class="account-detail-box">\n              <div class="div-block-23 w-clearfix">\n                <h5 class="h5-account">ACCOUNT DETAILS</h5>\n                <a *ngIf="mode!=\'account_edit\'" (click)="mode=\'account_edit\'" class="link-2">Edit</a>\n                <a *ngIf="mode==\'account_edit\'" (click)="postAddress()" class="link-2">Save</a>\n            </div>\n              <div class="div-block-24">\n                <div class="text-block-6 name">Name</div>\n                <div *ngIf="mode!=\'account_edit\'" class="text-block-6 username">{{accountInfo.first_name}} {{accountInfo.last_name}}</div>\n                <div><input *ngIf="mode==\'account_edit\'" type="text" name="first_name" [(ngModel)]="accountInfo.first_name"></div>\n                <input *ngIf="mode==\'account_edit\'" type="text" name="last_name" [(ngModel)]="accountInfo.last_name">\n              </div>\n              <div class="div-block-24">\n                <div class="text-block-6 name">Address</div>\n                <div *ngIf="mode!=\'account_edit\'" class="text-block-6 username">\n                    {{accountInfo.address_1}} {{accountInfo.address_2}}\n                    <br>\n                    {{accountInfo.city}}\n                    <br>\n                    {{stateName(accountInfo.state_id)}}\n                    <br>\n                    {{countryName(accountInfo.country_id)}}, {{accountInfo.postcode}}\n                </div>\n                <div *ngIf="mode==\'account_edit\'">\n                    <div><input type="text" name="address_1" placeholder="Address 1" [(ngModel)]="accountInfo.address_1"></div>\n                    <div><input type="text" name="address_2" placeholder="Address_2" [(ngModel)]="accountInfo.address_2"></div>\n                    <div><input type="text" name="postcode"  placeholder="Post Code" [(ngModel)]="accountInfo.postcode"></div>\n                    <ion-item *ngIf="state!=null && state!=\'\'">\n                    <ion-select interface="popover" class="addressSelect" name="state" placeholder="Select State" [(ngModel)]="accountInfo.state_id">\n                    <ion-option text-left *ngFor="let s of state" value="{{ s.id }}" selected="true">{{s.name}}</ion-option>\n                    </ion-select>\n                    </ion-item>\n                    <ion-item *ngIf="state==null || state==\'\'">\n                    <ion-select interface="popover" class="addressSelect" name="state" placeholder="--" disabled="true">\n                    </ion-select>\n                    </ion-item>\n                    <ion-item>\n                    <ion-select interface="popover" class="addressSelect" (ionChange)="updateState()" name="country" [(ngModel)]="accountInfo.country_id">\n                    <ion-option text-left value="14" selected="true">Australia</ion-option>\n                    <ion-option text-left value="239" selected="true">United States</ion-option>\n                    </ion-select>\n                    </ion-item>\n                </div>\n              </div>\n              <div class="div-block-24">\n                <div class="text-block-6 name">Phone</div>\n                <div *ngIf="mode!=\'account_edit\'" class="text-block-6 username">{{accountInfo.phone}}</div>\n                <div *ngIf="mode==\'account_edit\'"><input type=text name="phone" [(ngModel)]="accountInfo.phone"></div>\n              </div>\n              <div class="div-block-24">\n                <div class="text-block-6 name">Account Currency (?)</div>\n                <div style="width: 100%;" class="text-block-6 username">{{accountInfo.currency}} <a (click)="switchCurrency()" class="link-2" style="margin-top: 0px">Switch Currency</a></div>\n              </div>\n            </div>\n            <div class="account-detail-box">\n              <div class="div-block-23 w-clearfix">\n                <h5 class="h5-account">TRANSACTION HISTORY IN THE PAST 30 DAYS</h5><a (click)="navCtrl.setRoot(\'TransactionPage\')" class="link-2">View All</a></div>\n              <div *ngFor="let t of transactions" class="div-block-24">\n                <div class="text-block-6 date">{{ helper.convert_tz(t.date.text) }}</div>\n                <div class="text-block-6 username">Amount : ${{t.credit}}</div>\n                <div class="text-block-6 username">Description : {{t.description}}</div>\n              </div>\n              <div *ngIf="transactions.length==0" class="div-block-24">\n                <div class="text-block-6 date"></div>\n                <div class="text-block-6 username">No transaction history</div><div style="float: right;"></div>\n              </div>\n            </div>\n\n          </div>\n\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class="footer-section" style="height: 250px;">\n    <div class="w-container">\n      <div class="row w-row">\n        <div class="w-col w-col-3">\n          <div><img src="assets/images/Logo.png" width="121.5" class="image-3">\n            <div class="text-copy-right _1">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n        <div class="w-col w-col-6">\n          <div class="w-row">\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-18">\n                <a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="footer-link">Home</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Shop</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Find a kiosk</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Why Water3</a>\n                <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="footer-link">FAQ</a>\n                <a (click)="navCtrl.setRoot(\'ContactPage\')" class="footer-link">Contact</a></div>\n            </div>\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-19">\n                <a href="#" class="footer-link">Shipping</a>\n                <a (click)="navCtrl.setRoot(\'TermsPage\')" class="footer-link">Term of Services</a>\n                <a (click)="navCtrl.setRoot(\'PrivacyPage\')" class="footer-link">Privacy Policy</a></div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-21"><a href="#" class="social-link w-inline-block"><img src="assets/images/fb-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/ig-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/twitter-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/linkedin-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/youtube-icon.png" class="social-icon"></a><a href="#" class="footer-email">info@water3.com.au</a>\n            <div class="text-block-5">+61 410 000 000</div>\n            <div class="footer-address">12 New Street Name, Suburb<br>QLD 4000, Australia</div>\n            <div class="text-copy-right _2">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content></body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/account/account.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__services_helper__["a" /* Helper */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Events */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_clipboard__["a" /* Clipboard */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], AccountPage);

//# sourceMappingURL=account.js.map

/***/ })

});
//# sourceMappingURL=44.main.js.map