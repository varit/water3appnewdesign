webpackJsonp([32],{

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(333);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = (function () {
    function LoginPageModule() {
    }
    return LoginPageModule;
}());
LoginPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]
        ]
    })
], LoginPageModule);

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 333:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_facebook__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, platform, fb, loadingCtrl, alertCtrl, navParams, menuCtrl, api, event) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.fb = fb;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.api = api;
        this.event = event;
        this.remember = false;
        console.log("LoginController:: construct");
        this.token = localStorage.getItem("token");
        console.log("Token =" + this.token);
        this.itemAmount = localStorage.getItem("itemAmount");
        if (this.itemAmount == null || this.itemAmount <= 0)
            this.itemAmount = 0;
        else
            this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
    }
    LoginPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    LoginPage.prototype.submitLogin = function () {
        //console.log("Login details : "+this.email+":"+this.password);
        var _this = this;
        localStorage.setItem('email', this.email);
        localStorage.setItem('password', this.password);
        var loading = this.loadingCtrl.create({ content: "Loggin in" });
        loading.present();
        this.api.authenticate().subscribe(function (res) {
            //var status = res.status;
            var body = res.json();
            console.log("Response : " + status + " BODY " + JSON.stringify(body));
            localStorage.setItem("token", body.access_token);
            if (_this.remember == true) {
                console.log("remember me");
                localStorage.setItem("refresh", body.refresh_token);
            }
            localStorage.setItem("fullname", body.first_name + " " + body.last_name);
            localStorage.setItem("username", body.username);
            localStorage.setItem("isLoggedin", "1");
            var expiry = new Date();
            localStorage.setItem("expiry", new Date(expiry.getTime() + (_this.api.expiry * 1000 * 60)).toString());
            _this.event.publish('tokenUpdated', body.access_token);
            _this.event.publish('isLoggedIn', "true");
            console.log("Token = " + localStorage.getItem('token'));
            console.log("Refresher = " + localStorage.getItem('refresh'));
            console.log("Registered = " + expiry.toString());
            console.log("Expire = " + localStorage.getItem('expiry'));
            loading.dismiss();
            _this.navCtrl.setRoot('AccountPage');
        }, function (err) {
            console.log("Response : " + err.status + " BODY " + JSON.stringify(err));
            if (err.status == "401" || err.status == 401) {
                var alert_1 = _this.alertCtrl.create({
                    title: "Failure",
                    subTitle: "You have entered wrong username and/or password",
                    buttons: ['OK']
                });
                alert_1.present(prompt);
                loading.dismiss();
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: "Technical Error " + err.status,
                    subTitle: "Some technical error has occured : " + err.message,
                    buttons: ['OK']
                });
                alert_2.present(prompt);
                loading.dismiss();
            }
        });
    };
    LoginPage.prototype.toRegister = function () {
        this.navCtrl.setRoot('RegisterPage');
    };
    LoginPage.prototype.facebookLogin = function () {
        var _this = this;
        this.fb.login(['public_profile', 'email'])
            .then(function (res) {
            //console.log('Facebook!',res);
            _this.fbToken = res.authResponse.accessToken;
            console.log("UserID", res.authResponse.userID);
            _this.fb.api("/me?fields=id,email,first_name,last_name", ['public_profile', 'email'])
                .then(function (res) {
                //console.log(res); 
                _this.fbEmail = res.email;
                localStorage.setItem("email", _this.fbEmail);
                localStorage.setItem("facebook", "true");
                _this.fbID = res.id;
                _this.fbFirstName = res.first_name;
                _this.fbLastName = res.last_name;
                _this.api.AdminLogin().subscribe(function (res) {
                    var body = res.json();
                    localStorage.setItem("token", body.access_token);
                    _this.api.facebook(_this.fbEmail, _this.fbFirstName, _this.fbLastName, _this.fbID, _this.fbToken).subscribe(function (res) {
                        var body = res.json();
                        //console.log("Response : "+status+" BODY "+JSON.stringify(body));
                        localStorage.setItem("token", body.access_token);
                        localStorage.setItem("fullname", _this.fbFirstName + " " + _this.fbLastName);
                        localStorage.setItem("username", body.username);
                        localStorage.setItem("isLoggedin", "1");
                        var expiry = new Date();
                        localStorage.setItem("expiry", new Date(expiry.getTime() + (_this.api.expiry * 1000 * 60)).toString());
                        _this.event.publish('tokenUpdated', body.access_token);
                        _this.event.publish('isLoggedIn', "true");
                        console.log("Token = " + localStorage.getItem('token'));
                        console.log("Registered = " + expiry.toString());
                        console.log("Expire = " + localStorage.getItem('expiry'));
                        _this.navCtrl.setRoot('AccountPage');
                    });
                });
            });
        })
            .catch(function (e) {
            var alert = _this.alertCtrl.create({
                title: "Facebook Error",
                subTitle: "We could not conenct to Facebook at the moment, please try again later. (" + e.message + ")",
                buttons: ['OK']
            });
            alert.present(prompt);
        });
    };
    LoginPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return LoginPage;
}());
LoginPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/login/login.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a4f4c3f5c751a000134a95a" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>Login</title>\n  <meta content="Login" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">\n    <div class="w-container">\n      <div class="menu-button w-nav-button">\n        <div class="w-icon-nav-menu"></div>\n      </div><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="brand-5 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n      <nav role="navigation" class="nav-menu-2 w-nav-menu">\n        <div class="div-block">Already have an account?</div><a class="nav-button w-button">Login</a><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></nav><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png"  class="image-11"><div class="text-cart">({{itemAmount}})</div></a></div>\n  </div>\n  <ion-content>\n  <div class="section-main-hero login">\n    <div class="div-block-41"><img src="assets/images/500_F_115215766_xSCOJkKThtRgbtTaYOmEcB3Z05fIjk9X.png" class="image-10">\n    </div>\n    <div class="register-container w-container">\n      <div class="sub-nav back"><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="back-btn _2 w-inline-block w-clearfix"><img src="assets/images/Shape.png" width="15" class="image"><div class="text-back _2">Back</div></a></div>\n      <div class="w-row">\n        <div class="column-6 w-col w-col-4 w-col-medium-6 w-col-small-small-stack w-col-tiny-tiny-stack">\n          <div class="form-block-3 w-form">\n            <form id="email-form" name="email-form" data-name="Email Form" class="from">\n              <h3 class="h3-register">Login to Water3</h3><a (click)="facebookLogin()" class="login-facebook-btn w-button">Login with facebook</a>\n              <div class="div-text">\n                <div class="w-row">\n                  <div class="w-col w-col-3 w-col-small-4 w-col-tiny-4">\n                    <div class="line"></div>\n                  </div>\n                  <div class="w-col w-col-6 w-col-small-4 w-col-tiny-4">\n                    <div class="text-block">or log in below</div>\n                  </div>\n                  <div class="column w-col w-col-3 w-col-small-4 w-col-tiny-4">\n                    <div class="line"></div>\n                  </div>\n                </div>\n              </div>\n              <label for="email-3" class="field-label">Email Address:</label>\n              <input [(ngModel)]="email"  type="email" class="w-input" maxlength="256" name="email" data-name="email" placeholder="Enter your email" id="email-3">\n              <label for="Password" class="field-label">Password:</label>\n              <input [(ngModel)]="password"  type="password" class="w-input" maxlength="256" name="Password" data-name="Password" placeholder="Password" id="Password">\n\n              <div *ngIf="!platform.is(\'mobile\') && !platform.is(\'mobileweb\')" class="field-group">\n                  <div align="center" class="form-group checkbox">\n                      <input type="checkbox" name="remember" id="remember" value="true" [(ngModel)]="remember" title="Remember Me" />\n                      Remember Me\n                  </div>\n              </div>\n              <input *ngIf="platform.is(\'mobile\')" type="hidden" name="remember" id="remember" value="true" [(ngModel)]="remember"/>\n\n              <input (click)="submitLogin()" type="submit" value="Log In" data-wait="Please wait..." class="login-btn w-button">\n              <a (click)="navCtrl.setRoot(\'RequestPage\')" class="text-forgot">Reset or Forgotten your password?</a>\n              <a (click)="navCtrl.setRoot(\'RegisterPage\')" class="text-dont-have">Don’t have an account?</a></form>\n          </div>\n        </div>\n        <div class="column-5 w-col w-col-8 w-col-medium-6 w-col-small-small-stack w-col-tiny-tiny-stack"></div>\n      </div>\n    </div>\n  </div>\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content>\n</body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/login/login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_facebook__["a" /* Facebook */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */], __WEBPACK_IMPORTED_MODULE_3__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Events */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=32.main.js.map