webpackJsonp([34],{

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__kiosk__ = __webpack_require__(331);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KioskPageModule", function() { return KioskPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var KioskPageModule = (function () {
    function KioskPageModule() {
    }
    return KioskPageModule;
}());
KioskPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__kiosk__["a" /* KioskPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__kiosk__["a" /* KioskPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__kiosk__["a" /* KioskPage */]
        ]
    })
], KioskPageModule);

//# sourceMappingURL=kiosk.module.js.map

/***/ }),

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(201);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KioskPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var KioskPage = (function () {
    function KioskPage(navCtrl, alertCtrl, loadingCtrl, geolocation, api, navParams, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.geolocation = geolocation;
        this.api = api;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.keyword = "";
        this.action = "index";
        this.markers = [];
        this.isLoaded = false;
        this.latitude = 1;
        this.itemAmount = localStorage.getItem("itemAmount");
        if (this.itemAmount == null || this.itemAmount <= 0)
            this.itemAmount = 0;
        else
            this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
        var loading = this.loadingCtrl.create({ content: "Please wait..." });
        loading.present();
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.latitude = resp.coords.latitude;
            _this.longitude = resp.coords.longitude;
            var position = new google.maps.LatLng(_this.latitude, _this.longitude);
            var mapOptions = {
                center: position,
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            _this.map = new google.maps.Map(document.getElementById("map"), mapOptions);
            var userImage = "assets/images/marker-user.png";
            //var userLocation = {lat: this.latitude, lng: this.longitude };
            var user = new google.maps.LatLng(_this.latitude, _this.longitude);
            var marker = new google.maps.Marker({
                map: _this.map,
                animation: google.maps.Animation.DROP,
                position: user,
                icon: userImage
            });
            _this.isLoaded = true;
            loading.dismiss();
        }).catch(function (error) {
            console.log('Error getting location', error);
            var alert = _this.alertCtrl.create({
                title: "Error getting location",
                subTitle: error.message,
                buttons: ['OK']
            });
            alert.present(prompt);
            var position = new google.maps.LatLng(-27.560102, 152.988739);
            var mapOptions = {
                center: position,
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            _this.map = new google.maps.Map(document.getElementById("map"), mapOptions);
        });
    }
    KioskPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad KioskPage');
    };
    KioskPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    KioskPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    KioskPage.prototype.postSearch = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({ content: "Please wait..." });
        loading.present();
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.latitude = resp.coords.latitude;
            _this.longitude = resp.coords.longitude;
            console.log("Lat", _this.latitude);
            console.log("Long", _this.longitude);
            var login = localStorage.getItem("isLoggedin");
            if (login != "1") {
                _this.api.AdminLogin().subscribe(function (res) {
                    var body = res.json();
                    localStorage.setItem("token", body.access_token);
                    _this.api.findKiosk(_this.latitude, _this.longitude, _this.keyword, 70000000).subscribe(function (res) {
                        var body = res.json();
                        console.log("Response : " + status + " BODY " + JSON.stringify(body));
                        _this.navCtrl.setRoot('KioskSearchPage', {
                            kiosks: body.data,
                            lat: _this.latitude,
                            long: _this.longitude
                        });
                        loading.dismiss();
                    });
                });
            }
            else {
                _this.api.findKiosk(_this.latitude, _this.longitude, _this.keyword, 70000000).subscribe(function (res) {
                    var body = res.json();
                    console.log("Response : " + status + " BODY " + JSON.stringify(body));
                    _this.navCtrl.setRoot('KioskSearchPage', {
                        kiosks: body.data,
                        lat: _this.latitude,
                        long: _this.longitude
                    });
                    loading.dismiss();
                });
            }
        }).catch(function (error) {
            console.log('Error getting location', error);
            var alert = _this.alertCtrl.create({
                title: "Error getting location",
                subTitle: error.message,
                buttons: ['OK']
            });
            alert.present(prompt);
            var login = localStorage.getItem("isLoggedin");
            if (login != "1") {
                _this.api.AdminLogin().subscribe(function (res) {
                    var body = res.json();
                    localStorage.setItem("token", body.access_token);
                    _this.api.findKiosk(152.988739, -27.560102, _this.keyword, 70000000).subscribe(function (res) {
                        var body = res.json();
                        console.log("Response : " + status + " BODY " + JSON.stringify(body));
                        _this.navCtrl.setRoot('KioskSearchPage', {
                            kiosks: body.data,
                            lat: 1,
                            long: 1
                        });
                        loading.dismiss();
                    });
                });
            }
            else {
                _this.api.findKiosk(152.988739, -27.560102, _this.keyword, 70000000).subscribe(function (res) {
                    var body = res.json();
                    console.log("Response : " + status + " BODY " + JSON.stringify(body));
                    _this.navCtrl.setRoot('KioskSearchPage', {
                        kiosks: body.data,
                        lat: 1,
                        long: 1
                    });
                    loading.dismiss();
                }, function (err) {
                    loading.dismiss();
                });
            }
        });
    };
    return KioskPage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('map'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
], KioskPage.prototype, "mapElement", void 0);
KioskPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-kiosk',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/kiosk/kiosk.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a43717ff6b9a40001bdb535" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>kiosk</title>\n  <meta content="kiosk" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div class="navbar">\n    <div class="w-container">\n      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar home w-nav">\n        <div class="nav-container"><a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a>\n          <div class="menu-button w-nav-button">\n            <div (click)="openMenu()" class="w-icon-nav-menu"></div>\n          </div><a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="brand-3 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n          <nav role="navigation" class="nav-menu w-nav-menu">\n            <a (click)="navCtrl.setRoot(\'StorePage\')" class="navlink w-inline-block">\n              <div>Shop</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'KioskPage\')" class="navlink w-inline-block">\n              <div>Find a kiosk</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'WhyPage\')" class="navlink w-inline-block">\n              <div>Why Water3</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="navlink w-inline-block">\n              <div>FAQ</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'ContactPage\')" class="navlink w-inline-block">\n              <div>Contact</div>\n            </a>\n\n            <div class="nav-line"></div>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'RegisterPage\')" class="nav-button register w-button">Register</a>\n            <a *ngIf="!isLoggedin" (click)="navCtrl.setRoot(\'LoginPage\')" class="nav-button login w-button">Login</a>\n\n            <a *ngIf="isLoggedin" (click)="navCtrl.setRoot(\'AccountPage\')" class="navlink w-inline-block">My Account</a>\n\n            <a (click)="navCtrl.setRoot(\'CartPage\')" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">({{itemAmount}})</div></a></nav>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ion-content>\n  <div class="section-main-hero kiosk">\n    <div class="find-kiosk-box">\n      <div class="form-block-2 w-form">\n        <form id="email-form-2" name="email-form-2" data-name="Email Form 2" class="form-2"><label for="name-2" class="label-1">Find Kiosk</label>\n          <div class="div-block-33 w-clearfix"><input type="text" [(ngModel)]="keyword" class="filed-1 w-input" maxlength="256" name="keyword" data-name="keyword" placeholder="Enter your keyword" id="keyword"><input (click)="postSearch()" type="submit" value="Search" data-wait="Please wait..." class="search-kiosk-btn w-button"></div>\n        </form>\n      </div>\n    </div>\n    <div #map id="map" style="height:60vh;width: 100%;" ></div>\n  </div>\n  <div class="footer-section" style="height: 250px;">\n    <div class="w-container">\n      <div class="row w-row">\n        <div class="w-col w-col-3">\n          <div><img src="assets/images/Logo.png" width="121.5" class="image-3">\n            <div class="text-copy-right _1">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n        <div class="w-col w-col-6">\n          <div class="w-row">\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-18">\n                <a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="footer-link">Home</a>\n                <a (click)="navCtrl.setRoot(\'StorePage\')" class="footer-link">Shop</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Find a kiosk</a>\n                <a (click)="navCtrl.setRoot(\'WhyPage\')" class="footer-link">Why Water3</a>\n                <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="footer-link">FAQ</a>\n                <a (click)="navCtrl.setRoot(\'ContactPage\')" class="footer-link">Contact</a></div>\n            </div>\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-19">\n                <a href="#" class="footer-link">Shipping</a>\n                <a (click)="navCtrl.setRoot(\'TermsPage\')" class="footer-link">Term of Services</a>\n                <a (click)="navCtrl.setRoot(\'PrivacyPage\')" class="footer-link">Privacy Policy</a></div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-21"><a href="#" class="social-link w-inline-block"><img src="assets/images/fb-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/ig-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/twitter-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/linkedin-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/youtube-icon.png" class="social-icon"></a><a href="#" class="footer-email">info@water3.com.au</a>\n            <div class="text-block-5">+61 410 000 000</div>\n            <div class="footer-address">12 New Street Name, Suburb<br>QLD 4000, Australia</div>\n            <div class="text-copy-right _2">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content>\n</body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/kiosk/kiosk.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], KioskPage);

//# sourceMappingURL=kiosk.js.map

/***/ })

});
//# sourceMappingURL=34.main.js.map