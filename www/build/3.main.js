webpackJsonp([3],{

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__transaction__ = __webpack_require__(364);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionPageModule", function() { return TransactionPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TransactionPageModule = (function () {
    function TransactionPageModule() {
    }
    return TransactionPageModule;
}());
TransactionPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__transaction__["a" /* TransactionPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__transaction__["a" /* TransactionPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__transaction__["a" /* TransactionPage */]
        ]
    })
], TransactionPageModule);

//# sourceMappingURL=transaction.module.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_helper__ = __webpack_require__(200);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransactionPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TransactionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TransactionPage = (function () {
    function TransactionPage(navCtrl, alertCtrl, loadingCtrl, navParams, helper, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.helper = helper;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.period = 1;
        if (this.api.checkSession()) {
            var loading_1 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_1.present();
            var currentDate = new Date();
            console.log(currentDate + " Month=" + currentDate.getUTCMonth());
            var endDate = this.helper.dateQuery(currentDate);
            var startDate = this.helper.dateQuery(currentDate, 0, this.period);
            console.log(startDate);
            console.log(endDate);
            this.api.getTransactions(startDate, endDate).subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                _this.transactions = body.transactions;
                _this.isLoaded = true;
                loading_1.dismiss();
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "Error " + err.status,
                    subTitle: "Cannot retrieve transaction data",
                    buttons: ['OK']
                });
                alert.present(prompt);
                loading_1.dismiss();
            });
        }
    }
    TransactionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Transaction');
    };
    TransactionPage.prototype.getTransactions = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({ content: "Please wait..." });
        loading.present();
        var currentDate = new Date();
        var endDate = this.helper.dateQuery(currentDate);
        var startDate = this.helper.dateQuery(currentDate, 0, this.period);
        if (this.period == 99) {
            endDate = "2099-01-01 00:00:00";
            startDate = "2014-01-01 00:00:00";
        }
        console.log(startDate);
        console.log(endDate);
        this.api.getTransactions(startDate, endDate).subscribe(function (res) {
            var status = res.status;
            var body = res.json();
            console.log("Response : " + status + " BODY " + JSON.stringify(body));
            _this.transactions = body.transactions;
            loading.dismiss();
        }, function (err) {
        });
    };
    TransactionPage.prototype.pushShow = function (t) {
        this.navCtrl.push('ShowPage', {
            transaction: t
        });
    };
    TransactionPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    TransactionPage.prototype.back = function () {
        this.navCtrl.setRoot('AccountPage');
    };
    TransactionPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return TransactionPage;
}());
TransactionPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-transaction',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/transaction/transaction.html"*/'<!DOCTYPE html>\n<html lang="en-AU" >\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <title>Filtered water: a healthier alternative to plastic bottled water</title>\n        <meta name="title" content="water3 | think better. be better. do more">\n        <meta name="description" content="water3 is revolutionising the way we drink water; via a network of innovative kiosks that provide fresh and healthy chilled spring water.">\n        <meta content="width=device-width, initial-scale=1" name="viewport">\n        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:400,700,300%7CUbuntu">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/style-1489479537.css">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/app-1486002319.css">\n        <link rel="shortcut icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <link rel="icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n                <script type="text/javascript">\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n    ga(\'create\', \'UA-83198216-1\', \'auto\');\n    ga(\'send\', \'pageview\');\n</script>\n    </head>\n        <body class="layout-default" overflow-scroll="true">\n        \n        <header class="page">\n        <div class="container">\n        <a class="branding" href="index.html" title="Home">\n            <img src="assets/img/water3-logo.png" alt="Water3">\n        </a>\n\n        <button class="open-navigation" type="button" aria-label="Toggle menu" (click)="openMenu()">\n            <span class="bar top"></span>\n            <span class="bar middle"></span>\n            <span class="bar bottom"></span>\n            <span class="text">Menu</span>\n        </button>\n\n        <a class="btn btn-top-up btn-circle btn-icon" (click)="toBottle()" title="Top up">\n            <span class="icon icon-top-up hidden-hover"></span>\n            <span class="icon icon-top-up-white visible-hover"></span>\n            <span class="text">Top Up</span>\n        </a>\n    </div>\n</header>\n        <ion-content>\n        <main class="page white">\n        <section class="component-section">\n            <div class="container">\n                <h1>Transaction History</h1>\n\n                <a class="back" (click)="back()" title="Back">\n                    <span class="icon icon-arrow-left-white"></span>\n                    <span class="text">Back</span>\n                </a>\n            </div>\n        </section>\n\n        <div class="content narrow">\n            <section class="component component-transactions">\n                <form method="GET" action="https://www.water3.com.au/transactions" accept-charset="UTF-8" novalidate="novalidate">\n                    <div class="form-group">                        \n                        <ion-item>\n                                <ion-select interface="popover" class="transactionMonthSelect" [(ngModel)]="period" (ionChange)="getTransactions()" name="period">\n                                  <ion-option text-left value=1 >Last Month</ion-option>\n                                  <ion-option text-left value=3 >Last 3 Months</ion-option>\n                                  <ion-option text-left value=6 >Last 6 Months</ion-option>\n                                  <ion-option text-left value=99 >All</ion-option>\n                                </ion-select>\n                        </ion-item>\n                    </div>\n                </form>\n\n                \n                    <div *ngIf="isLoaded" class="transactions-list">\n                        <div class="list-group">\n                                <a class="list-group-item transaction" *ngFor="let t of transactions" (click)="pushShow(t)" title="View transaction">\n                                    <span *ngIf="t.type==\'Credit Topup\'" class="icon icon-arrow-up-circle"></span>\n                                    <span *ngIf="t.type==\'Credit Adjsut\'" class="icon icon-arrow-up-circle"></span>\n                                    <span *ngIf="t.type==\'Credit Expiry\'" class="icon icon-information"></span>\n                                    <span *ngIf="t.type==\'Credit Subscription\'" class="icon icon-payment"></span>\n                                    <span *ngIf="t.type==\'Kiosk Purchase\'" class="icon icon-waterdrop-circle"></span>\n                                    <span *ngIf="t.type==\'Free Credit\'" class="icon icon-account"></span>\n                                    <span *ngIf="t.type==\'Credit Adjustment\'" class="icon icon-information"></span>\n                                    \n                                    <span class="date">{{ helper.convert_tz(t.date.text) }}</span>\n\n                                    <span class="amount pull-right">${{t.credit}}</span>\n                                    <span class="icon icon-arrow-right"></span>\n                                </a>\n                        </div>\n                    </div>\n\n            </section>\n        </div>\n    </main>\n\n        <footer class="page">\n    <div class="container">\n        <div class="row">\n            <div class="col-xs-12 col-md-6 social">\n                <h3>Follow Us</h3>\n\n                <div class="actions">\n                    <a class="btn btn-c btn-circle btn-icon" href="https://www.facebook.com/water3global" title="Facebook" target="_blank">\n                        <span class="icon icon-facebook hidden-hover"></span>\n                        <span class="icon icon-facebook-white visible-hover"></span>\n                        <span class="text">Facebook</span>\n                    </a>\n\n                    <a class="btn btn-c btn-circle btn-icon" href="https://twitter.com/water3_" title="Twitter" target="_blank">\n                        <span class="icon icon-twitter hidden-hover"></span>\n                        <span class="icon icon-twitter-white visible-hover"></span>\n                        <span class="text">Twitter</span>\n                    </a>\n\n                    <a class="btn btn-c btn-circle btn-icon" href="https://www.linkedin.com/company/water3" title="LinkedIn" target="_blank">\n                        <span class="icon icon-linkedin hidden-hover"></span>\n                        <span class="icon icon-linkedin-white visible-hover"></span>\n                        <span class="text">LinkedIn</span>\n                    </a>\n\n                    <a class="btn btn-c btn-circle btn-icon" href="https://www.instagram.com/_water3_" title="Instagram" target="_blank">\n                        <span class="icon icon-instagram hidden-hover"></span>\n                        <span class="icon icon-instagram-white visible-hover"></span>\n                        <span class="text">Instagram</span>\n                    </a>\n\n                    <a class="btn btn-c btn-circle btn-icon" href="https://www.youtube.com/channel/UCCPqmAd8l2dCmH2iGXbSFvg" title="YouTube" target="_blank">\n                        <span class="icon icon-youtube hidden-hover"></span>\n                        <span class="icon icon-youtube-white visible-hover"></span>\n                        <span class="text">YouTube</span>\n                    </a>\n                </div>\n            </div>\n\n            <div class="col-xs-12 col-md-6 subscribe">\n                <h3>Subscribe</h3>\n\n                <form method="POST" action="https://water3.com.au/subscribe#subscribe" accept-charset="UTF-8" id="subscribe"><input name="_session_key" type="hidden" value="eUawDN3pvQfIycwYiJkiaxytd1gS29WbCAYC0Pn2"><input name="_token" type="hidden" value="fzjkFcCSHFdpYLYa4Iogpix7ogBm5Cuw1DLYfo30">\n                    <div class="pre-alerts">\n                                                                    </div>\n\n                    <div class="field-group">\n                        <div class="form-group">\n                            <label for="email" class="sr-only">Email address</label>\n                            <span class="icon icon-envelope"></span>\n                            <input class="form-control" placeholder="Email address" name="email" type="email" id="email">\n                        </div>\n                    </div>\n\n                    <button class="btn btn-c" type="submit">Submit</button>\n\n                    <div class="post-alerts">\n                                                                    </div>\n                </form>\n            </div>\n        </div>\n\n        <div class="divider"></div>\n\n        <div class="row">\n            <div class="col-xs-12">\n                <ul class="navigation">\n                    <li><a (click)="navCtrl.setRoot(\'KioskPage\')" title="Find kiosks">Find kiosks</a></li>\n                    <li><a (click)="navCtrl.setRoot(\'FaqsPage\')"  title="Frequently asked questions">FAQs</a></li>\n                    <li><a (click)="navCtrl.setRoot(\'TermsPage\')"  title="Terms of service">Terms of service</a></li>\n                    <li><a (click)="navCtrl.setRoot(\'PrivacyPage\')"  title="Privacy policy">Privacy policy</a></li>\n                </ul>\n\n                <p class="question">Have a question? <a (click)="navCtrl.setRoot(\'ContactPage\')" title="Contact us">Contact us</a></p>\n\n                <p class="copyright">\n                    Copyright &copy; 2017 Vending Machines International PTE LTD. All Rights Reserved.<br>\n                    Designed by <a href="https://www.littlegiant.co.nz/" title="Little Giant">Little Giant</a>.\n                </p>\n            </div>\n        </div>\n    </div>\n</footer>\n</ion-content>\n        <section class="component component-bottom-button responsive">\n    <div class="button-container">\n        <a class="btn btn-icon-inline btn-square btn-facebook-messenger" href="https://m.me/water3global" target="_blank">\n            <span class="icon icon-facebook-messenger"></span>\n            Message water3\n        </a>\n    </div>\n</section>\n    </body>\n\n    <script type="text/javascript" src="assets/js/vendor-1486002319.js"></script>\n        <script type="text/javascript" src="assets/js/app-1486002319.js"></script>\n\n</html>\n'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/transaction/transaction.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_helper__["a" /* Helper */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], TransactionPage);

//# sourceMappingURL=transaction.js.map

/***/ })

});
//# sourceMappingURL=3.main.js.map