webpackJsonp([6],{

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment__ = __webpack_require__(361);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentPageModule", function() { return PaymentPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaymentPageModule = (function () {
    function PaymentPageModule() {
    }
    return PaymentPageModule;
}());
PaymentPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__payment__["a" /* PaymentPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__payment__["a" /* PaymentPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__payment__["a" /* PaymentPage */]
        ]
    })
], PaymentPageModule);

//# sourceMappingURL=payment.module.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PaymentPage = (function () {
    function PaymentPage(navCtrl, loadingCtrl, alertCtrl, navParams, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.save = "false";
        this.Stripe_param = { name: '', number: '', exp_month: '', exp_year: '', cvc: '' };
        if (this.api.checkSession()) {
            this.amount = localStorage.getItem('amount');
            var loading_1 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_1.present();
            if (localStorage.getItem("card_id") != null || localStorage.getItem("card_id") != "")
                this.api.getUserCreditCards().subscribe(function (res) {
                    var body = res.json();
                    console.log("Response : " + status + " BODY " + JSON.stringify(body));
                    body.cards.forEach(function (element) {
                        if (element.stripe_card_id == localStorage.getItem("card_id")) {
                            _this.card = element;
                            console.log(_this.card.stripe_card_id);
                        }
                    });
                    _this.isLoaded = true;
                    loading_1.dismiss();
                }, function (err) {
                });
            this.stripe_pk = this.api.getStripePK();
        }
    }
    PaymentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TopupPage - payment');
        Stripe.setPublishableKey(this.stripe_pk);
    };
    PaymentPage.prototype.postPayment = function () {
        var _this = this;
        console.log(this.Stripe_param);
        console.log(this.save);
        if (!this.card) {
            if (this.Stripe_param.number.length > 16) {
                var alert_1 = this.alertCtrl.create({
                    title: "Validation Error",
                    subTitle: "You credit card number exceeded digit limit",
                    buttons: ['OK']
                });
                alert_1.present(prompt);
            }
            else if (+this.Stripe_param.exp_month > 12 || +this.Stripe_param.exp_month == null) {
                var alert_2 = this.alertCtrl.create({
                    title: "Date Error",
                    subTitle: "Invalid expiry month",
                    buttons: ['OK']
                });
                alert_2.present(prompt);
            }
            else if (+this.Stripe_param.exp_year < 2016 || +this.Stripe_param.exp_year == null) {
                var alert_3 = this.alertCtrl.create({
                    title: "Date Error",
                    subTitle: "Invalid expiry year",
                    buttons: ['OK']
                });
                alert_3.present(prompt);
            }
            else if (+this.Stripe_param.cvc > 999 || +this.Stripe_param.cvc == null || +this.Stripe_param.cvc <= 0) {
                var alert_4 = this.alertCtrl.create({
                    title: "CVC Error",
                    subTitle: "Incorrect CVC",
                    buttons: ['OK']
                });
                alert_4.present(prompt);
            }
            else {
                Stripe.card.createToken({
                    name: this.Stripe_param.name,
                    number: this.Stripe_param.number,
                    exp_month: this.Stripe_param.exp_month,
                    exp_year: this.Stripe_param.exp_year,
                    cvc: this.Stripe_param.cvc
                }, function (status, response) { return _this.stripeResponseHandler(status, response); });
            }
        }
        else {
            var loading_2 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_2.present();
            var cur;
            this.api.getCurrency().subscribe(function (res) {
                cur = res.json().currency;
                _this.api.charge(_this.card.stripe_card_id, _this.save, cur, true).subscribe(function (res) {
                    var body = res.json();
                    console.log("Response : " + status + " BODY " + JSON.stringify(body));
                    loading_2.dismiss();
                    _this.navCtrl.setRoot('ConfirmPage');
                }, function (err) {
                    console.log(err);
                    loading_2.dismiss();
                });
            });
        }
    };
    PaymentPage.prototype.stripeResponseHandler = function (status, response) {
        var _this = this;
        if (response.error) {
            var alert_5 = this.alertCtrl.create({
                title: "Payment Error",
                subTitle: response.error.message,
                buttons: ['OK']
            });
            alert_5.present(prompt);
        }
        else {
            var token = response.id;
            //this.Stripe_token = response.id;
            console.log(token);
            var loading_3 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_3.present();
            this.api.charge(token, this.save).subscribe(function (res) {
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                loading_3.dismiss();
                _this.navCtrl.setRoot('ConfirmPage');
            }, function (err) {
                console.log(err);
                loading_3.dismiss();
            });
        }
    };
    PaymentPage.prototype.back = function () {
        this.navCtrl.setRoot('PaymentMethodPage');
    };
    PaymentPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    PaymentPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return PaymentPage;
}());
PaymentPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-address',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/topup/payment.html"*/'<!DOCTYPE html>\n<!--  This site was created in Webflow. http://www.webflow.com  -->\n<!--  Last Published: Mon Jan 29 2018 09:41:02 GMT+0000 (UTC)  -->\n<html data-wf-page="5a3eb9967b791a0001977dff" data-wf-site="5a3d2c1e43c15000012d9aa1">\n<head>\n  <meta charset="utf-8">\n  <title>topup1</title>\n  <meta content="topup1" property="og:title">\n  <meta content="width=device-width, initial-scale=1" name="viewport">\n  <meta content="Webflow" name="generator">\n  <link href="../../assets/css/normalize.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/webflow.css" rel="stylesheet" type="text/css">\n  <link href="../../assets/css/vending-register.webflow.css" rel="stylesheet" type="text/css">\n  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->\n  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>\n  <link href="assets/images/favicon.png" rel="shortcut icon" type="image/x-icon">\n  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">\n</head>\n<body>\n  <div class="navbar">\n    <div class="w-container">\n      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar home w-nav">\n        <div class="nav-container"><a href="#" class="cart-link mobile w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">(2)</div></a>\n          <div class="menu-button w-nav-button">\n            <div class="w-icon-nav-menu"></div>\n          </div><a href="#" class="brand-3 w-nav-brand"><img src="assets/images/Logo.png" width="121.5"></a>\n          <nav role="navigation" class="nav-menu w-nav-menu">\n            <a href="#" class="navlink w-inline-block">\n              <div>Shop</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'KioskPage\')" class="navlink w-inline-block">\n              <div>Find a kiosk</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'WhyPage\')" class="navlink w-inline-block">\n              <div>Why Water3</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="navlink w-inline-block">\n              <div>FAQ</div>\n            </a>\n            <a (click)="navCtrl.setRoot(\'ContactPage\')" class="navlink w-inline-block">\n              <div>Contact</div>\n            </a>\n            <div class="nav-line"></div>\n            <div data-delay="0" class="w-dropdown">\n            <a (click)="navCtrl.setRoot(\'AccountPage\')" class="navlink w-inline-block">My Account</a>\n            </div><a href="#" class="cart-link w-inline-block"><img src="assets/images/Cart-icon.png" class="image-11"><div class="text-cart">(2)</div></a></nav>\n        </div>\n      </div>\n    </div>\n  </div>\n  <ion-content *ngIf="isLoaded">\n<div class="topup-section">\n    <div class="topup-head">\n      <div class="topup-row w-row">\n        <div class="column-3 w-col w-col-4 w-col-tiny-tiny-stack"></div>\n        <div class="w-col w-col-4 w-col-tiny-tiny-stack">\n          <div class="step-box">\n            <div class="div-block-35 step-1">\n              <div class="text-block-14 _1">1</div>\n            </div>\n            <div class="line-step past"></div>\n            <div class="div-block-35 step-1">\n              <div class="text-block-14 _1">2</div>\n            </div>\n            <div class="line-step past"></div>\n            <div class="div-block-35 step-1">\n              <div class="text-block-14 _1">3</div>\n            </div>\n            <div class="line-step past"></div>\n            <div class="div-block-35 step2 current">\n              <div class="text-block-14 current">4</div>\n            </div>\n          </div>\n        </div>\n        <div class="column-4 w-col w-col-4 w-col-tiny-tiny-stack">\n          <div class="canel-box"><a (click)="navCtrl.setRoot(\'PaymentMethodPage\')" class="link-3">BACK</a></div>\n        </div>\n      </div>\n    </div>\n    <div class="topup-container w-container">\n      <div class="topup-form-1">\n        <h3 class="h3-topup">Confirm Payment</h3>\n\n        <div *ngIf="card!=null && card!=\'\'" class="summary-box">\n              <div class="div-block-23 w-clearfix">\n                <h5 class="h5-account">Summary</h5>\n              </div>\n              <div class="div-block-24">\n                <div>\n                  <div class="w-form">\n                    <form id="email-form" name="email-form" data-name="Email Form">\n                      <div class="w-row">\n                        <div class="w-col w-col-6">\n                          <div class="text-block-22"><strong class="bold-text-3">Topup Amount</strong></div>\n                        </div>\n                        <div class="w-col w-col-6">\n                          <div class="text-block-21"><strong class="bold-text-2">$ {{amount}}</strong></div>\n                        </div>\n                      </div>\n                      <input type="submit" (click)="postPayment()" value="Confirm" data-wait="Please wait..." class="checkout-btn1 unlogin w-button">\n                      <input type="submit" value="Back" data-wait="Please wait..." class="checkout-btn-2 unlogin w-button">\n                  </form>\n                  </div>\n                </div>\n              </div>\n            </div>\n\n\n            <div *ngIf="card==null || card==\'\'" class="w-form">\n          <form id="email-form" name="email-form" data-name="Email Form">\n            <label>Credit Card Details</label>\n            <input type="text" [(ngModel)]="Stripe_param.name" name="name" id="name" placeholder="Cardholder Name" maxlength="256" class="top-up-filed-form w-input">\n            <label for="Number">Number</label>\n            <input type="text" [(ngModel)]="Stripe_param.number" class="top-up-filed-form w-input" maxlength="256" name="Number" data-name="Number" placeholder="Number" id="Number">\n            <div>\n              <div class="w-row">\n                <div class="w-col w-col-6"><label for="Expiry-Date">Expiry Date</label>\n                    <input type="text" [(ngModel)]="Stripe_param.exp_month" style="width:75px; display: initial;" class="top-up-filed-form w-input" size=3 maxlength="256" name="Expiry-Date" data-name="Expiry Date" placeholder="MM" id="Expiry-Date">\n                    <input type="text" [(ngModel)]="Stripe_param.exp_year" style="width:75px; display: initial;" class="top-up-filed-form w-input" maxlength="256" name="Expiry-Date" data-name="Expiry Date" placeholder="YYYY" id="Expiry-Date">\n                </div>\n                <div class="w-col w-col-6"><label for="CVC">CVC</label>\n                    <input type="text" [(ngModel)]="Stripe_param.cvc" class="top-up-filed-form w-input" maxlength="256" name="CVC" data-name="CVC" placeholder="CVC" id="CVC">\n                </div>\n              </div>\n              <div class="w-row">\n                Save this card for future use?\n                <input [(ngModel)]="save" type="checkbox" name="save_card" value="true">\n              </div>\n            </div>\n            <div><input type="submit" (click)="navCtrl.setRoot(\'PaymentMethodPage\')" value="Back" data-wait="Please wait..." class="top-up-back-btn w-button"><a (click)="postPayment()" class="top-up-next-btn confirm w-button">Confirm</a></div>\n          </form>\n\n        </div>\n\n      </div>\n    </div>\n  </div>\n  <div class="footer-section" style="height: 250px;">\n    <div class="w-container">\n      <div class="row w-row">\n        <div class="w-col w-col-3">\n          <div><img src="assets/images/Logo.png" width="121.5" class="image-3">\n            <div class="text-copy-right _1">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n        <div class="w-col w-col-6">\n          <div class="w-row">\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-18">\n                <a (click)="navCtrl.setRoot(\'HelloIonicPage\')" class="footer-link">Home</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Shop</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Find a kiosk</a>\n                <a (click)="navCtrl.setRoot(\'KioskPage\')" class="footer-link">Why Water3</a>\n                <a (click)="navCtrl.setRoot(\'FaqsPage\')" class="footer-link">FAQ</a>\n                <a (click)="navCtrl.setRoot(\'ContactPage\')" class="footer-link">Contact</a></div>\n            </div>\n            <div class="w-col w-col-6 w-col-tiny-6">\n              <div class="div-block-19">\n                <a href="#" class="footer-link">Shipping</a>\n                <a (click)="navCtrl.setRoot(\'TermsPage\')" class="footer-link">Term of Services</a>\n                <a (click)="navCtrl.setRoot(\'PrivacyPage\')" class="footer-link">Privacy Policy</a></div>\n            </div>\n          </div>\n        </div>\n        <div class="w-col w-col-3">\n          <div class="div-block-21"><a href="#" class="social-link w-inline-block"><img src="assets/images/fb-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/ig-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/twitter-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/linkedin-icon.png" class="social-icon"></a><a href="#" class="social-link w-inline-block"><img src="assets/images/youtube-icon.png" class="social-icon"></a><a href="#" class="footer-email">info@water3.com.au</a>\n            <div class="text-block-5">+61 410 000 000</div>\n            <div class="footer-address">12 New Street Name, Suburb<br>QLD 4000, Australia</div>\n            <div class="text-copy-right _2">Copyright © 2017 Water3</div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>\n  <script src="js/webflow.js" type="text/javascript"></script>\n  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->\n</ion-content></body>\n</html>'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/topup/payment.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], PaymentPage);

//# sourceMappingURL=payment.js.map

/***/ })

});
//# sourceMappingURL=6.main.js.map