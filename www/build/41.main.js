webpackJsonp([41],{

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create__ = __webpack_require__(325);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BottleCreatePageModule", function() { return BottleCreatePageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BottleCreatePageModule = (function () {
    function BottleCreatePageModule() {
    }
    return BottleCreatePageModule;
}());
BottleCreatePageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__create__["a" /* BottleCreatePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__create__["a" /* BottleCreatePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__create__["a" /* BottleCreatePage */]
        ]
    })
], BottleCreatePageModule);

//# sourceMappingURL=create.module.js.map

/***/ }),

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BottleCreatePage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the BottlePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var BottleCreatePage = (function () {
    function BottleCreatePage(navCtrl, loadingCtrl, alertCtrl, navParams, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.bottle = { bottle_name: '', bottle_number: '' };
        this.action = "edit";
        if (this.api.checkSession()) {
            this.api.getAccountInfo().subscribe(function (res) {
                var body = res.json();
                _this.balance = body.regular_credit_balance + body.subscription_credit_balance;
            });
            if (navParams.get('bottle_no') != null) {
                this.bottle.bottle_number = navParams.get('bottle_no');
            }
            this.isLoaded = true;
        }
    }
    BottleCreatePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Bottle Create Page');
    };
    BottleCreatePage.prototype.toDelete = function () {
        this.action = "delete";
    };
    BottleCreatePage.prototype.toEdit = function () {
        this.action = "edit";
    };
    BottleCreatePage.prototype.performAddBottle = function () {
        var _this = this;
        this.api.addBottle(this.bottle).subscribe(function (res) {
            var status = res.status;
            var body = res.json();
            console.log("Response : " + status + " BODY " + JSON.stringify(body));
            var data = {
                email_to: localStorage.getItem("username"),
                email_from: "noreply@water3.com.au",
                subject: "You have added a new bottle!",
                template_file: "new_bottle",
                params: {
                    custoner_name: localStorage.getItem("fullname"),
                    designation: _this.bottle.bottle_name,
                    bottle_no: _this.bottle.bottle_number,
                    balance: _this.balance
                }
            };
            _this.api.sendEmail(data).subscribe(function (res) {
                console.log(res.json());
            });
            var alert = _this.alertCtrl.create({
                title: "Success",
                subTitle: "Add New Bottle Successful",
                buttons: ['OK']
            });
            alert.present(prompt);
            _this.navCtrl.setRoot('TopupPage');
        }, function (err) {
            if (err.status == 404) {
                var alert_1 = _this.alertCtrl.create({
                    title: "Error ",
                    subTitle: "Your bottle number is invalid",
                    buttons: ['OK']
                });
                alert_1.present(prompt);
            }
            if (err.status == 409) {
                var alert_2 = _this.alertCtrl.create({
                    title: "Error ",
                    subTitle: "This bottle has already been registered.",
                    buttons: ['OK']
                });
                alert_2.present(prompt);
            }
        });
    };
    BottleCreatePage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    BottleCreatePage.prototype.back = function () {
        this.navCtrl.setRoot('BottlePage');
    };
    BottleCreatePage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return BottleCreatePage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */])
], BottleCreatePage.prototype, "slides", void 0);
BottleCreatePage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-bottle',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/bottle/create.html"*/'<!DOCTYPE html>\n<html lang="en-AU" >\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <title>Filtered water: a healthier alternative to plastic bottled water</title>\n        <meta name="title" content="water3 | think better. be better. do more">\n        <meta name="description" content="water3 is revolutionising the way we drink water; via a network of innovative kiosks that provide fresh and healthy chilled spring water.">\n        <meta content="width=device-width, initial-scale=1" name="viewport">\n        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:400,700,300%7CUbuntu">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/style-1489479537.css">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/app-1486002319.css">\n        <link rel="shortcut icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <link rel="icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n                <script type="text/javascript">\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n    ga(\'create\', \'UA-83198216-1\', \'auto\');\n    ga(\'send\', \'pageview\');\n</script>\n    </head>\n        <body class="layout-portal" overflow-scroll="true">\n        \n        <header class="page">\n        <div class="container">\n        <a class="branding" href="index.html" title="Home">\n            <img src="assets/img/water3-logo.png" alt="Water3">\n        </a>\n\n        <button class="open-navigation" type="button" aria-label="Toggle menu" (click)="openMenu()">\n            <span class="bar top"></span>\n            <span class="bar middle"></span>\n            <span class="bar bottom"></span>\n            <span class="text">Menu</span>\n        </button>\n\n        <a class="btn btn-top-up btn-circle btn-icon" (click)="toBottle()" title="Top up">\n            <span class="icon icon-top-up hidden-hover"></span>\n            <span class="icon icon-top-up-white visible-hover"></span>\n            <span class="text">Top Up</span>\n        </a>\n    </div>\n</header>\n        <ion-content style="background-color: #368ba1">\n        <main class="page bottle" *ngIf="isLoaded">\n        <section class="component component-section">\n            <div class="container">\n                <h1>Add New Bottle</h1>\n\n                <a class="back" (click)="back()" title="Back">\n                    <span class="icon icon-arrow-left-white"></span>\n                    <span class="text">Back</span>\n                </a>\n            </div>\n        </section>\n\n        <div *ngIf="action==\'edit\'" class="content narrow">\n            <section class="component component-form">\n                <img src="assets/img/bottle-large.png" alt="water3 bottle">\n\n                <form (ngSubmit)="performAddBottle()" accept-charset="UTF-8" novalidate="novalidate" #loginForm="ngForm">\n                    <div class="field-group">\n                        <div class="form-group">\n                            <label for="bottle_number" class="sr-only">Bottle Number</label>\n                            <input class="form-control" placeholder="Bottle Number" name="bottle_number" type="text" [(ngModel)]="bottle.bottle_number" id="bottle_number">\n                        </div>\n                        <div class="form-group">\n                            <label for="bottle_name" class="sr-only">Bottle Name</label>\n                            <input class="form-control" placeholder="Enter your bottle name" name="bottle_name" type="text" [(ngModel)]="bottle.bottle_name" id="bottle_name">\n                        </div>\n                    </div>\n\n                    <div class="form-submit-group">\n                        <button class="btn btn-d btn-block" type="submit">Submit</button>\n                    </div>\n                </form>\n            </section>\n        </div>\n\n    </main>\n</ion-content>\n    </body>\n\n    <script type="text/javascript" src="assets/js/vendor-1486002319.js"></script>\n        <script type="text/javascript" src="assets/js/app-1486002319.js"></script>\n\n</html>\n'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/bottle/create.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], BottleCreatePage);

//# sourceMappingURL=create.js.map

/***/ })

});
//# sourceMappingURL=41.main.js.map