webpackJsonp([0],{

/***/ 303:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sub_address__ = __webpack_require__(349);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubAddressPageModule", function() { return SubAddressPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SubAddressPageModule = (function () {
    function SubAddressPageModule() {
    }
    return SubAddressPageModule;
}());
SubAddressPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__sub_address__["a" /* SubAddressPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sub_address__["a" /* SubAddressPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__sub_address__["a" /* SubAddressPage */]
        ]
    })
], SubAddressPageModule);

//# sourceMappingURL=sub-address.module.js.map

/***/ }),

/***/ 349:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubAddressPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SubAddressPage = (function () {
    function SubAddressPage(navCtrl, loadingCtrl, alertCtrl, navParams, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        if (this.api.checkSession()) {
            var loading_1 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_1.present();
            this.api.getAccountInfo().subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                _this.accountInfo = body;
                _this.api.getCountry().subscribe(function (res) {
                    var status = res.status;
                    var body = res.json();
                    console.log("Response : " + status + " BODY " + JSON.stringify(body));
                    _this.country = body.countries;
                    _this.api.getState(_this.accountInfo.country_id).subscribe(function (res) {
                        var status = res.status;
                        var body = res.json();
                        console.log("Response : " + status + " BODY " + JSON.stringify(body));
                        _this.state = body.states;
                        loading_1.dismiss();
                        _this.isLoaded = true;
                    }, function (err) {
                    });
                }, function (err) {
                });
            }, function (err) {
            });
        }
    }
    SubAddressPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TopupPage - Address');
    };
    SubAddressPage.prototype.postAddress = function () {
        var _this = this;
        if (this.accountInfo.address_1 == "" || this.accountInfo.address_1 == null || this.accountInfo.city == "" || this.accountInfo.city == null || this.accountInfo.postcode == "" || this.accountInfo.postcode == null) {
            var alert_1 = this.alertCtrl.create({
                title: "Error",
                subTitle: "Please check that all the fields are filled and try again.",
                buttons: ['OK']
            });
            alert_1.present(prompt);
        }
        else {
            this.api.updateUserAddress(this.accountInfo).subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                _this.navCtrl.setRoot('SubPaymentMethodPage');
            }, function (err) {
            });
        }
    };
    SubAddressPage.prototype.updateState = function () {
        var _this = this;
        this.api.getState(this.accountInfo.country_id).subscribe(function (res) {
            var status = res.status;
            var body = res.json();
            console.log("Response : " + status + " BODY " + JSON.stringify(body));
            _this.state = body.states;
        }, function (err) {
        });
    };
    SubAddressPage.prototype.back = function () {
        this.navCtrl.setRoot('PlanPage');
    };
    SubAddressPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    SubAddressPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return SubAddressPage;
}());
SubAddressPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-address',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/subscription/sub-address.html"*/'\n<!DOCTYPE html>\n<html lang="en-AU" >\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <title>Filtered water: a healthier alternative to plastic bottled water</title>\n        <meta name="title" content="water3 |     Top Up | Amount\n">\n        <meta name="description" content="water3 is revolutionising the way we drink water; via a network of innovative kiosks that provide fresh and healthy chilled spring water.">\n        <meta content="width=device-width, initial-scale=1" name="viewport">\n        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:400,700,300%7CUbuntu">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/style-1489479537.css">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/app-1486002319.css">\n        <link rel="shortcut icon" href="../favicon-1495700985.ico" type="image/x-icon">\n        <link rel="icon" href="../favicon-1495700985.ico" type="image/x-icon">\n        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n                <script type="text/javascript">\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n    ga(\'create\', \'UA-83198216-1\', \'auto\');\n    ga(\'send\', \'pageview\');\n</script>\n    </head>\n        <body class="layout-portal" overflow-scroll="true">\n        \n        <header class="page">\n        <div class="container">\n        <a class="branding" href="index.html" title="Home">\n            <img src="assets/img/water3-logo.png" alt="Water3">\n        </a>\n\n        <button class="open-navigation" type="button" aria-label="Toggle menu" (click)="openMenu()">\n            <span class="bar top"></span>\n            <span class="bar middle"></span>\n            <span class="bar bottom"></span>\n            <span class="text">Menu</span>\n        </button>\n\n        <a class="btn btn-top-up btn-circle btn-icon" (click)="toBottle()" title="Top up">\n            <span class="icon icon-top-up hidden-hover"></span>\n            <span class="icon icon-top-up-white visible-hover"></span>\n            <span class="text">Top Up</span>\n        </a>\n    </div>\n</header>\n           <main class="page">\n        <section class="component component-section">\n            <div class="container">\n                <h1>Confirm Address</h1>\n\n                <a class="back" (click)="navCtrl.setRoot(\'SubscriptionPage\')" title="Back">\n                    <span class="icon icon-arrow-left-white"></span>\n                    <span class="text">Back</span>\n                </a>\n\n                <ol class="progression">\n                    <li class="complete"><span class="icon icon-success-white"></span></li>\n                    <li class="current">2</li>\n                    <li>3</li>\n                    <li>4</li>\n                </ol>\n            </div>\n        </section>\n        \n        <div class="content narrow">\n            <section class="component component-form">\n                                \n                <form *ngIf="isLoaded" (ngSubmit)="postAddress()" accept-charset="UTF-8" novalidate="novalidate"><input name="_session_key" type="hidden" value="NwFiAm0hAkKMWgjl36eUZ8pzEwO4vtQ3bDkAbQ2W"><input name="_token" type="hidden" value="BExa9c3dStVd2CHnuEq0744dX0lI8fykyjlWalPW">\n                    <fieldset>\n                        <legend>Address details</legend>\n\n                        <div class="field-group">\n                            <div class="form-group">\n                                <label for="address_1" class="sr-only">Street address</label>\n                                <span class="icon icon-house"></span>\n                                <input class="form-control" placeholder="Street address" name="address_1" type="text" [(ngModel)]="accountInfo.address_1" id="address_1">\n                            </div>\n\n                            <div class="form-group">\n                                <label for="address_2" class="sr-only">Street address</label>\n                                <span class="icon icon-house"></span>\n                                <input class="form-control" placeholder="Address 2 - Optional" name="address_2" type="text" [(ngModel)]="accountInfo.address_2" id="address_2">\n                            </div>\n\n                            <div class="form-group">\n                                <label for="city" class="sr-only">City</label>\n                                <span class="icon icon-house"></span>\n                                <input class="form-control" placeholder="City" name="city" type="text" [(ngModel)]="accountInfo.city" id="city">\n                            </div>\n\n                            <div class="form-group">\n                                <div class="row">\n                                    <div class="col-xs-6">\n                                        <label for="state_id" class="sr-only">State</label>\n                                        <span class="icon icon-house"></span>\n                                        \n                                <ion-item *ngIf="state!=null && state!=\'\'">\n                                <ion-select interface="popover" class="addressSelect" name="state" placeholder="Select State" [(ngModel)]="accountInfo.state_id">\n\n                                  <ion-option text-left *ngFor="let s of state" value="{{ s.id }}" selected="true">{{s.name}}</ion-option>\n                                </ion-select>\n                                </ion-item>\n\n                                <ion-item *ngIf="state==null || state==\'\'">\n                                <ion-select interface="popover" class="addressSelect" name="state" placeholder="--" disabled="true">\n                                </ion-select>\n                                </ion-item>\n\n\n                                    </div>\n                                    <div class="col-xs-6" style="padding-top: 6px; padding-bottom: 6px;">\n                                        <label for="postcode" class="sr-only">Postcode</label>\n                                        <input class="form-control" placeholder="Postcode" name="postcode" [(ngModel)]="accountInfo.postcode" type="text" id="postcode">\n                                    </div>\n                                </div>\n                            </div>\n\n                            <div class="form-group">\n                                <label for="country_id" class="sr-only">Country</label>\n                                <span class="icon icon-house"></span>\n                                <ion-item>\n                                <ion-select interface="popover" class="addressSelect" (ionChange)="updateState()" name="country" [(ngModel)]="accountInfo.country_id">\n                                  <ion-option text-left *ngFor="let c of country" value="{{ c.id }}" selected="true">{{c.name}}</ion-option>\n                                </ion-select>\n                              </ion-item>\n                            </div>\n                        </div>\n                    </fieldset>\n\n                    <div class="form-submit-group">\n                        <button class="btn btn-d btn-block" type="submit">Next</button>\n                    </div>\n                </form>\n            </section>\n\n        </div>\n    </main>\n    </body>\n\n    <script type="text/javascript" src="../themes/water3/assets/js/vendor-1495700986.js"></script>\n        <script type="text/javascript" src="../themes/water3/assets/js/app-1498189486.js"></script>\n\n</html>\n'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/subscription/sub-address.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], SubAddressPage);

//# sourceMappingURL=sub-address.js.map

/***/ })

});
//# sourceMappingURL=0.main.js.map