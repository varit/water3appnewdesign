webpackJsonp([42],{

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__bottle__ = __webpack_require__(324);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BottlePageModule", function() { return BottlePageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BottlePageModule = (function () {
    function BottlePageModule() {
    }
    return BottlePageModule;
}());
BottlePageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__bottle__["a" /* BottlePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__bottle__["a" /* BottlePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__bottle__["a" /* BottlePage */]
        ]
    })
], BottlePageModule);

//# sourceMappingURL=bottle.module.js.map

/***/ }),

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BottlePage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the BottlePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var BottlePage = (function () {
    function BottlePage(navCtrl, loadingCtrl, navParams, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.bottles = [];
        if (this.api.checkSession()) {
            var loading_1 = this.loadingCtrl.create({ content: "Please wait..." });
            loading_1.present();
            this.api.getUserBalance().subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                _this.balance = body.regular_credit_balance + body.subscription_credit_balance;
                _this.balance = _this.balance.toFixed(2);
            }, function (err) {
            });
            this.api.getBottleList().subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                for (var _i = 0, _a = body.data; _i < _a.length; _i++) {
                    var bottle = _a[_i];
                    var newbottle = {
                        bottle_name: bottle.bottle_name,
                        bottle_no: bottle.bottle_no
                    };
                    _this.bottles.push(newbottle);
                }
                _this.isLoaded = true;
                loading_1.dismiss();
            }, function (err) {
            });
        }
    }
    BottlePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TopupPage');
    };
    BottlePage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    BottlePage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    BottlePage.prototype.getIndex = function () {
        var activeIndex = this.slides.getActiveIndex();
        if (activeIndex >= this.bottles.length)
            activeIndex -= 1;
        console.log("Bottle id = " + activeIndex, this.bottles[activeIndex]["bottle_no"]);
        this.index = this.bottles[activeIndex]['bottle_no'];
    };
    BottlePage.prototype.toUpdate = function () {
        this.getIndex();
        localStorage.setItem('bottle_no', this.index);
        this.navCtrl.setRoot('BottleUpdatePage');
    };
    BottlePage.prototype.toNewBottle = function () {
        this.navCtrl.setRoot('BottleCreatePage');
    };
    BottlePage.prototype.slidePrev = function () {
        this.slides.slidePrev();
    };
    BottlePage.prototype.slideNext = function () {
        this.slides.slideNext();
    };
    BottlePage.prototype.slidePosition = function () {
        if (this.slides.isBeginning()) {
            return 1;
        }
        if (this.slides.isEnd()) {
            return 99;
        }
        return 0;
    };
    return BottlePage;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */])
], BottlePage.prototype, "slides", void 0);
BottlePage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-bottle',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/bottle/bottle.html"*/'<!DOCTYPE html>\n<html lang="en-AU" >\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <title>Filtered water: a healthier alternative to plastic bottled water</title>\n        <meta name="title" content="water3 | think better. be better. do more">\n        <meta name="description" content="water3 is revolutionising the way we drink water; via a network of innovative kiosks that provide fresh and healthy chilled spring water.">\n        <meta content="width=device-width, initial-scale=1" name="viewport">\n        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:400,700,300%7CUbuntu">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/style-1489479537.css">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/app-1486002319.css">\n        <link rel="shortcut icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <link rel="icon" href="favicon-1486002253.ico" type="image/x-icon">\n        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n                <script type="text/javascript">\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n    ga(\'create\', \'UA-83198216-1\', \'auto\');\n    ga(\'send\', \'pageview\');\n</script>\n    </head>\n        <body class="layout-default" overflow-scroll="true">\n        \n        <header class="page">\n        <div class="container">\n        <a class="branding" href="index.html" title="Home">\n            <img src="assets/img/water3-logo.png" alt="Water3">\n        </a>\n\n        <button class="open-navigation" type="button" aria-label="Toggle menu" (click)="openMenu()">\n            <span class="bar top"></span>\n            <span class="bar middle"></span>\n            <span class="bar bottom"></span>\n            <span class="text">Menu</span>\n        </button>\n\n        <a class="btn btn-top-up btn-circle btn-icon" (click)="toBottle()" title="Top up">\n            <span class="icon icon-top-up hidden-hover"></span>\n            <span class="icon icon-top-up-white visible-hover"></span>\n            <span class="text">Top Up</span>\n        </a>\n    </div>\n</header>\n        <ion-content>\n        <main class="page white">\n        <section class="component-section">\n            <div class="container">\n                <h1>My Bottles</h1>\n            </div>\n        </section>\n\n        <div class="content wide">\n            <section class="component component-bottle-list">\n                \n                \n                    <h2>Choose your bottle</h2>\n\n                    <div *ngIf="bottles.length>=1 && isLoaded">\n                \n                        <div>\n                                <ion-slides (ionSlideDidChange)="getIndex()" loop="false">\n                                    <ion-slide *ngFor="let bottle of bottles" data-id="ng-bind: bottle.bottle_no">\n                                        <div class="circle">\n                                            <h4>\n                                                &nbsp;\n                                                <span class="balance"></span>\n                                            </h4>\n                                        </div>\n\n                                        <img src="assets/img/bottle-small.png" alt="water3 bottle">\n\n                                        <h3 style="color:black;">{{ bottle.bottle_name }}</h3>\n                                        <p style="color:black;" class="number">{{ bottle.bottle_no }}</p>\n                                    </ion-slide>\n                                </ion-slides>\n                        </div>\n\n                        <button style="top:240px;" class="btn btn-a btn-circle btn-icon btn-carousel previous" type="button" (click)="slidePrev()">\n                            <span class="icon icon-arrow-left"></span>\n                            <span class="text">Previous</span>\n                        </button>\n\n                        <button style="top:240px;" class="btn btn-a btn-circle btn-icon btn-carousel next" type="button" (click)="slideNext()">\n                            <span class="icon icon-arrow-right"></span>\n                            <span class="text">Next</span>\n                        </button>\n\n\n\n                    </div>\n\n                    <div *ngIf="bottles.length==0 && isLoaded" class="bottles-empty">\n                        <span class="icon icon-waterdrop-sad"></span>\n                        <h2 style="color:black;">So Sad...</h2>\n                        <p style="color:black;">Sorry, you don\'t have any bottles registered under your account.</p>\n                    </div>\n\n                    <div class="btn-actions actions row">\n                        <div class="col-xs-6">\n                            <a class="btn btn-responsive btn-block btn-c" (click)="toUpdate()" title="Edit bottle" data-route="https://www.water3.com.au/bottles/:bottle_no">Edit bottle</a>\n                        </div>\n                        <div class="col-xs-6">\n                            <a class="btn btn-responsive btn-block btn-c" (click)="toBottle()" title="Top up">Top up</a>\n                        </div>\n                    </div>\n\n                    <div align="center" style="margin-top: 20px; color:black;">\n                        Get the most out of your bottles, <br>visit our <a (click)="navCtrl.setRoot(\'BottlecarePage\')">Bottle Care</a> section\n                    </div>\n                    <div style="padding-bottom: 40px;"></div>\n\n            </section>\n        </div>\n    </main>\n</ion-content>\n    <section class="component component-bottom-button responsive">\n    <div class="button-container">\n                <a style="width:100%;" class="btn btn-block btn-icon-inline btn-square btn-e" (click)="toNewBottle()">\n                        <span class="icon icon-plus"></span>\n                        Add new bottle\n        </a>\n    </div>\n</section>\n    </body>\n\n    <script type="text/javascript" src="assets/js/vendor-1486002319.js"></script>\n        <script type="text/javascript" src="assets/js/app-1486002319.js"></script>\n\n</html>\n'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/bottle/bottle.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], BottlePage);

//# sourceMappingURL=bottle.js.map

/***/ })

});
//# sourceMappingURL=42.main.js.map