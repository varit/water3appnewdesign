webpackJsonp([13],{

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__subscription__ = __webpack_require__(354);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubscriptionPageModule", function() { return SubscriptionPageModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SubscriptionPageModule = (function () {
    function SubscriptionPageModule() {
    }
    return SubscriptionPageModule;
}());
SubscriptionPageModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__subscription__["a" /* SubscriptionPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__subscription__["a" /* SubscriptionPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__subscription__["a" /* SubscriptionPage */]
        ],
        schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NO_ERRORS_SCHEMA */]]
    })
], SubscriptionPageModule);

//# sourceMappingURL=subscription.module.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_helper__ = __webpack_require__(200);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubscriptionPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { TopupPage } from '../topup/topup';
// import { SubWelcomePage } from '../subscription/welcome';
// import { PlanPage } from '../subscription/plan';
// import { SubDeletePage } from '../subscription/delete';
var SubscriptionPage = (function () {
    function SubscriptionPage(navCtrl, loadingCtrl, alertCtrl, helper, navParams, api, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.helper = helper;
        this.navParams = navParams;
        this.api = api;
        this.menuCtrl = menuCtrl;
        this.isLoaded = false;
        this.hasActive = false;
        this.hasPending = false;
        this.hasLast = false;
        if (this.api.checkSession()) {
            this.loading = this.loadingCtrl.create({ content: "Please wait..." });
            this.loading.present();
            this.api.getAccountInfo().subscribe(function (res) {
                //var status = res.status;
                var body = res.json();
                //console.log("Response : "+status+" BODY "+JSON.stringify(body));
                if (body.gender == null || body.birth_date == null) {
                    var alert_1 = _this.alertCtrl.create({
                        title: "Please update your profle",
                        subTitle: "Your profile is missing some information. Please complete your profile to proceed.",
                        buttons: ['OK']
                    });
                    alert_1.present(prompt);
                    _this.navCtrl.setRoot('AccountPage');
                }
                _this.credits = body.subscription_credit_balance;
                _this.credits = _this.credits.toFixed(2);
            });
            this.checkSub();
        }
    }
    SubscriptionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TopupPage - Subscription');
    };
    SubscriptionPage.prototype.checkSub = function () {
        var _this = this;
        this.api.getActiveSubscription().subscribe(function (res) {
            var status = res.status;
            var body = res.json();
            console.log("Response : " + status + " BODY " + JSON.stringify(body));
            if (body != null && body != "") {
                _this.hasActive = true;
                _this.Active = body;
            }
            _this.api.getPendingSubscription().subscribe(function (res) {
                var status = res.status;
                var body = res.json();
                console.log("Response : " + status + " BODY " + JSON.stringify(body));
                if (body != null && body != "") {
                    _this.hasPending = true;
                    _this.Pending = body;
                }
                _this.api.getLastSubscription().subscribe(function (res) {
                    var status = res.status;
                    var body = res.json();
                    console.log("Response : " + status + " BODY " + JSON.stringify(body));
                    if (body != null && body != "") {
                        _this.hasLast = true;
                        _this.Last = body;
                    }
                    _this.redirect();
                }, function (err) {
                    var alert = _this.alertCtrl.create({
                        title: "Error " + err.status,
                        subTitle: "Cannot retrieve subscription data",
                        buttons: ['OK']
                    });
                    alert.present(prompt);
                });
            }, function (err) {
                var alert = _this.alertCtrl.create({
                    title: "Error " + err.status,
                    subTitle: "Cannot retrieve subscription data",
                    buttons: ['OK']
                });
                alert.present(prompt);
            });
        }, function (err) {
            var alert = _this.alertCtrl.create({
                title: "Error " + err.status,
                subTitle: "Cannot retrieve subscription data",
                buttons: ['OK']
            });
            alert.present(prompt);
        });
    };
    SubscriptionPage.prototype.redirect = function () {
        if (!this.hasActive && !this.hasPending && !this.hasLast) {
            this.navCtrl.setRoot('SubWelcomePage');
            this.loading.dismiss();
        }
        else if (this.Last.creditExpireDate == null) {
            this.navCtrl.setRoot('SubWelcomePage');
            this.loading.dismiss();
        }
        else {
            this.isLoaded = true;
            this.loading.dismiss();
        }
    };
    SubscriptionPage.prototype.toPlan = function () {
        this.navCtrl.setRoot('PlanPage');
    };
    SubscriptionPage.prototype.toDelete = function () {
        this.navCtrl.setRoot('SubDeletePage');
    };
    SubscriptionPage.prototype.openMenu = function () {
        //push another page onto the history stack
        //causing the nav controller to animate the new page in
        this.menuCtrl.toggle();
    };
    SubscriptionPage.prototype.toBottle = function () {
        this.navCtrl.setRoot('TopupPage');
    };
    return SubscriptionPage;
}());
SubscriptionPage = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_7" /* Component */])({
        selector: 'page-subscription',template:/*ion-inline-start:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/subscription/subscription.html"*/'\n<!DOCTYPE html>\n<html lang="en-AU" >\n    <head>\n        <meta charset="utf-8">\n        <meta http-equiv="X-UA-Compatible" content="IE=edge">\n        <title>Filtered water: a healthier alternative to plastic bottled water</title>\n        <meta name="title" content="water3 |     Top Up | Amount\n">\n        <meta name="description" content="water3 is revolutionising the way we drink water; via a network of innovative kiosks that provide fresh and healthy chilled spring water.">\n        <meta content="width=device-width, initial-scale=1" name="viewport">\n        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:400,700,300%7CUbuntu">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/style-1489479537.css">\n        <link rel="stylesheet" type="text/css" href="../../assets/css/app-1486002319.css">\n        <link rel="shortcut icon" href="../favicon-1495700985.ico" type="image/x-icon">\n        <link rel="icon" href="../favicon-1495700985.ico" type="image/x-icon">\n        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>\n                <script type="text/javascript">\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n\n    ga(\'create\', \'UA-83198216-1\', \'auto\');\n    ga(\'send\', \'pageview\');\n</script>\n    </head>\n        <body class="layout-portal" overflow-scroll="true">\n        \n        <header class="page">\n        <div class="container">\n        <a class="branding" href="index.html" title="Home">\n            <img src="assets/img/water3-logo.png" alt="Water3">\n        </a>\n\n        <button class="open-navigation" type="button" aria-label="Toggle menu" (click)="openMenu()">\n            <span class="bar top"></span>\n            <span class="bar middle"></span>\n            <span class="bar bottom"></span>\n            <span class="text">Menu</span>\n        </button>\n\n        <a class="btn btn-top-up btn-circle btn-icon" (click)="toBottle()" title="Top up">\n            <span class="icon icon-top-up hidden-hover"></span>\n            <span class="icon icon-top-up-white visible-hover"></span>\n            <span class="text">Top Up</span>\n        </a>\n    </div>\n</header>\n<ion-content style="background-color: #368ba1">\n<main *ngIf="isLoaded" class="page home with-bottom-button">\n    <div class="sub-form-wrap">\n        <section id="hero" class="subscribe-wrap sub-info-main">\n            <div class="subscribe-title">\n                <div class="container">\n                    <h1>Subscription Information</h1>\n                </div>\n            </div>\n            <div class="subscription-info-pan">\n                <div class="subscription-info-pan-part">\n                    <div class="sub-info-wrap">\n                        <h1 *ngIf="hasActive" class="sub-info-title">Current Plan</h1>\n                        <h1 *ngIf="!hasActive && hasLast" class="sub-info-title">Previous Plan</h1>\n                        <p class="sub-info-text">Credits Remaining: <span>${{credits}},</span></p>\n                        <p *ngIf="hasActive" class="sub-info-text">Next Payment: <span>25 Aug 2017</span></p>\n                        <p *ngIf="!hasActive && hasLast" class="sub-info-text">Expire: <span>{{ helper.convert_time(Last.creditExpireDate.iso,"subscription") }}</span></p>\n                    </div>\n                    <div class="subscription-btn-section-inner dollars subscription-info-dollar subscription-info-pan-part-left">\n                        <div class="sub-info-dollar-conatiner sub-info-dollar-conatiner-margin">\n                            <div class="refill-circle2">\n                                <div class="refill-circle-inner2 dark-active active-circle-new">\n                                    <svg width="120" height="120" viewBox="0 0 42 42" class="donut">\n\n                                        <defs>\n                                            <linearGradient id="grad1">\n                                                <stop offset="40%" stop-color="#03b0d7"/>\n                                                <stop offset="90%" stop-color="#dae031"/>\n                                            </linearGradient>\n                                        </defs>\n\n                                        <circle class="donut-hole" cx="21" cy="21" r="18.8" fill="#166788"/>\n                                        <circle class="donut-ring" cx="21" cy="21" r="20" fill="transparent" stroke="transparent" stroke-width="1"/>\n                                        <circle class="donut-segment" cx="21" cy="21" r="20" fill="transparent" stroke="url(#grad1)" stroke-width="1" stroke-dasharray="100 0" stroke-dashoffset="0" stroke-linecap="round"/>\n                                        <g class="chart-text">\n                                            <text x="50%" y="50%" class="chart-number">\n                                            </text>\n                                        </g>\n                                        \n                                    </svg>\n                                    <div class="refill-count">\n                                        <strong *ngIf="hasActive">{{Last.plan.substr(0,5)+" "+Last.plan.substr(5)}}</strong>\n                                        <strong *ngIf="!hasActive && hasLast">{{Last.plan.substr(0,5)+" "+Last.plan.substr(5)}}</strong>\n                                        <strong *ngIf="!hasActive && !hasLast">-</strong>\n                                    </div>\n                                    <p class="circle-bottom-text">Plan</p>\n                                </div>\n                            </div>\n\n                            <div class="refill-circle2 dark-active active-circle-new">\n                                <div class="refill-circle-inner2">\n                                    <svg width="120" height="120" viewBox="0 0 42 42" class="donut">\n                                        <defs>\n                                            <linearGradient id="grad1">\n                                                <stop offset="40%" stop-color="#03b0d7"/>\n                                                <stop offset="90%" stop-color="#dae031"/>\n                                            </linearGradient>\n                                        </defs>\n                                        <circle class="donut-hole" cx="21" cy="21" r="18.8" fill="#166788"/>\n                                        <circle class="donut-ring" cx="21" cy="21" r="20" fill="transparent" stroke="transparent" stroke-width="1"/>\n                                        <circle class="donut-segment" cx="21" cy="21" r="20" fill="transparent" stroke="url(#grad1)" stroke-width="1" stroke-dasharray="100 0" stroke-dashoffset="0" stroke-linecap="round"/>\n                                        <g class="chart-text">\n                                            <text x="50%" y="50%" class="chart-number">\n                                                \n                                            </text>\n                                        </g>\n                                    </svg>\n                                    <div class="refill-count">\n                                        <strong *ngIf="hasActive">${{Active.priceAmount}}</strong>\n                                        <strong *ngIf="!hasActive && hasLast">${{Last.priceAmount}}</strong>\n                                        <strong *ngIf="!hasActive && !hasLast">-</strong>\n                                    </div>\n                                    <p class="circle-bottom-text">Price</p>\n                                </div>\n                            </div>\n\n                            <div class="refill-circle2 dark-active active-circle-new">\n                                <div class="refill-circle-inner2">\n                                    <svg width="120" height="120" viewBox="0 0 42 42" class="donut">\n                                        <defs>\n                                            <linearGradient id="grad1">\n                                                <stop offset="40%" stop-color="#03b0d7"/>\n                                                <stop offset="90%" stop-color="#dae031"/>\n                                            </linearGradient>\n                                        </defs>\n                                        <circle class="donut-hole" cx="21" cy="21" r="18.8" fill="#166788"/>\n                                        <circle class="donut-ring" cx="21" cy="21" r="20" fill="transparent" stroke="transparent" stroke-width="1"/>\n                                        <circle class="donut-segment" cx="21" cy="21" r="20" fill="transparent" stroke="url(#grad1)" stroke-width="1" stroke-dasharray="100 0" stroke-dashoffset="0" stroke-linecap="round"/>\n                                        <g class="chart-text">\n                                            <text x="50%" y="50%" class="chart-number">\n                                                \n                                            </text>\n                                        </g>\n                                    </svg>\n                                    <div class="refill-count">\n                                        <strong *ngIf="hasActive">{{Active.creditAmount}}</strong>\n                                        <strong *ngIf="!hasActive && hasLast">{{Last.creditAmount}}</strong>\n                                        <strong *ngIf="!hasActive && !hasLast">-</strong>\n                                    </div>\n                                    <p class="circle-bottom-text">Credit</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <a *ngIf="hasActive" (click)="toDelete()"class="btn sub-info-btn">Unsubscribe</a>\n                    <a *ngIf="!hasActive && !hasPending" (click)="toPlan()" class="btn sub-info-btn">New Subscription</a>\n                    <p *ngIf="hasPending" class="sub-info-text">Your pending plan will become active on the active date or when you use up the remaining credits.</p>\n                </div>\n                <div class="subscription-info-pan-part">\n                    <div class="sub-info-wrap">\n                        <h1 class="sub-info-title">Pending Plan</h1>\n                        <p class="sub-info-text">Active Date: <span *ngIf="hasPending">{{helper.convert_time(Pending.activedate.iso,\'subscription\')}}</span></p>\n                        \n                    </div>\n                    <div class="subscription-btn-section-inner dollars subscription-info-dollar">\n                        <div class="sub-info-dollar-conatiner sub-info-dollar-conatiner-margin">\n                            <div class="refill-circle2 sub-info-refill">\n                                <div class="refill-circle-inner2">\n                                    <svg width="120" height="120" viewBox="0 0 42 42" class="donut">\n\n                                        <defs>\n                                            <linearGradient id="grad1">\n                                                <stop offset="40%" stop-color="#03b0d7"/>\n                                                <stop offset="90%" stop-color="#dae031"/>\n                                            </linearGradient>\n                                        </defs>\n                                        <circle class="donut-hole" cx="21" cy="21" r="18.8" fill="#166788"/>\n                                        <circle class="donut-ring" cx="21" cy="21" r="20" fill="transparent" stroke="transparent" stroke-width="1"/>\n                                        <circle class="donut-segment" cx="21" cy="21" r="20" fill="transparent" stroke="url(#grad1)" stroke-width="1" stroke-dasharray="100 0" stroke-dashoffset="0" stroke-linecap="round"/>\n                                        <g class="chart-text">\n                                            <text x="50%" y="50%" class="chart-number">\n                                                \n                                            </text>\n                                        </g>\n                                    </svg>\n                                    <div class="refill-count">\n                                        <strong *ngIf="hasPending">{{Pending.plan.substr(0,5)+" "+Pending.plan.substr(5)}}</strong>\n                                        <strong *ngIf="!hasPending">-</strong>\n                                    </div>\n                                    <p class="circle-bottom-text">Plan</p>\n                                </div>\n                            </div>\n\n                            <div class="refill-circle2 sub-info-refill">\n                                <div class="refill-circle-inner2">\n                                    <svg width="120" height="120" viewBox="0 0 42 42" class="donut">\n                                        <defs>\n                                            <linearGradient id="grad1">\n                                                <stop offset="40%" stop-color="#03b0d7"/>\n                                                <stop offset="90%" stop-color="#dae031"/>\n                                            </linearGradient>\n                                        </defs>\n                                        <circle class="donut-hole" cx="21" cy="21" r="18.8" fill="#166788"/>\n                                        <circle class="donut-ring" cx="21" cy="21" r="20" fill="transparent" stroke="transparent" stroke-width="1"/>\n                                        <circle class="donut-segment" cx="21" cy="21" r="20" fill="transparent" stroke="url(#grad1)" stroke-width="1" stroke-dasharray="100 0" stroke-dashoffset="0" stroke-linecap="round"/>\n                                        <g class="chart-text">\n                                            <text x="50%" y="50%" class="chart-number">\n                                                \n                                            </text>\n                                        </g>\n                                    </svg>\n                                    <div class="refill-count">\n                                        <strong *ngIf="hasPending">${{ Pending.priceAmount }}</strong>\n                                        <strong *ngIf="!hasPending">-</strong>\n                                    </div>\n                                    <p class="circle-bottom-text">Price</p>\n                                </div>\n                            </div>\n\n                            <div class="refill-circle2 sub-info-refill">\n                                <div class="refill-circle-inner2">\n                                    <svg width="120" height="120" viewBox="0 0 42 42" class="donut">\n                                        <defs>\n                                            <linearGradient id="grad1">\n                                                <stop offset="40%" stop-color="#03b0d7"/>\n                                                <stop offset="90%" stop-color="#dae031"/>\n                                            </linearGradient>\n                                        </defs>\n                                        <circle class="donut-hole" cx="21" cy="21" r="18.8" fill="#166788"/>\n                                        <circle class="donut-ring" cx="21" cy="21" r="20" fill="transparent" stroke="transparent" stroke-width="1"/>\n                                        <circle class="donut-segment" cx="21" cy="21" r="20" fill="transparent" stroke="url(#grad1)" stroke-width="1" stroke-dasharray="100 0" stroke-dashoffset="0" stroke-linecap="round"/>\n                                        <g class="chart-text">\n                                            <text x="50%" y="50%" class="chart-number">\n                                                \n                                            </text>\n                                        </g>\n                                    </svg>\n                                    <div class="refill-count">\n                                        <strong *ngIf="hasPending">{{ Pending.creditAmount}}</strong>\n                                        <strong *ngIf="!hasPending">-</strong>\n                                    </div>\n                                    <p class="circle-bottom-text">Credit</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <a *ngIf="hasPending" (click)="toDelete()" class="btn sub-info-btn">Cancel</a>\n                </div>\n                <div class="clearfix"></div>\n            </div>\n        </section>\n    </div>\n</main>\n    </ion-content>\n    </body>\n\n\n    <script type="text/javascript" src="../themes/water3/assets/js/vendor-1495700986.js"></script>\n    <script type="text/javascript" src="../themes/water3/assets/js/app-1498189486.js"></script>\n\n</html>\n'/*ion-inline-end:"/Users/vmimac/Desktop/ionic-app-erp3/src/pages/subscription/subscription.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__services_helper__["a" /* Helper */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* MenuController */]])
], SubscriptionPage);

//# sourceMappingURL=subscription.js.map

/***/ })

});
//# sourceMappingURL=13.main.js.map