import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class Helper {

  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	constructor() {

  }

  dateQuery(date:Date, day_offset=0,month_offset=0,year_offset=0) {

    var day = date.getDate();
    var month = date.getMonth()-(month_offset-1);
    var year = date.getFullYear();

    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    return ""+year+"-"+month+"-"+day+" "+hour+":"+min+":"+sec;
  }

  convert_tz(date, full=false) {

    var day = date.substr(0,2);
    var month = this.months[Number(date.substr(3,2))-1];
    var year = date.substr(6,4);

    var hour = date.substr(11,2);
    var min =  date.substr(14,2);

    // 05-09-2017 14:42:16
    var ampm;

    if(Number(hour)>=0 && Number(hour)<12) {
      ampm = "am"
    }
    if(Number(hour)>=12 && Number(hour)<24) {
      hour -= 12;
      ampm = "pm"
    }

    if(full)
      return ""+day+" "+month+" "+year+", "+hour+":"+min+ampm;
    if(!full)
      return ""+day+" "+month.substr(0,3)+" "+year+", "+hour+":"+min+ampm;
    
  }

  convert_time(date,mode="") {
    var d = new Date(date);

    var day = d.getDate();
    var month = this.months[d.getMonth()];
    var year = d.getFullYear();

    var hour = d.getHours();
    var min = d.getMinutes();

    var hours;
    var mins;

    var ampm;

    if(hour>=0 && hour<12) {
      hours = "0"+hour;
      ampm = "am"
    }
    if(hour>=12 && hour<24) {
      hour -= 12;
      ampm = "pm"
    }
    if(min>=0 && min<10) {
      mins = "0"+min;
    }

    if(mode="subscription") {
      return ""+day+" "+month.substr(0,3)+" "+year;
    }

  }


}