
import { Injectable , ViewChild } from '@angular/core';
import { Http , Headers , RequestOptions } from '@angular/http';
import { ENV } from './env';

import { Events } from 'ionic-angular';

import { Nav , AlertController } from 'ionic-angular';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class Api {

  @ViewChild(Nav) nav: Nav;

  message: string;
  endpoint: string;
  expiry;
  credential = { email: '', password: ''};

	constructor(public http:Http, public env:ENV , public alertCtrl:AlertController, public event:Events) {

    console.log("API:: construct");

    this.message = null;
    this.endpoint = env.endpoint();
    this.expiry = env.getTokenLife();

  }

  endPoint(url) {

    if(url=="login") {
      return this.endpoint+"api/login";
    }
    if(url.indexOf("refresh") !== -1) {
      return this.endpoint+"api/"+url;
    }

    return this.endpoint+"web_api/"+url;

  }

  authenticate(){

    this.credential.email = localStorage.getItem('email');
    this.credential.password = localStorage.getItem('password');

    console.log("API:: authenthicate()");
    return this.http.post(this.endPoint("login"), JSON.stringify(this.credential) , this.options());

  }

  facebook(email,first_name,last_name,id,token) {

    var data = {
      email : email,
      first_name : first_name,
      last_name : last_name,
      facebook_id : id,
      facebook_token : token
    };

    return this.http.post(this.endPoint("facebook/login"), JSON.stringify(data) , this.options());
  }

  decodeBottleTag(tag){
    return this.http.get(this.endPoint("rfid/"+tag) , this.options());
  }

  getBottleList() {
    console.log("API:: getBottleList()");
    //console.log("Request : "+this.endPoint("user/bottles")+" "+ JSON.stringify(this.options()));

    return this.http.get(this.endPoint("user/bottles") , this.options());
  }

  addBottle(bottle) {
    console.log("API():: addBottle()");
    console.log("Request : POST "+this.endPoint("user/bottles") + JSON.stringify(bottle) + JSON.stringify(this.options()));
    return this.http.post(this.endPoint("user/bottles") , JSON.stringify(bottle) , this.options());
  }
  updateBottle(bottle) {
    console.log("API():: updateBottle()");
    //console.log("Request : PUT "+this.endPoint("user/bottles")+"/"+bottle.bottle_no+" "+ JSON.stringify(bottle) + JSON.stringify(this.options()));
    return this.http.put(this.endPoint("user/bottles/"+bottle.bottle_no) , JSON.stringify(bottle) , this.options());
  }

  deleteBottle(bottle) {
    console.log("API():: deleteBottle()");
    //console.log("Request : DELETE "+this.endPoint("user/bottles")+"/"+bottle.bottle_no+" "+ JSON.stringify(bottle) + JSON.stringify(this.options()));
    return this.http.delete(this.endPoint("user/bottles/"+bottle.bottle_no) , this.options());
  }

  getBottle(bottleId) {
    console.log("API:: getBottle()");

    //console.log("Request : "+this.endPoint("user/bottles/")+bottleId+ JSON.stringify(this.options()));

    return this.http.get(this.endPoint("user/bottles/"+bottleId) , this.options());
  }

  changeUserPassword(data) {
    console.log("API:: ChangeUserPassword");
    //console.log("Request : POST"+this.endPoint("password/change")+" BODY "+JSON.stringify(data)+" "+JSON.stringify(this.options()));
    return this.http.post(this.endPoint("password/change") , JSON.stringify(data), this.options());
    
  }

  register(account) {
    console.log("API:: Register New User");
    console.log("Request : POST"+this.endPoint("user/create")+" BODY "+JSON.stringify(account)+" "+JSON.stringify(this.options()));
    return this.http.post(this.endPoint("user/create") , JSON.stringify(account), this.options());
  }

  getReferCode() {
    return this.http.get(this.endPoint("referral/code/get") , this.options());
  }

  checkReferCode(code) {
    console.log("Request : GET"+this.endPoint("referral/code/"+code)+JSON.stringify(this.options()));
    return this.http.get(this.endPoint("referral/code/"+code) , this.options());
  }

  useReferCode(code) {
    return this.http.get(this.endPoint("referral/code/use/"+code) , this.options());
  }

  getAccountInfo() {

    var response = this.http.get(this.endPoint("user") , this.options());

    response.subscribe(res => {
        var body = res.json();
        //console.log("Response : "+status+" BODY "+JSON.stringify(body));

        localStorage.setItem("first_name",body.first_name);
        localStorage.setItem("last_name",body.last_name);
        localStorage.setItem("email",body.email);
        localStorage.setItem("phone",body.phone);
        localStorage.setItem("address_1",body.address_1);
        localStorage.setItem("address_2",body.address_2);
        localStorage.setItem("state_id",body.state_id);
        localStorage.setItem("city",body.city);
        localStorage.setItem("country_id",body.country_id);
        localStorage.setItem("gender",body.gender);
        localStorage.setItem("birth_date",body.birth_date);
        localStorage.setItem("stripe_customer_id",body.stripe_customer_id);
        
        }, (err) => {
        
        });

    var ip = this.http.get('http://ipv4.myexternalip.com/json');
    ip.subscribe(res => {

        var body = res.json();
        console.log("ip = "+body.ip)
        localStorage.setItem("ip",body.ip);
        
        }, (err) => {
        
        });

    return response;
  }

  getCountry() {
    console.log("API :: getCountry() is executed");
    //console.log("Request : "+this.endpoint+" "+ JSON.stringify(this.options()));
    return this.http.get(this.endPoint("countries") , this.options());
  }

  getState(countryID) {
    console.log("API():: getState()");
    //console.log("Request : "+this.endPoint("states/"+countryID)+ " "+ JSON.stringify(this.options()));
    return this.http.get(this.endPoint("states/"+countryID) , this.options());

  }

  redeemCode(code) {
    return this.http.get(this.endPoint("coupon/redeem/"+code) , this.options());
  }

  updateUserAddress(address) {

    console.log("API:: updateUserAddress()");
    console.log("Request : PUT "+this.endPoint("user")+ JSON.stringify(address) + JSON.stringify(this.options()));
    return this.http.put(this.endPoint("user") , JSON.stringify(address) , this.options());

  }

  getUserBalance() {
    console.log("API:: getUserBalance() is executed");
    //console.log("Request : "+this.endpoint+" "+ JSON.stringify(this.options()));
    return this.http.get(this.endPoint("user") , this.options());

  }

  getUserCreditCards() {
    console.log('API:: getUserCreditCards()');
    //console.log("Request : "+this.endpoint+"stripe/user/cards "+ JSON.stringify(this.options()));
    return this.http.get(this.endPoint("stripe/user/cards") , this.options());
  }

  deleteCard(card_id) {
    console.log('API:: deleteCard()');
    //console.log("Request : DELETE "+this.endpoint+"stripe/user/card"+card_id+" "+ JSON.stringify(this.options()));
    return this.http.delete(this.endPoint("stripe/user/card/"+card_id) , this.options());
  }

  getTransactions(startDate, endDate) {
    console.log("API:: getTransactions() is executed");
    //console.log("Request : "+this.endpoint+"transaction?start="+startDate+"&end="+endDate+ JSON.stringify(this.options()));
    return this.http.get(this.endPoint("transaction?start="+startDate+"&end="+endDate) , this.options());
  }

  requestResetPassword(email) {
    console.log("API:: requestResetPassword()");
    return this.http.get(this.endPoint("password/token/"+email) , this.options());
  }

  verifyResetToken(token) {
    console.log("API:: verifyResetToken()");
    //console.log("Request : GET"+this.endPoint("password/validate/"+token) + JSON.stringify(this.options()));
    return this.http.get(this.endPoint("password/validate/"+token) , this.options());
  }

  resetPassword(token,password) {
    console.log("API:: resetPassword()");
    var data = {
      token : token,
      password : password
    };
    //console.log("Request : POST"+this.endPoint("password/reset/") + JSON.stringify(data) + JSON.stringify(this.options()));
    return this.http.post(this.endPoint("password/reset") , JSON.stringify(data), this.options());
  }

  charge(token,save,currency="AUD",isCard=false) {

    var isSave;
    var data;
    var ip;

    if(localStorage.getItem("ip") == null || localStorage.getItem("ip") == ""){
      ip = "127.0.0.1";
    } else {
      ip = localStorage.getItem("ip");
    }

    if(save)
      isSave = "true";
    if(!save)
      isSave = "false";
    if(!isCard) {
      data = {
        source : token,
        amount : localStorage.getItem("amount"),
        currency : currency,
        description : 'water3 credit topup for '+localStorage.getItem("amount")+" AUD "+" / "+localStorage.getItem("email"),
        ip : ip,
        save_card : isSave
      };
    }
    if(isCard) {
      data = {
        source : token,
        amount : localStorage.getItem("amount"),
        currency : currency,
        description : 'water3 credit topup for '+localStorage.getItem("amount")+" AUD "+" / "+localStorage.getItem("email"),
        ip : ip,
      };
    }
    console.log("API:: charge()");
    //console.log(JSON.stringify(data));
    //console.log("Request : "+this.endpoint+"stripe/user/charge"+ JSON.stringify(data) + JSON.stringify(this.options()));

    return this.http.post(this.endPoint("stripe/user/charge"), JSON.stringify(data) , this.options());

  }

  subscribe(data) {
    console.log("API:: subscribe()");
    //console.log("Request : "+this.endpoint+"user/subscription"+ JSON.stringify(data) + JSON.stringify(this.options()));
    return this.http.post(this.endPoint("user/subscription"), JSON.stringify(data) , this.options());
  }

  unsubscribe(subID,mode) {
    console.log("API:: unsubscribe()");
    
    if(mode=="active") {
      //console.log("Request : DELETE "+this.endpoint+"user/subscription"+subID + JSON.stringify(this.options()));
      return this.http.delete(this.endPoint("user/subscription/"+subID) , this.options());
    }
    //console.log("Request : DELETE "+this.endpoint+"user/subscription/pending" + JSON.stringify(this.options()));
    return this.http.delete(this.endPoint("user/subscription/pending"), this.options());
  }

  getActiveSubscription() {
    console.log("API:: getActiveSubscription()");
    //console.log("Request : "+this.endpoint+"user/subscription" + JSON.stringify(this.options()));
    return this.http.get(this.endPoint("user/subscription"), this.options());
  }

  getPendingSubscription() {
    console.log("API:: getPendingSubscription()");
    //console.log("Request : "+this.endpoint+"user/subscription/pending" + JSON.stringify(this.options()));
    return this.http.get(this.endPoint("user/subscription/pending"), this.options());
  }

  getLastSubscription() {
    console.log("API:: getHistorySubscription()");
    //console.log("Request : "+this.endpoint+"user/subscription/last" + JSON.stringify(this.options()));
    return this.http.get(this.endPoint("user/subscription/last"), this.options());
  } 

  getProducts() {
    console.log("API:: getProducts()");
    return this.http.get(this.endPoint("store/products"));
  }

  getProductsByID(prod_id) {
    console.log("API:: getProductsByID()");
    return this.http.get(this.endPoint("store/products/"+prod_id));
  }

  updateBillingAddress(data) {
    console.log("API:: updateBillingAddress()");
    return this.http.put(this.endPoint("store/updateBillingAddress"), JSON.stringify(data)  ,this.options());
  }

  updateShippingAddress(data) {
    console.log("API:: updateShippingAddress()");
    return this.http.put(this.endPoint("store/updateShippingAddress"), JSON.stringify(data) , this.options());
  }

  getShippingAddress() {
    return this.http.get(this.endPoint("store/getShippingAddress"), this.options());
  }

  postOrder(data) {
    console.log("API:: postOrder()");
    console.log("Request : "+this.endpoint+"store/confirmOrder" + JSON.stringify(data) + JSON.stringify(this.options()));
    return this.http.post(this.endPoint("store/confirmOrder"), JSON.stringify(data) , this.options());
  }

  options(token?) {

    var headers = new Headers();
    
    if(token)
      headers.append('Authorization', "Bearer "+token);
    else
      headers.append('Authorization', "Bearer "+localStorage.getItem("token"));

    let options = new RequestOptions({headers:headers});
    return options;

  }



  checkSession() {

    var currentDate = new Date();
    var expire = localStorage.getItem("expiry");
    var token = localStorage.getItem('token');
    var refresh = localStorage.getItem('refresh');
    var expireDate = new Date(expire);
    console.log("API:: checkSession() executed");
    console.log("API:: checkSession() current time = "+currentDate.toString());
    console.log("API:: checkSession() expiry time = "+expire);
    if(currentDate > expireDate || token == "" || token == null) {

      this.refreshToken().subscribe(res => {
        console.log("Expired, Attempting to refresh",refresh);
        var body = res.json();
        console.log(body);
        var expiryTime = new Date();
        localStorage.setItem("token",body.access_token);
        localStorage.setItem("refresh",body.refresh_token);
        //localStorage.setItem("isLoggedin","1");
        localStorage.setItem("expiry",new Date(expiryTime.getTime() + (this.expiry*1000*60)).toString());
        this.event.publish('refreshPage',"true");
        return true;
      }, (err) => {
        console.log("Refresh failed : session has ended");
        localStorage.setItem("token","");
        localStorage.setItem("isLoggedin","0");
        this.event.publish('tokenUpdated',localStorage.getItem('token'));
        this.event.publish('expire',"true");
        return false;
      });     
    }
    else {
      return true;
    }

  }

  refreshToken() {
    var refresh = localStorage.getItem('refresh');

    return this.http.post(this.endPoint("users/refresh/token/"+refresh), JSON.stringify(""));
  }

  getStripePK() {
    return this.env.getStripePK();
  }

  getCurrency() {
    return this.http.get(this.endPoint("currency"), this.options());
  }

  exchangeCurrency(mode,exchangeTo) {
    console.log("API:: exchangeCurrency()");
    return this.http.get(this.endPoint("currency/exchange/"+mode+"/"+exchangeTo), this.options());
  }

	AdminLogin() : any{  

    //var fetch = true;
    var auth_token = "";

  	console.log("API:: AdminLogin() is executed");

    var headers = new Headers();
      
    headers.append('Authorization', auth_token);

    let postParams = {
        email: 'webapi@vmi.asia',
        password: 'webwater3vmi'
    }
    console.log("Request : "+this.endPoint("login")+" BODY "+JSON.stringify(postParams)+ JSON.stringify(this.options()));
    return this.http.post(this.endPoint("login"), JSON.stringify(postParams) , this.options());

  }

  sendEmail(param) {
    console.log("Request : POST"+this.endPoint("email/send")+" BODY "+JSON.stringify(param)+ JSON.stringify(this.options()));
    return this.http.post(this.endPoint("email/send"), JSON.stringify(param) , this.options());
  }

  findKiosk(lat,long,keyword,distance) {

    if(lat==0 || lat==null || long==0 || long==null) {
    }

    console.log("Request : "+this.endPoint("kiosk/find?longitude="+long+"&latitude="+lat+"&meter_distance="+distance+"&keyword="+keyword));

    return this.http.get(this.endPoint("kiosk/find?longitude="+long+"&latitude="+lat+"&meter_distance="+distance+"&keyword="+keyword), this.options());

  }

  

}