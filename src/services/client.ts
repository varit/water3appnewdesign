import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { ENV } from './env';

@Injectable()
export class Client {

  message: string;

	constructor(public http: Http, public env: ENV) {

    this.message = null;

  }

  get(url:string , options? , admin=false) : any {

    if(!admin) {
      return this.request('get',url, null, this.options());
    }

    return this.request('get',url,null,this.adminAuth());

  }

  
  post(url:string , body:any , admin=false) : any {

    if(!admin) {
      return this.request('post',url, body, this.options());
    }
    return this.request('post',url, body, this.adminAuth());
  }

  put(url:string , params:any ) : any {

    return this.request('put',url,null,this.options());

  }

  delete(url:string , params:any ) : any {

    return this.request('delete',url,null,this.options());

  }

  options() : any {

    var token = this.authenticate();

    var auth_token = "Bearer "+token;

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );

    headers.append('Authorization', auth_token);

    return headers;

  }


  adminAuth() {

    var token = this.authenticate();

    var auth_token = "Bearer "+token;

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );

      
    headers.append('Authorization', auth_token);
    return headers;


  }

  request( method:string , url:string , body:any , headers:Headers) {



    var endpoint = this.env.endpoint();
    var response;
    if(url=="login")
      endpoint += "api/login";
    else
      endpoint += "web_api/"+endpoint;

    let options = new RequestOptions({headers:headers});

    console.log('Request: '+method+' to '+endpoint+' BODY '+JSON.stringify(body));

    if(method=='post') {
      response = this.http.post(endpoint, JSON.stringify(body),options);
      return this.handle(response);
    }
    if(method=='get') {
      response = this.http.get(endpoint, options);
      return this.handle(response);
    }
    if(method=='put') {
      response = this.http.put(endpoint, JSON.stringify(body),options);
      return this.handle(response);
    }

  }

  handle( response: any) : any {

    var status;
    var body;
    response.subscribe(res => {
      status = res.status;
      body = res.json();

      console.log('Response: '+status+' Body: '+JSON.stringify(body));

      if(status == 200){

      }
      if(status == 204){

      }
      if(status == 422){

      }
      if(status == 400){

      }
      if(status < 200 || status > 299){

      }

      return body;

    });


  }

  authenticate() {



  }

  

}