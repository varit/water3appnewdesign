import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ENV {

  apiURL : string;
  apiAdmin : string;
  apiPass : string;
  tokenLife : any;
  platform : string;

  stripe_pk;
  stripe_sk;

	constructor() {

    this.apiURL = "https://test-erp-backend.vmi.asia/";
    //this.apiURL = "https://prod-erp-aus-brisbane-backend.water3.com.au/";
    // this.apiURL = "https://test-erp2-backend.water3.com.au/";
    // this.apiURL = "https://prod-erp2-backend.water3.com.au/";
    this.apiAdmin = "webapi@vmi.asia";
    this.apiPass = "webwater3vmi";
    this.tokenLife = 20;

    this.platform = "website";

     this.stripe_sk = "sk_test_MtRLvIx61x4SqSybvPDVmnyp";
     this.stripe_pk = "pk_test_aADjKUWVAg8GEq0QsMb5ncDj";
    // this.stripe_sk = "sk_live_ApsuY4Q9J1MfGVQkVxltPXxR";
    // this.stripe_pk = "pk_live_bR0DHIYJaenqfuxEm6pAcifZ";

  }

  endpoint() : string {
    return this.apiURL;
  }

  getTokenLife() {
    return this.tokenLife;
  }
  
  getStripeSK() {
    return this.stripe_sk;
  }

  getStripePK() {
    return this.stripe_pk;
  }

}