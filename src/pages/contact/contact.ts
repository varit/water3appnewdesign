import { Component } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , AlertController, MenuController } from 'ionic-angular';


import { Api } from '../../services/api';

/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  isLoggedin=false;
  data = {
  	first_name:'',
  	last_name:'',
  	phone:'',
  	email:'',
  	message:'',
  	type:'Account'
  };

  itemAmount;

constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public alertCtrl:AlertController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {

      if(localStorage.getItem("isLoggedin")=="1")
    this.isLoggedin = true;

     this.itemAmount = localStorage.getItem("itemAmount");
    if(this.itemAmount == null || this.itemAmount <= 0)
      this.itemAmount = 0;
    else
      this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
  }


  sentEnquiry() {

  	let loading = this.loadingCtrl.create({content : "Please wait..."});
    loading.present();

  	if(localStorage.getItem("token") == "" || localStorage.getItem("token") == null) {

  		this.api.AdminLogin().subscribe(res => {
	      var body = res.json();
	      localStorage.setItem("token",body.access_token);

	      var param = {
          email_to : "hello@water3.com.au",
          email_from : this.data.email,
          subject : "New Enquiry for water3 support!",
          template_file : "enquiry",
          params : {
            enquirytype : this.data.type,
            customer_name : this.data.first_name+" "+this.data.last_name,
            phone : this.data.phone,
            email : this.data.email,
            message : this.data.message
          }
        };

        this.api.sendEmail(param).subscribe(res =>{
          console.log(res.json());
        });

        let alert = this.alertCtrl.create({
          title : "Success",
          subTitle: "We have received your enquiry. Thank you for contacting water3",
          buttons: ['OK']
        });
        alert.present(prompt);
        this.navCtrl.setRoot('HelloIonicPage');
        loading.dismiss();

    	});

  	} else {

  		var param = {
          email_to : "hello@water3.com.au",
          email_from : this.data.email,
          subject : "New Enquiry for water3 support!",
          template_file : "enquiry",
          params : {
            enquirytype : this.data.type,
            customer_name : this.data.first_name+" "+this.data.last_name,
            phone : this.data.phone,
            email : this.data.email,
            message : this.data.message
          }
        };

        this.api.sendEmail(param).subscribe(res =>{
          console.log(res.json());
        });

        let alert = this.alertCtrl.create({
          title : "Success",
          subTitle: "We have received your enquiry. Thank you for contacting water3",
          buttons: ['OK']
        });
        alert.present(prompt);
        this.navCtrl.setRoot('HelloIonicPage');
        loading.dismiss();

  	}

  	
  }


  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

}
