import { Component } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams ,AlertController, MenuController} from 'ionic-angular';
import { Api } from '../../services/api';

/**

/**
 * Generated class for the AccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'changepassword.html',
})
export class ChangePasswordPage {

  isLoaded = false;
  accountInfo;
  data = { current_password:'', new_password:'', confirm_password:''}

  constructor(public navCtrl: NavController,public alertCtrl:AlertController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
  	if(this.api.checkSession()) { 
      this.isLoaded = true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }
  
  postAddress() {
    let loading = this.loadingCtrl.create({content : "Please wait..."});
    loading.present();
    this.api.changeUserPassword(this.data).subscribe(res => {
      let alert = this.alertCtrl.create({
              title : "Success",
              subTitle: "New Password has been updated.",
              buttons: ['OK']
            });
        alert.present(prompt);
        loading.dismiss();
        this.navCtrl.setRoot('AccountPage');
    });
  }

  back() {
    this.navCtrl.setRoot('AccountPage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
