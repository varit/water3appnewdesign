import { Component, ViewChild } from '@angular/core';
import { IonicPage, LoadingController , NavController, Events, NavParams ,AlertController, Slides, MenuController} from 'ionic-angular';
import { Api } from '../../services/api';
import { Helper } from  '../../services/helper';

import { Clipboard } from '@ionic-native/clipboard';


/**
 * Generated class for the AccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
   @ViewChild(Slides) slides: Slides;
  isLoaded = false;
  referCode;
  referCount;
  accountInfo;
  type;
  state;
  country;
  balance;
  birth = { day:0 , month:0  , year:0 };
  alertbox;
  mode;
  itemAmount;
  transactions;

  newBottle = {bottle_name:'', bottle_number:''};

  bottles: Array<{bottle_name:any , bottle_no:any}> = [];
  bottle;
  index;

  constructor(public navCtrl: NavController,public helper: Helper,public event:Events, public clipboard: Clipboard, public alertCtrl:AlertController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
  	if(this.api.checkSession()) {
      this.mode = "view";
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();

      this.itemAmount = localStorage.getItem("itemAmount");
    if(this.itemAmount == null || this.itemAmount <= 0)
      this.itemAmount = 0;
    else
      this.itemAmount = parseInt(localStorage.getItem("itemAmount"));

      this.api.getBottleList().subscribe(res => {
        //var status = res.status;
        var body = res.json();
        //console.log("Response : "+status+" BODY "+JSON.stringify(body));
        for(let bottle of body.data){
          let newbottle = {
            bottle_name : bottle.bottle_name,
            bottle_no : bottle.bottle_no
          };
          this.bottles.push(newbottle);
        }
        }, (err) => {
        
        });

      this.api.getAccountInfo().subscribe(res => {
        //var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.accountInfo = body;
        this.balance = body.regular_credit_balance + body.subscription_credit_balance;
        this.balance = this.balance.toFixed(2);

        if(localStorage.getItem("facebook")=="true") {
          this.type = "facebook";
        }

        if(body.gender == null || body.birth_date == null) {

          this.alertbox = "It seems like your profile information is missing. Complete your profile for free credits and more offers!";
        }

        if(body.birth_date != null) {
          let bd = new Date(body.birth_date.iso);
          this.birth.day = bd.getUTCDate();
          this.birth.month = bd.getUTCMonth()+1;
          this.birth.year = bd.getUTCFullYear();
        }

        this.api.getReferCode().subscribe(res => {
          var code = res.json();
          this.referCode = code.code;
          this.referCount = code.usedCount;
          console.log("refercode",this.referCode);
        });

        this.api.getCountry().subscribe(res => {
          //var status = res.status;
          var body = res.json();
          //console.log("Response : "+status+" BODY "+JSON.stringify(body));
          this.country = body.countries;

          

          this.api.getState(this.accountInfo.country_id).subscribe(res => {
            //var status = res.status;
            var body = res.json();
            //console.log("Response : "+status+" BODY "+JSON.stringify(body));
            this.state = body.states;
            if(this.state == null) {
              this.state = {"":""};
            }

            var currentDate = new Date();
            var endDate = this.helper.dateQuery(currentDate);
            var startDate = this.helper.dateQuery(currentDate,0,1);

            this.api.getTransactions(startDate,endDate).subscribe(res => {
              var status = res.status;
              var body = res.json();
              console.log("Response : "+status+" BODY "+JSON.stringify(body));
              this.transactions = body.transactions;
              loading.dismiss();
            this.isLoaded=true;
            }, (err) => {
              
            });

            
          }, (err) => {
          });      

        }, (err) => {
        });

      }, (err) => {
        let alert = this.alertCtrl.create({
              title : "Error",
              subTitle: "Some error occured or the server took too long to respond.",
              buttons: ['OK']
            });
        alert.present(prompt);
        loading.dismiss()
        this.navCtrl.setRoot('HelloIonicPage');
      });   
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }

  changeMonth() {
    this.birth.day = 1;
  }

  copyCode() {
    console.log("Copy Code");
    this.clipboard.copy(this.referCode);
  }

  copyURL() {
    console.log("Copy URL");
    this.clipboard.copy("https://www.water3.com.au/#/register/"+this.referCode);
    let alert = this.alertCtrl.create({
        title : "Copied to clipboard",
        buttons: ['OK']
        });
      alert.present(prompt);
  }

  popReferral() {
    let alert = this.alertCtrl.create({
        title : "Referral Program",
        subTitle: "Invite your friends to join our water revolution and receive special rewards!",
        buttons: ['OK']
        });
      alert.present(prompt);
  }

  performAddBottle() {
    this.api.addBottle(this.newBottle).subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));

        var data = {
          email_to : localStorage.getItem("username"),
          email_from : "noreply@water3.com.au",
          subject : "You have added a new bottle!",
          template_file : "new_bottle",
          params : { 
            custoner_name : localStorage.getItem("fullname"),
            designation : this.newBottle.bottle_name,
            bottle_no : this.newBottle.bottle_number,
            balance : this.balance
          }
        };

        this.api.sendEmail(data).subscribe(res =>{
          console.log(res.json());
        });

        this.event.publish("refreshPage","1");

        let alert = this.alertCtrl.create({
          title : "Success",
          subTitle: "Add New Bottle Successful",
          buttons: ['OK']
        });
        alert.present(prompt);

      }, (err) => {
        if(err.status == 404) {
          console.log(err);
          let alert = this.alertCtrl.create({
            title : "Error ",
            subTitle: "Your bottle number is invalid",
            buttons: ['OK']
          });
          alert.present(prompt);
        }
        if(err.status == 409) {
          let alert = this.alertCtrl.create({
            title : "Error ",
            subTitle: "This bottle has already been registered.",
            buttons: ['OK']
          });
          alert.present(prompt);
        }
      });
  }

  switchCurrency() {
    var cur;
    var exTo;
    this.api.getCurrency().subscribe(res => {
      var body = res.json();
      cur = body.currency;
      if(cur=="AUD")
        exTo = "USD";
      else if(cur=="USD")
        exTo = "AUD";

      this.api.exchangeCurrency("test",exTo).subscribe(res =>{
        var body = res.json();
        let alert = this.alertCtrl.create({
            title : "Notice ",
            message: "Exchanging "+cur+" to "+exTo+" at the rate of "+body.rate,
            buttons: [
            {
              text : "Cancel",
              role : "cancel",
              handler: () => {}
            },
            {
              text : "Confirm",
              handler: () => {
                this.api.exchangeCurrency("real",exTo).subscribe(res =>{
                  let alert = this.alertCtrl.create({
                    title : "Success",
                    subTitle: "You have now switch to use "+exTo,
                    buttons: ['OK']
                  });
                  alert.present(prompt);
                  this.api.getCurrency().subscribe(res => {
                    this.event.publish('refreshPage',"true");
                  });
                });
              }
            }
            ]
          });
          alert.present();
      });

    });
  }
  
  postAddress() {

    this.accountInfo.birth_date = this.birth.day+"/"+this.birth.month+"/"+this.birth.year;

    if(this.birth.month == 2 && this.birth.year%4 == 0 && this.birth.day>29) {
      let alert = this.alertCtrl.create({
        title : "Birth date invalid",
        subTitle: "Invalid 'day'",
        buttons: ['OK']
        });
      alert.present(prompt);
    } else if(this.birth.month==2 && this.birth.day>28) {
      let alert = this.alertCtrl.create({
        title : "Birth date invalid",
        subTitle: "Invalid 'day'",
        buttons: ['OK']
        });
      alert.present(prompt);
    } else if((this.birth.month == 2 || this.birth.month == 4 || this.birth.month == 6 || this.birth.month == 9 || this.birth.month == 11) && this.birth.day>30) {
      let alert = this.alertCtrl.create({
        title : "Birth date invalid",
        subTitle: "Invalid 'day'",
        buttons: ['OK']
        });
      alert.present(prompt);
    } else if(this.accountInfo.address_1 == null || this.accountInfo.address_1 == "" || this.accountInfo.postcode == null || this.accountInfo.postcode == "" || this.accountInfo.state_id == null || this.accountInfo.country_id == null) {
      let alert = this.alertCtrl.create({
        title : "Address invalid",
        subTitle: "Please complete the address section",
        buttons: ['OK']
        });
      alert.present(prompt);
    } else if(this.accountInfo.gender == null || this.accountInfo.gender == "") {
      let alert = this.alertCtrl.create({
        title : "Gender invalid",
        subTitle: "Please select a gender",
        buttons: ['OK']
        });
      alert.present(prompt);
    } else if(this.birth.day == 0 || this.birth.day == null || this.birth.month == 0 || this.birth.month == null || this.birth.year == 0 || this.birth.year == null) {
      let alert = this.alertCtrl.create({
        title : "Birthdate invalid",
        subTitle: "Please make sure you have input a birthdate and try again.",
        buttons: ['OK']
        });
      alert.present(prompt);
    } else {

        console.log(this.accountInfo);

        this.api.updateUserAddress(this.accountInfo).subscribe(res => {
            var status = res.status;
            var body = res.json();
            console.log("Response : "+status+" BODY "+JSON.stringify(body));
            let alert = this.alertCtrl.create({
              title : "Success",
              subTitle: "Update User data successful",
              buttons: ['OK']
              });
            alert.present(prompt);
          }, (err) => {
            var message;
            if(err.status == 404)
              message = "Required fields do not met";
            else if(err.status == 500)
              message = "Internal server error. Please try again later or contact our support.";
            else
              message = "Technial error, please contact our support.";
            let alert = this.alertCtrl.create({
                  title : "Error "+err.status,
                  subTitle: message,
                  buttons: ['OK']
                });
            alert.present(prompt);
          });
    }
    this.mode="view";
  }

  updateState() {

    this.api.getState(this.accountInfo.country_id).subscribe(res => {
            //var status = res.status;
            var body = res.json();

            var found;
            //console.log("Response : "+status+" BODY "+JSON.stringify(body));
            this.state = body.states;
            if(this.state != null && this.state != "") {

              //this.accountInfo.state_id = this.state[0].id;

              //console.log("check state s="+s.id+" Saved ID="+this.accountInfo.state_id);
              for( let s of this.state ) {
                console.log("check state s="+s.id+" Saved ID="+this.accountInfo.state_id);
                if(s.id == this.accountInfo.state_id) {
                  console.log("break at id "+s.id+" "+this.accountInfo.state_id);
                  found = s.id;
                  break;
                }
              }
              if(found!="" && found != null) {
                this.accountInfo.state_id = found;
              }

            } else { 
              this.accountInfo.state_id = 0;
              console.log("Reset State");
            }
          }, (err) => {
          });

  }

  stateName(state_id) {
    if(state_id!=null || state_id!="") {
      for(let s of this.state) {
        if(s.id == this.accountInfo.state_id) {
        return s.name;
        }
      }
    }
    return "";
  }

  countryName(country_id) {
    if(country_id!=null || country_id!="") {
      for(let c of this.country) {
        if(c.id == this.accountInfo.country_id) {
          return c.name;
        }
      }
    }
    return "";
  }

  performDelete() {


    let alert = this.alertCtrl.create({
            title : "Removing bottle ",
            message: "Are you sure to remove this bottle from your account? You can add it back at anytime",
            buttons: [
            {
              text : "Cancel",
              role : "cancel",
              handler: () => {}
            },
            {
              text : "Confirm",
              handler: () => {
                

                let loading = this.loadingCtrl.create({content : "Please wait..."});
                loading.present();
                  this.api.getBottle(this.index).subscribe(res => {
                  var status = res.status;
                  var body = res.json();
                  console.log("Response : "+status+" BODY "+JSON.stringify(body));
                  this.bottle = body;

                  this.api.deleteBottle(this.bottle).subscribe(res => {
                    var status = res.status;
                    var body = res.json();
                    console.log("Response : "+status+" BODY "+JSON.stringify(body));

                    let alert = this.alertCtrl.create({
                      title : "Success",
                      subTitle: "The bottle has been removed",
                      buttons: ['OK']
                    });
                    alert.present(prompt);
                    this.event.publish("refreshPage","1");
                  }, (err) => {
                    
                  });
                  loading.dismiss();
                  }, (err) => {
                  
                  });



              }
            }
            ]
          });
          alert.present();

    
  }

  back() {
    this.navCtrl.setRoot('AmountPage');
  }

  toChange() {
    this.navCtrl.setRoot('ChangePasswordPage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  getIndex() {
    let activeIndex = this.slides.getActiveIndex();
    if(activeIndex>=this.bottles.length)
      activeIndex -= 1;
    console.log("Bottle id = "+activeIndex,this.bottles[activeIndex]["bottle_no"]);
    this.index = this.bottles[activeIndex]['bottle_no'];
  }

  slidePrev() {
    this.slides.slidePrev();
  }

  slideNext() {
    this.slides.slideNext();

  }

  logout() {
    this.event.publish('logout',"1");
  }

}
