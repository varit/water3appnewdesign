import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WhyPage } from './why';

@NgModule({
  declarations: [
    WhyPage,
  ],
  imports: [
    IonicPageModule.forChild(WhyPage),
  ],
  exports: [
    WhyPage
  ]
})
export class WhyPageModule {}
