import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController, AlertController ,LoadingController} from 'ionic-angular';
import { Api } from '../../services/api';

import { Events } from 'ionic-angular';
/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'request.html',
})
export class RequestPage {

  email: string;

  constructor(public navCtrl: NavController,public alertCtrl:AlertController,public loadingCtrl:LoadingController, public navParams: NavParams, public menuCtrl:MenuController, public api:Api, public event:Events) { 
    
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle(); 
  }

  postRequest() {
    let loading = this.loadingCtrl.create({content : "Please wait..."});
    loading.present();

    this.api.AdminLogin().subscribe(res => {
      var body = res.json();
      localStorage.setItem("token",body.access_token);
      this.api.requestResetPassword(this.email).subscribe(res => {

        var body = res.json();

        var data = {
          email_to : this.email,
          email_from : "noreply@water3.com.au",
          subject : "Reset Password for your water3 account",
          template_file : "reset_password",
          params : { token : body.token }
        };

        this.api.sendEmail(data).subscribe(res =>{
          console.log(res.json());
        }); 

        let alert = this.alertCtrl.create({
          title : "Success",
          subTitle: "An email with reset password instructions has been sent",
          buttons: ['OK']
        });
        alert.present(prompt);
        loading.dismiss();
        this.navCtrl.setRoot('HelloIonicPage');
      }, (err) => {
        let alert = this.alertCtrl.create({
          title : "Error",
          subTitle: "Incorrect email or the email does not exist",
          buttons: ['OK']
        });
        alert.present(prompt);
        loading.dismiss();
      });
    });
  }


    toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }


}
