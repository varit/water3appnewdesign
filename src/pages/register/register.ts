import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Platform, MenuController, AlertController ,LoadingController} from 'ionic-angular';
import { Api } from '../../services/api';

import { Events } from 'ionic-angular';
/**

/**
 * Generated class for the AccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage({
  segment : 'register/:referral'
 })
@Component({
  selector: 'page-account',
  templateUrl: 'register.html',
})
export class RegisterPage {

  isLoaded = false;
  referCode;
  checked = false;
  accountInfo = {
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    password: '',
    currency: '',
    sex: '',
    birth_date: ''
  };
  birth = { day:0, month:0, year:0};
  confirm_pass;
  itemAmount;

  constructor(public navCtrl: NavController,public platform:Platform,public event:Events,public alertCtrl:AlertController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    this.referCode = this.navParams.get('referral');
    if(this.referCode=='standard' || this.referCode==":referral") {
      this.referCode=null;
    } 
    else if (this.referCode != null && this.referCode != 'standard') {
      this.performCheck(true);
    }

     this.itemAmount = localStorage.getItem("itemAmount");
    if(this.itemAmount == null || this.itemAmount <= 0)
      this.itemAmount = 0;
    else
      this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Register');
  }
  
  performRegister() {
    if(this.validate()) {

      this.accountInfo.birth_date = this.birth.day+"/"+this.birth.month+"/"+this.birth.year;
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      this.api.AdminLogin().subscribe(res => {
        var body = res.json();
        localStorage.setItem("token",body.access_token);

        this.api.register(this.accountInfo).subscribe(res => {
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        localStorage.setItem('email',this.accountInfo.email);
        localStorage.setItem('password',this.accountInfo.password);

        var data = {
          email_to : this.accountInfo.email,
          email_from : "noreply@water3.com.au",
          subject : "Welcome to the water revolution!",
          template_file : "new_account",
          params : { custoner_name : this.accountInfo.first_name+" "+this.accountInfo.last_name }
        };

        this.api.useReferCode(this.referCode).subscribe(res => {
          console.log("referral updated");
        });

        this.api.sendEmail(data).subscribe(res => {
          console.log(res.json());
        });

        this.api.authenticate().subscribe(res => {
          var status = res.status;
          var body = res.json();
          console.log("Response : "+status+" BODY "+JSON.stringify(body));
          localStorage.setItem("token",body.access_token);
          localStorage.setItem("username",body.username);
          localStorage.setItem("isLoggedin","1");
          var expiry = new Date();
          localStorage.setItem("expiry",new Date(expiry.getTime() + (this.api.expiry*1000*60)).toString());
          this.event.publish('tokenUpdated',body.access_token);
          this.event.publish('isLoggedIn',"true");
          console.log("Token = "+localStorage.getItem('token'));
          console.log("Registered = "+expiry.toString());
          console.log("Expire = "+localStorage.getItem('expiry'));
          this.navCtrl.setRoot('AccountPage');
        }, (err) => {
          let alert = this.alertCtrl.create({
          title : "Error "+err.status,
          subTitle: "Error logging in, please try again",
          buttons: ['OK']
          });
          alert.present(prompt);
          this.navCtrl.setRoot('HelloIonicPage');
        });
        loading.dismiss();
        }, (err) => {
          var message;
          if(err.statis == 409) {
            message = "This email address is already registered."
          } else {
            message = "An error was occured trying to register you account, please try again later.";
          }
          let alert = this.alertCtrl.create({
          title : "Error "+err.status,
          subTitle: message,
          buttons: ['OK']
          });
          alert.present(prompt);
          loading.dismiss();
        });
      });

    }
  }

  back() {
  }

  performCheck(url=false) {

    if(this.referCode != null) {

      this.api.AdminLogin().subscribe(res => {

        var body = res.json();

        var body = res.json();
        localStorage.setItem("token",body.access_token);

        this.api.checkReferCode(this.referCode).subscribe(res => {
          var body = res.json();
          this.checked = true;
          if(!url) {
          let alert = this.alertCtrl.create({
              title : "Success",
              subTitle: "Referral code is accepted",
              buttons: ['OK']
              });
          alert.present(prompt);
          }
        }, (err) => {
          var body = err.json();
          let alert = this.alertCtrl.create({
              title : "Error "+err.status,
              subTitle: body.message,
              buttons: ['OK']
              });
          alert.present(prompt);
        });
        localStorage.clear();

      });
    }
  }

  validate() {

    if(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(this.accountInfo.email))  
    {  

    }
    else {
      console.log("Email invalid");
      let alert = this.alertCtrl.create({
      title : "Validation failed",
      subTitle: "Email address is invalid.",
      buttons: ['OK']
      });
      alert.present(prompt);
      return false;
    }

    if(this.accountInfo.password != this.confirm_pass){
      let alert = this.alertCtrl.create({
      title : "Validation failed",
      subTitle: "Your passwords do not match",
      buttons: ['OK']
      });
      alert.present(prompt);
      return false;
    }
    if(this.accountInfo.first_name == "" || this.accountInfo.last_name == "" || this.accountInfo.phone == "" || this.accountInfo.password == "" ) {
      let alert = this.alertCtrl.create({
      title : "Validation failed",
      subTitle: "Please make sure you filled every field and try again.",
      buttons: ['OK']
      });
      alert.present(prompt);
      return false;
    }

    var year = new Date();

    if(!this.platform.is('ios')) {

      console.log("Not from iOS");

      if(this.birth.year<1900 || this.birth.year>year.getUTCFullYear()) {
        let alert = this.alertCtrl.create({
          title : "Birth date invalid",
          subTitle: "Invalid 'year'",
          buttons: ['OK']
          });
        alert.present(prompt);
        return false;
      } else if(this.birth.month<1 || this.birth.month>12) {
        let alert = this.alertCtrl.create({
          title : "Birth date invalid",
          subTitle: "Invalid 'month'",
          buttons: ['OK']
          });
        alert.present(prompt);
        return false;
      } else if(this.birth.day<1 || this.birth.day>31) {
        let alert = this.alertCtrl.create({
          title : "Birth date invalid",
          subTitle: "Invalid 'day'",
          buttons: ['OK']
          });
        alert.present(prompt);
        return false;
      } else if(this.birth.month ==2 && this.birth.year%4==0 && this.birth.day>29) {
        let alert = this.alertCtrl.create({
          title : "Birth date invalid",
          subTitle: "Invalid 'day'",
          buttons: ['OK']
          });
        alert.present(prompt);
      } else if(this.birth.month ==2 && this.birth.year%4!=0 && this.birth.day>28) {
        let alert = this.alertCtrl.create({
          title : "Birth date invalid",
          subTitle: "Invalid 'day'",
          buttons: ['OK']
          });
        alert.present(prompt);
      }
    } else if( this.platform.is('ios')) {

      console.log("From iOS");
      if(this.birth.day != 0 || this.birth.month != 0 || this.birth.year != 0) {
        if(this.birth.year<1900 || this.birth.year>year.getUTCFullYear()) {
        let alert = this.alertCtrl.create({
          title : "Birth date invalid",
          subTitle: "Invalid 'year'",
          buttons: ['OK']
          });
        alert.present(prompt);
        return false;
        } else if(this.birth.month<1 || this.birth.month>12) {
          let alert = this.alertCtrl.create({
            title : "Birth date invalid",
            subTitle: "Invalid 'month'",
            buttons: ['OK']
            });
          alert.present(prompt);
          return false;
        } else if(this.birth.day<1 || this.birth.day>31) {
          let alert = this.alertCtrl.create({
            title : "Birth date invalid",
            subTitle: "Invalid 'day'",
            buttons: ['OK']
            });
          alert.present(prompt);
          return false;
        }
      }
    }

    return true;
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
