import { Component } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , MenuController, AlertController} from 'ionic-angular';
import { Api } from '../../services/api';

 /* Generated class for the PaymentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage({
  segment : 'payments'
})
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentsPage {

	cards;
	isLoaded = false;
  mode = "view";
  card_id;
  backPage;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public alertCtrl: AlertController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    this.refresh();
    this.backPage = this.navParams.get('back');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

  back() {
    this.navCtrl.setRoot(this.backPage);
  }

  refresh() {
    if(this.api.checkSession()) {
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      this.api.getUserCreditCards().subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.cards = body.cards;
        this.mode = "view";
        this.isLoaded = true;
        loading.dismiss();

      }, (err) => {
        let alert = this.alertCtrl.create({
          title : "Error : "+err.status,
          subTitle: "An error was occured",
          buttons: ['OK']
        });
        alert.present(prompt);
        loading.dismiss();
        this.back();
      });
    }
  }

  switchMode() {

    if(this.card_id!=null && this.card_id!='' && this.mode=='view')
      this.mode="delete";
    else if(this.card_id!=null && this.card_id!='' && this.mode=='delete')
      this.mode='view';
  }

  performDelete() {
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      this.api.deleteCard(this.card_id).subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        let alert = this.alertCtrl.create({
          title : "Success",
          subTitle: "Card removal successful",
          buttons: ['OK']
        });
        alert.present(prompt);
        loading.dismiss();
        this.refresh();

      }, (err) => {
        let alert = this.alertCtrl.create({
          title : "Error : "+err.status,
          subTitle: "An error was occured",
          buttons: ['OK']
        });
        alert.present(prompt);
        loading.dismiss();
      });
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
