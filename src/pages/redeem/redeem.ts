import { Component, ViewChild } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , AlertController, MenuController , Slides} from 'ionic-angular';
import { Api } from '../../services/api';
/**
 * Generated class for the BottlePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-bottle',
  templateUrl: 'redeem.html',
})
export class RedeemPage {
 @ViewChild(Slides) slides: Slides;
  isLoaded = false;
  code;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public alertCtrl:AlertController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {

    if(this.api.checkSession()) {
      this.isLoaded=true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Bottle Create Page');
  }

  performRedeem() {
    this.api.redeemCode(this.code).subscribe( res => {
      let alert = this.alertCtrl.create({
          title : "Success",
          subTitle: "Coupon is successfully redeeemed, you can view your credit in transaction page",
          buttons: ['OK']
        });
        alert.present(prompt);
    }, (err) => {
      let alert = this.alertCtrl.create({
          title : "Error",
          subTitle: err.json().message,
          buttons: ['OK']
        });
        alert.present(prompt);
    });
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  back() {
    this.navCtrl.setRoot('BottlePage');
  }

  toBottle() {
    this.navCtrl.setRoot('TopupPage');
  }


}
