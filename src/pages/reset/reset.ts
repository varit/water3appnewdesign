import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController, AlertController ,LoadingController} from 'ionic-angular';
import { Api } from '../../services/api';

import { Events } from 'ionic-angular';
/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
 segment: 'reset/:token'
})
@Component({
  selector: 'page-login',
  templateUrl: 'reset.html',
})
export class ResetPage {

  token: string;
  email: "";
  password;
  confirm_pass;

  constructor(public navCtrl: NavController,public loadingCtrl:LoadingController, public alertCtrl:AlertController, public navParams: NavParams, public menuCtrl:MenuController, public api:Api, public event:Events) {
    this.token=localStorage.getItem("token");
    let loading = this.loadingCtrl.create({content : "Please wait..."});
    loading.present();
    this.token = this.navParams.get('token');
    this.api.AdminLogin().subscribe(res => {

      var body = res.json();
      localStorage.setItem("token",body.access_token);

      this.api.verifyResetToken(this.token).subscribe(res => {
        var body = res.json();
        this.email = body.email;
        loading.dismiss();
      }, (err) => {
        loading.dismiss();
        this.navCtrl.setRoot('HelloIonicPage');
      });

    });

    
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle(); 
  }

  postReset() {

    if(this.password != this.confirm_pass) {
      let alert = this.alertCtrl.create({
      title : "Error",
      subTitle: "Your password does not match",
      buttons: ['OK']
      });
      alert.present(prompt);
    } else {
      this.api.resetPassword(this.token,this.password).subscribe(res => {
        let alert = this.alertCtrl.create({
          title : "Success",
          subTitle: "You have succussfully reset your password.",
          buttons: ['OK']
          });
        alert.present(prompt);
        localStorage.setItem("token","");
        localStorage.setItem("isLoggedin","0");
        this.navCtrl.setRoot('LoginPage');
      });
    }

  }

    toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }


}
