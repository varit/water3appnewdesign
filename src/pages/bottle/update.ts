import { Component, ViewChild } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , AlertController, MenuController , Slides} from 'ionic-angular';
import { Api } from '../../services/api';
/**
 * Generated class for the BottlePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-bottle',
  templateUrl: 'update.html',
})
export class BottleUpdatePage {
 @ViewChild(Slides) slides: Slides;
  isLoaded = false;
  balance;
  bottle;
  action = "edit";

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public alertCtrl:AlertController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    

    if(this.api.checkSession()) {
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      this.api.getUserBalance().subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.balance = body.regular_credit_balance + body.subscription_credit_balance;
        this.balance = this.balance.toFixed(2);

      }, (err) => {
        
      });
      this.api.getBottle(localStorage.getItem("bottle_no")).subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.bottle = body;
        this.isLoaded = true;
        loading.dismiss();
        }, (err) => {
        
        });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopupPage');
  }

  toDelete() {
    this.action = "delete";
  }

  toEdit() {
    this.action="edit";
  }

  performUpdate() {
    this.api.updateBottle(this.bottle).subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));

        let alert = this.alertCtrl.create({
          title : "Success",
          subTitle: "New bottle name has been saved",
          buttons: ['OK']
        });
        alert.present(prompt);

      }, (err) => {
        
      });
  }

  performDelete() {
    this.api.deleteBottle(this.bottle).subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));

        let alert = this.alertCtrl.create({
          title : "Success",
          subTitle: "The bottle has been removed",
          buttons: ['OK']
        });
        alert.present(prompt);
        this.navCtrl.setRoot('BottlePage');
      }, (err) => {
        
      });
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  back() {
    this.navCtrl.setRoot('BottlePage');
  }

  toBottle() {
    this.navCtrl.setRoot('TopupPage');
  }


}
