import { Component, ViewChild } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , MenuController , Slides} from 'ionic-angular';
import { Api } from '../../services/api';
/**
 * Generated class for the BottlePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-bottle',
  templateUrl: 'bottle.html',
})
export class BottlePage {
 @ViewChild(Slides) slides: Slides;
  isLoaded = false;
  balance;
  bottles: Array<{bottle_name:any , bottle_no:any}> = [];
  index;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    

    if(this.api.checkSession()) {
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      this.api.getUserBalance().subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.balance = body.regular_credit_balance + body.subscription_credit_balance;
        this.balance = this.balance.toFixed(2);

      }, (err) => {
        
      });
      this.api.getBottleList().subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        for(let bottle of body.data){
          let newbottle = {
            bottle_name : bottle.bottle_name,
            bottle_no : bottle.bottle_no
          };
          this.bottles.push(newbottle);
        }
        
        this.isLoaded = true;
        loading.dismiss();
        }, (err) => {
        
        });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopupPage');
  }


  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {
    this.navCtrl.setRoot('TopupPage');
  }

  getIndex() {
    let activeIndex = this.slides.getActiveIndex();
    if(activeIndex>=this.bottles.length)
      activeIndex -= 1;
    console.log("Bottle id = "+activeIndex,this.bottles[activeIndex]["bottle_no"]);
    this.index = this.bottles[activeIndex]['bottle_no'];
  }

  toUpdate() {
    this.getIndex();
    localStorage.setItem('bottle_no',this.index);
    this.navCtrl.setRoot('BottleUpdatePage');
  }

  toNewBottle() {
    this.navCtrl.setRoot('BottleCreatePage');
  }

  slidePrev() {
    this.slides.slidePrev();
  }

  slideNext() {
    this.slides.slideNext();

  }

  slidePosition() {
    if(this.slides.isBeginning()){
      return 1;
    }
    if(this.slides.isEnd()) {
      return 99;
    }
    return 0;
  }


}
