import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BottleCreatePage } from './create';

@NgModule({
  declarations: [
    BottleCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(BottleCreatePage),
  ],
  exports: [
    BottleCreatePage
  ]
})
export class BottleCreatePageModule {}
