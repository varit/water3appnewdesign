import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BottleUpdatePage } from './update';

@NgModule({
  declarations: [
    BottleUpdatePage,
  ],
  imports: [
    IonicPageModule.forChild(BottleUpdatePage),
  ],
  exports: [
    BottleUpdatePage
  ]
})
export class BottleUpdatePageModule {}
