import { Component, ViewChild } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , AlertController, MenuController , Slides} from 'ionic-angular';
import { Api } from '../../services/api';
/**
 * Generated class for the BottlePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-bottle',
  templateUrl: 'create.html',
})
export class BottleCreatePage {
 @ViewChild(Slides) slides: Slides;
  isLoaded = false;
  balance;
  bottle = {bottle_name:'', bottle_number:''};
  action = "edit";

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public alertCtrl:AlertController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {

    if(this.api.checkSession()) {
      this.api.getAccountInfo().subscribe(res => {
        var body = res.json();
        this.balance = body.regular_credit_balance + body.subscription_credit_balance;
      });

      if(navParams.get('bottle_no') != null) {
        this.bottle.bottle_number = navParams.get('bottle_no');
      }

      this.isLoaded = true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Bottle Create Page');
  }

  toDelete() {
    this.action = "delete";
  }

  toEdit() {
    this.action="edit";
  }

  performAddBottle() {
    this.api.addBottle(this.bottle).subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));

        var data = {
          email_to : localStorage.getItem("username"),
          email_from : "noreply@water3.com.au",
          subject : "You have added a new bottle!",
          template_file : "new_bottle",
          params : { 
            custoner_name : localStorage.getItem("fullname"),
            designation : this.bottle.bottle_name,
            bottle_no : this.bottle.bottle_number,
            balance : this.balance
          }
        };

        this.api.sendEmail(data).subscribe(res =>{
          console.log(res.json());
        });

        let alert = this.alertCtrl.create({
          title : "Success",
          subTitle: "Add New Bottle Successful",
          buttons: ['OK']
        });
        alert.present(prompt);
        this.navCtrl.setRoot('TopupPage');

      }, (err) => {
        if(err.status == 404) {
          let alert = this.alertCtrl.create({
            title : "Error ",
            subTitle: "Your bottle number is invalid",
            buttons: ['OK']
          });
          alert.present(prompt);
        }
        if(err.status == 409) {
          let alert = this.alertCtrl.create({
            title : "Error ",
            subTitle: "This bottle has already been registered.",
            buttons: ['OK']
          });
          alert.present(prompt);
        }
      });
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  back() {
    this.navCtrl.setRoot('BottlePage');
  }

  toBottle() {
    this.navCtrl.setRoot('TopupPage');
  }


}
