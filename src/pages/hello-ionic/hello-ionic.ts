import { Component , ViewChild} from '@angular/core';
import { NavController, IonicPage , Content, Platform, MenuController} from 'ionic-angular';
import { Api } from '../../services/api';
import { ENV } from '../../services/env';

//import { HTTP } from '@ionic-native/http';

@IonicPage({
  segment : 'index'
})
@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'hello-ionic.html'
})
export class HelloIonicPage {
  @ViewChild(Content) content: Content;
  token : string;
  message  : string;
  displayLog : string;
  isLoggedin = false;
  itemAmount;
  constructor(public navCtrl: NavController,public platform:Platform,public env:ENV, public api: Api, public menuCtrl: MenuController) {

  this.message = "Waiting command";
  this.token = "0";
  this.displayLog = "0";

  this.itemAmount = localStorage.getItem("itemAmount");
    if(this.itemAmount == null || this.itemAmount <= 0)
      this.itemAmount = 0;
    else
      this.itemAmount = parseInt(localStorage.getItem("itemAmount"));

  if(localStorage.getItem("isLoggedin")=="1")
    this.isLoggedin = true;

  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

  toKiosk(){
    this.navCtrl.setRoot('KioskPage');
  }

  scrollTo() {
    this.content.scrollTo(0,2500,1500);
  }

}

