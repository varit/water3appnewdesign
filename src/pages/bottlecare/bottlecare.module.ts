import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BottlecarePage } from './bottlecare';

@NgModule({
  declarations: [
    BottlecarePage,
  ],
  imports: [
    IonicPageModule.forChild(BottlecarePage),
  ],
  exports: [
    BottlecarePage
  ]
})
export class BottlecarePageModule {}
