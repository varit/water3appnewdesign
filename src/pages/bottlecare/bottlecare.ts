import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

/**
 * Generated class for the BottlecarePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-bottlecare',
  templateUrl: 'bottlecare.html',
})
export class BottlecarePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl:MenuController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BottlecarePage');
  }

    openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

}
