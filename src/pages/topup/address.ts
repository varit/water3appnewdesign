import { Component } from '@angular/core';
import { IonicPage, LoadingController , AlertController, NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';
/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-address',
  templateUrl: 'address.html',
})
export class AddressPage {
  isLoaded = false;
  accountInfo;
  state;
  country;
  sameAccountInfo;

  isLoggedin;
  itemAmount;

  constructor(public navCtrl: NavController,public alertCtrl:AlertController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    if(this.api.checkSession()) {

      if(localStorage.getItem("isLoggedin")=="1")
        this.isLoggedin = true;

      this.itemAmount = localStorage.getItem("itemAmount");
      if(this.itemAmount == null || this.itemAmount <= 0)
        this.itemAmount = 0;
      else
        this.itemAmount = parseInt(localStorage.getItem("itemAmount"));

      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      this.api.getAccountInfo().subscribe(res => {
        var status = res.status;
        var body = res.json();
        //console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.accountInfo = body;
        
        let oldAddress = {
          address_1 : this.accountInfo.address_1,
          address_2 : this.accountInfo.address_2,
          city : this.accountInfo.city,
          state_id : this.accountInfo.state_id,
          country_id : this.accountInfo.country_id,
          postcode : this.accountInfo.postcode
        };

        this.sameAccountInfo = oldAddress;

        this.api.getCountry().subscribe(res => {
          var status = res.status;
          var body = res.json();
          //console.log("Response : "+status+" BODY "+JSON.stringify(body));
          this.country = body.countries;

          this.api.getState(this.accountInfo.country_id).subscribe(res => {
            var status = res.status;
            var body = res.json();
            //console.log("Response : "+status+" BODY "+JSON.stringify(body));
            this.state = body.states;

            loading.dismiss();
            this.isLoaded=true;
          }, (err) => {
          });            
        }, (err) => {
        });        
      }, (err) => {       
      });   
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopupPage - Address');
  }
  
  postAddress() {

    if(this.accountInfo.address_1 == "" || this.accountInfo.address_1 == null || this.accountInfo.city == "" || this.accountInfo.city == null || this.accountInfo.postcode == "" || this.accountInfo.postcode == null) {
      let alert = this.alertCtrl.create({
            title : "Error",
            subTitle: "Please check that all the fields are filled and try again.",
            buttons: ['OK']
          });
      alert.present(prompt);
    }
    else {

        this.api.updateUserAddress(this.accountInfo).subscribe(res => {
            var status = res.status;
            var body = res.json();
            //console.log("Response : "+status+" BODY "+JSON.stringify(body));
            this.navCtrl.setRoot('PaymentMethodPage');
          }, (err) => {
            let alert = this.alertCtrl.create({
              title : "Error",
              subTitle: "Please check that all the fields are filled and try again.",
              buttons: ['OK']
            });
            alert.present(prompt);
          });
    }
  }

  sameAddress(){
    this.navCtrl.setRoot('PaymentMethodPage');
  }

  updateState() {

    this.api.getState(this.accountInfo.country_id).subscribe(res => {
            //var status = res.status;
            var body = res.json();

            var found;
            //console.log("Response : "+status+" BODY "+JSON.stringify(body));
            this.state = body.states;
            if(this.state != null && this.state != "") {

              //this.accountInfo.state_id = this.state[0].id;

              //console.log("check state s="+s.id+" Saved ID="+this.accountInfo.state_id);
              for( let s of this.state ) {
                //console.log("check state s="+s.id+" Saved ID="+this.accountInfo.state_id);
                if(s.id == this.accountInfo.state_id) {
                  //console.log("break at id "+s.id+" "+this.accountInfo.state_id);
                  found = s.id;
                  break;
                }
              }
              if(found!="" && found != null) {
                this.accountInfo.state_id = found;
              }

            } else { 
              this.accountInfo.state_id = 0;
              console.log("Reset State");
            }
          }, (err) => {
          });

  }

    stateName(state_id) {
    for(let s of this.state) {
      if(s.id == this.accountInfo.state_id) {
        return s.name;
      }
    }
  }

  countryName(country_id) {
    for(let c of this.country) {
      if(c.id == this.accountInfo.country_id) {
        return c.name;
      }
    }
  }

  back() {
    this.navCtrl.setRoot('AmountPage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
