import { Component , ViewChild} from '@angular/core';
import { IonicPage, LoadingController , AlertController, Platform, NavController, NavParams , MenuController , Slides} from 'ionic-angular';
import { Api } from '../../services/api';

/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-topup',
  templateUrl: 'topup.html',
})
export class TopupPage {
  @ViewChild(Slides) slides: Slides;
  isLoaded = false;
  balance;
  bottles: Array<{bottle_name:any , bottle_no:any}> = [];
  alertbox;

  constructor(public navCtrl: NavController,public platform:Platform, public alertCtrl:AlertController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    

  	if(this.api.checkSession()) {
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      this.api.getAccountInfo().subscribe(res => {
        //var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));

        if(body.gender == null || body.birth_date == null) {

          // localStorage.setItem('alert','true');
          // localStorage.setItem('alertText','<a (click)="navCtrl.setRoot(/"AccountPage/")"');

          if(this.platform.is('ios')) {
            console.log("iOS alert");
            this.alertbox = "It seems like your profile information is missing. Complete your profile for free credits and more offers! Click here!";
          } else {     
            let alert = this.alertCtrl.create({
            title : "Please update your profle",
            subTitle: "Your profile is missing some information. Please complete your profile to proceed.",
            buttons: ['OK']
            });
            alert.present(prompt);
            this.navCtrl.setRoot('AccountPage');
          }
        }

        this.balance = body.regular_credit_balance + body.subscription_credit_balance;
        this.balance = this.balance.toFixed(2);

      }, (err) => {
        
      });
  		this.api.getBottleList().subscribe(res => {
        //var status = res.status;
        var body = res.json();
        //console.log("Response : "+status+" BODY "+JSON.stringify(body));
        for(let bottle of body.data){
        	let newbottle = {
        		bottle_name : bottle.bottle_name,
        		bottle_no : bottle.bottle_no
        	};
        	this.bottles.push(newbottle);
        }
        this.isLoaded = true;
        loading.dismiss();
      	}, (err) => {
        
      	});
  	}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopupPage');
  }


  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {
    this.navCtrl.setRoot('TopupPage');
  }

  toNewBottle() {
    this.navCtrl.setRoot('BottleCreatePage')
  }

  slidePrev() {
    this.slides.slidePrev();
  }

  slideNext() {
    this.slides.slideNext();

  }

  slidePosition() {
    if(this.slides.isBeginning()){
      return 1;
    }
    if(this.slides.isEnd()) {
      return 99;
    }
    return 0;
  }

  toAmount() {
    this.navCtrl.setRoot('AmountPage');
  }

}
