import { Component } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';

/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-topup',
  templateUrl: 'confirm.html',
})
export class ConfirmPage {
  isLoaded = false;
  balance;
  amount;
  email;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
  	if(this.api.checkSession()) {
        let loading = this.loadingCtrl.create({content : "Please wait..."});
        loading.present();
        this.api.getAccountInfo().subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.balance = body.regular_credit_balance + body.subscription_credit_balance;
        this.balance = this.balance.toFixed(2);
        this.amount = localStorage.getItem("amount");
        this.email = localStorage.getItem('email');

        var data = {
          email_to : this.email,
          email_from : "noreply@water3.com.au",
          subject : "Your Topup was successful!",
          template_file : "topup",
          params : {
            customer_name : body.first_name+" "+body.last_name,
            amount : this.amount
          }
        };

        this.api.sendEmail(data).subscribe(res =>{
          console.log(res.json());
        });


        this.isLoaded = true;
        loading.dismiss();

      }, (err) => {
        
      });
  	}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopupPage');
  }

  toFindKiosk() {
    this.navCtrl.setRoot('KioskPage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }
}
