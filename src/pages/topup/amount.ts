import { Component } from '@angular/core';
import { IonicPage, AlertController, LoadingController , NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';

/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-amount',
  templateUrl: 'amount.html',
})
export class AmountPage {
  isLoaded = false;
  amount: any;

  constructor(public navCtrl: NavController,public alertCtrl:AlertController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    this.api.checkSession();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopupPage - Amount');
  }
  
  postAmount() {
    console.log("POST amount");
    if(this.amount!=null && this.amount!="" && this.amount>=5) {
      localStorage.setItem("amount", parseFloat(this.amount).toFixed(2));
      console.log("Set Amount = "+this.amount);
      this.navCtrl.setRoot('AddressPage');
    } else {
      let alert = this.alertCtrl.create({
              title : "Amount error",
              subTitle: "Please input a valid amount (Minimum $5.00)",
              buttons: ['OK']
            });
        alert.present(prompt);
    }
  }

  setAmount(amount:number){
    this.amount = amount;
  }

  setDecimal() {
    this.amount = parseFloat(this.amount).toFixed(2);
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  back() {
    this.navCtrl.setRoot('TopupPage');
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
