import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubPaymentPage } from './sub-payment';

@NgModule({
  declarations: [
    SubPaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(SubPaymentPage),
  ],
  exports: [
    SubPaymentPage
  ]
})
export class SubPaymentPageModule {}
