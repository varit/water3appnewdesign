import { Component } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , MenuController, AlertController} from 'ionic-angular';
import { Api } from '../../services/api';

/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-payment-method',
  templateUrl: 'sub-payment-method.html',
})
export class SubPaymentMethodPage {
  isLoaded = false;
  amount;
  cards;
  card_id;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public alertCtrl: AlertController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    if(this.api.checkSession()) {
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      this.api.getUserCreditCards().subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.cards = body.cards;
        this.isLoaded = true;
        loading.dismiss();

      }, (err) => {
        let alert = this.alertCtrl.create({
          title : "Error : "+err.status,
          subTitle: "An error was occured",
          buttons: ['OK']
        });
        alert.present(prompt);
        loading.dismiss();
        this.back();
      });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubscriptionPage - PaymentMethod');
  }

  postPaymentMethod() {
    if(this.card_id!=null) {
      localStorage.setItem("card_id",this.card_id);
      this.navCtrl.setRoot('SubPaymentPage');
    }
  }

  back() {
    this.navCtrl.setRoot('SubAddressPage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
