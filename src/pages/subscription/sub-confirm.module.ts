import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubConfirmPage } from './sub-confirm';

@NgModule({
  declarations: [
    SubConfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(SubConfirmPage),
  ],
  exports: [
    SubConfirmPage
  ]
})
export class SubConfirmPageModule {}
