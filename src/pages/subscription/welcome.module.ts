import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubWelcomePage } from './welcome';

@NgModule({
  declarations: [
    SubWelcomePage,
  ],
  imports: [
    IonicPageModule.forChild(SubWelcomePage),
  ],
  exports: [
    SubWelcomePage
  ]
})
export class SubWelcomePageModule {}
