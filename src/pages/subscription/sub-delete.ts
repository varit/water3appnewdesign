import { Component } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';
import { Helper } from '../../services/helper';

/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-topup',
  templateUrl: 'sub-delete.html',
})
export class SubDeletePage {
  isLoaded = false;
  isPending = false;
  subID;
  action = "start";

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public helper:Helper, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
  	if(this.api.checkSession()) {
        let loading = this.loadingCtrl.create({content : "Please wait..."});
        loading.present();
        this.api.getActiveSubscription().subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.subID = body.subscription_id;
        if(body=="" || body==null)
        {
          this.isPending=true;
        }
        this.isLoaded=true;
        loading.dismiss();
      }, (err) => {
        
      });
  	}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Subscription - Delete');
  }

  postDelete() {
    if(this.isPending) {
      this.api.unsubscribe(this.subID,"pending").subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.action = "finish";
      }, (err) => {
        
      });
    }
    else {
      this.api.unsubscribe(this.subID,"active").subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.action = "finish";
      }, (err) => {
        
      });
    }
  }


  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

  back(){
    this.navCtrl.setRoot('SubscriptionPage');
  }
}
