import { Component } from '@angular/core';
import { IonicPage, LoadingController , AlertController , NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';

/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-address',
  templateUrl: 'sub-address.html',
})
export class SubAddressPage {
  isLoaded = false;
  accountInfo;
  state;
  country;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public alertCtrl:AlertController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    if(this.api.checkSession()) {
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      this.api.getAccountInfo().subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.accountInfo = body;

        this.api.getCountry().subscribe(res => {
          var status = res.status;
          var body = res.json();
          console.log("Response : "+status+" BODY "+JSON.stringify(body));
          this.country = body.countries;

          this.api.getState(this.accountInfo.country_id).subscribe(res => {
            var status = res.status;
            var body = res.json();
            console.log("Response : "+status+" BODY "+JSON.stringify(body));
            this.state = body.states;

            loading.dismiss();
            this.isLoaded=true;
          }, (err) => {
          });            
        }, (err) => {
        });        
      }, (err) => {       
      });   
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopupPage - Address');
  }
  
  postAddress() {

    if(this.accountInfo.address_1 == "" || this.accountInfo.address_1 == null || this.accountInfo.city == "" || this.accountInfo.city == null || this.accountInfo.postcode == "" || this.accountInfo.postcode == null) {
      let alert = this.alertCtrl.create({
            title : "Error",
            subTitle: "Please check that all the fields are filled and try again.",
            buttons: ['OK']
          });
      alert.present(prompt);
    }
    else {

        this.api.updateUserAddress(this.accountInfo).subscribe(res => {
            var status = res.status;
            var body = res.json();
            console.log("Response : "+status+" BODY "+JSON.stringify(body));
            this.navCtrl.setRoot('SubPaymentMethodPage');
          }, (err) => {
          });
    }

  }

  updateState() {

    this.api.getState(this.accountInfo.country_id).subscribe(res => {
            var status = res.status;
            var body = res.json();
            console.log("Response : "+status+" BODY "+JSON.stringify(body));
            this.state = body.states;
          }, (err) => {
          });

  }

  back() {
    this.navCtrl.setRoot('PlanPage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
