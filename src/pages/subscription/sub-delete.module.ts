import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubDeletePage } from './sub-delete';

@NgModule({
  declarations: [
    SubDeletePage,
  ],
  imports: [
    IonicPageModule.forChild(SubDeletePage),
  ],
  exports: [
    SubDeletePage
  ]
})
export class SubDeletePageModule {}
