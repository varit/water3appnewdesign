import { Component } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';
import { Helper } from '../../services/helper';

/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-topup',
  templateUrl: 'sub-confirm.html',
})
export class SubConfirmPage {
  isLoaded = false;
  balance;
  amount;
  activedate;
  email;
  plan;
  isPending = "";

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public helper:Helper, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
  	if(this.api.checkSession()) {
        let loading = this.loadingCtrl.create({content : "Please wait..."});
        loading.present();
        this.api.getAccountInfo().subscribe(res => {
        var status = res.status;
        var body = res.json();
        this.plan = localStorage.getItem('subPlan');
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.balance = body.regular_credit_balance + body.subscription_credit_balance;
        this.balance = this.balance.toFixed(2);
        this.amount = localStorage.getItem("amount");
        this.email = localStorage.getItem('email');
        this.isLoaded = true;
        this.isPending = localStorage.getItem("SubPending");
        this.activedate = localStorage.getItem("SubActive");
        loading.dismiss();

      }, (err) => {
        
      });
  	}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Subscription - Confirmation');
  }


  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

  back(){
    this.navCtrl.setRoot('SubscriptionPage');
  }
}
