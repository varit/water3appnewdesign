import { Component , ViewChild} from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , MenuController , Slides} from 'ionic-angular';
import { Api } from '../../services/api';
import { Helper } from '../../services/helper';

import { HelloIonicPage } from '../hello-ionic/hello-ionic';

@IonicPage()
@Component({
  selector: 'page-subscription',
  templateUrl: 'plan.html',
})

export class PlanPage {
  @ViewChild(Slides) slides: Slides;
  currentSlide = 0;
  isLoaded = false;
  loading;
  amount;
  credits;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public helper:Helper, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    if(this.api.checkSession()) {
      //this.loading = this.loadingCtrl.create({content : "Please wait..."});
      //this.loading.present();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Subscription - Plan');
  }

  getIndex() {
    this.currentSlide = this.slides.getActiveIndex();
    console.log("Current index is",this.currentSlide);
  }

  postPlan() {
    if(this.currentSlide == 0)
      localStorage.setItem("subPlan","15FOR$10");
    if(this.currentSlide == 1)
      localStorage.setItem("subPlan","25FOR$15");
    if(this.currentSlide == 2)
      localStorage.setItem("subPlan","35FOR$20");
    if(this.currentSlide == 3)
      localStorage.setItem("subPlan","45FOR$25");
    if(this.currentSlide == 4 || this.currentSlide == 5)
      localStorage.setItem("subPlan","55FOR$30");

    console.log("Plan ",localStorage.getItem('subPlan'));
    this.navCtrl.setRoot('SubAddressPage');
  }

  openMenu() {
    this.menuCtrl.toggle();
  }
  back() {
    this.navCtrl.setRoot(HelloIonicPage);
  }

  slidePrev() {
    this.slides.slidePrev();
  }

  slideNext() {
    this.slides.slideNext();
  }

  toBottle() {
    this.navCtrl.setRoot('TopupPage');
  }

}
