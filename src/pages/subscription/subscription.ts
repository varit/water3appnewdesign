import { Component } from '@angular/core';
import { IonicPage, LoadingController, AlertController, NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';
import { Helper } from '../../services/helper';

// import { TopupPage } from '../topup/topup';

// import { SubWelcomePage } from '../subscription/welcome';
// import { PlanPage } from '../subscription/plan';
// import { SubDeletePage } from '../subscription/delete';

@IonicPage()
@Component({
  selector: 'page-subscription',
  templateUrl: 'subscription.html',
})

export class SubscriptionPage {

  isLoaded = false;
  loading;
  amount;
  hasActive = false;
  Active;
  hasPending = false;
  Pending;
  hasLast = false;
  Last;
  credits;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController, public alertCtrl: AlertController, public helper:Helper, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    if(this.api.checkSession()) {
      this.loading = this.loadingCtrl.create({content : "Please wait..."});
      this.loading.present();
      this.api.getAccountInfo().subscribe(res => {
        //var status = res.status;
        var body = res.json();
        //console.log("Response : "+status+" BODY "+JSON.stringify(body));

        if(body.gender == null || body.birth_date == null) {
          let alert = this.alertCtrl.create({
            title : "Please update your profle",
            subTitle: "Your profile is missing some information. Please complete your profile to proceed.",
            buttons: ['OK']
            });
          alert.present(prompt);
          this.navCtrl.setRoot('AccountPage');
        }

        this.credits = body.subscription_credit_balance;
        this.credits = this.credits.toFixed(2);

      });
    	this.checkSub();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopupPage - Subscription');
  }

  checkSub() {

  	this.api.getActiveSubscription().subscribe(res => {
  		var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        if(body!=null && body!="") {
        	this.hasActive=true;
          this.Active = body;
        }
        this.api.getPendingSubscription().subscribe(res => {
	  		var status = res.status;
	        var body = res.json();
	        console.log("Response : "+status+" BODY "+JSON.stringify(body));
	        if(body!=null && body!="") {
	        	this.hasPending=true;
            this.Pending = body
	        }
	        this.api.getLastSubscription().subscribe(res => {
		  		var status = res.status;
		        var body = res.json();
		        console.log("Response : "+status+" BODY "+JSON.stringify(body));
		        if(body!=null && body!="") {
		        	this.hasLast=true;
              this.Last = body;
		        }
            this.redirect();
		  	}, (err) => {
          let alert = this.alertCtrl.create({
          title : "Error "+err.status,
          subTitle: "Cannot retrieve subscription data",
          buttons: ['OK']
          });
          alert.present(prompt);
        });
	  	}, (err) => {
        let alert = this.alertCtrl.create({
          title : "Error "+err.status,
          subTitle: "Cannot retrieve subscription data",
          buttons: ['OK']
          });
          alert.present(prompt);
      });
  	}, (err)=>{
      let alert = this.alertCtrl.create({
          title : "Error "+err.status,
          subTitle: "Cannot retrieve subscription data",
          buttons: ['OK']
          });
          alert.present(prompt);
    });

  }

  redirect() {
    if(!this.hasActive && !this.hasPending && !this.hasLast) {
      this.navCtrl.setRoot('SubWelcomePage');
      this.loading.dismiss();
    }
    else if(this.Last.creditExpireDate==null) {
      this.navCtrl.setRoot('SubWelcomePage');
      this.loading.dismiss();
    }
    else {
      this.isLoaded = true;
      this.loading.dismiss();
    } 
  }

  toPlan() {
    this.navCtrl.setRoot('PlanPage');
  }

  toDelete() {
    this.navCtrl.setRoot('SubDeletePage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
