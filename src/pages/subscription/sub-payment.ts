import { Component } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , MenuController, AlertController} from 'ionic-angular';
import { Api } from '../../services/api';


declare var Stripe: any;
/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-address',
  templateUrl: 'sub-payment.html',
})
export class SubPaymentPage {
  isLoaded = false;
  accountInfo;
  card;
  amount;
  save = false;
  stripe_pk;
  Stripe_token;
  Stripe_param = {name: '', number: '', exp_month:'', exp_year:'', cvc:''};

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public alertCtrl:AlertController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    if(this.api.checkSession()) {
      this.amount = localStorage.getItem('subPlan');
      this.amount = this.amount.substr(5);
      console.log("Amount due",this.amount);
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      if(localStorage.getItem("card_id") != null || localStorage.getItem("card_id") != "")
      this.api.getUserCreditCards().subscribe(res => {

        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        body.cards.forEach(element => {
          if(element.stripe_card_id == localStorage.getItem("card_id")) {
              this.card = element;
              console.log(this.card.stripe_card_id);
          }
        });
        this.isLoaded = true;
        loading.dismiss();
      }, (err) => {       
        let alert = this.alertCtrl.create({
          title : "Error "+err.status,
          subTitle: "Cannot retrieve card data",
          buttons: ['OK']
          });
          alert.present(prompt);
      });
    this.stripe_pk = this.api.getStripePK();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Subscription - payment');
    Stripe.setPublishableKey(this.stripe_pk);
  }

  postPayment() {
    console.log(this.Stripe_param);
    console.log(this.save);
    if(!this.card){
              if(this.Stripe_param.number.length > 16) {
                let alert = this.alertCtrl.create({
                    title : "Validation Error",
                    subTitle: "You credit card number exceeded digit limit",
                    buttons: ['OK']
                  });
                  alert.present(prompt);
              } else if(+this.Stripe_param.exp_month > 12 || +this.Stripe_param.exp_month < 1) {
                let alert = this.alertCtrl.create({
                    title : "Date Error",
                    subTitle: "Invalid expiry month",
                    buttons: ['OK']
                  });
                  alert.present(prompt);
              } else if(+this.Stripe_param.exp_year < 2016) {
                let alert = this.alertCtrl.create({
                    title : "Date Error",
                    subTitle: "Invalid expiry year",
                    buttons: ['OK']
                  });
                  alert.present(prompt);
              } else if(+this.Stripe_param.cvc > 999 || +this.Stripe_param.cvc < 100) {
                let alert = this.alertCtrl.create({
                    title : "CVC Error",
                    subTitle: "Invalid CVC",
                    buttons: ['OK']
                  });
                  alert.present(prompt);
              } else {

                  Stripe.card.createToken({
                  name : this.Stripe_param.name,
                  number : this.Stripe_param.number,
                  exp_month : this.Stripe_param.exp_month,
                  exp_year : this.Stripe_param.exp_year,
                  cvc : this.Stripe_param.cvc

                }, (status, response) => this.stripeResponseHandler(status,response));
              }
    }
    else {
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      let payment = { source:this.card.stripe_card_id , plan:localStorage.getItem("subPlan")};
      this.api.subscribe(payment).subscribe(res => {
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        if(body.status == "406") {
          localStorage.setItem("SubPending","true");
          localStorage.setItem("SubActive",body.activedate.iso);
        }
        loading.dismiss();
        this.navCtrl.setRoot('SubConfirmPage');
      }, (err) => {    
        let alert = this.alertCtrl.create({
          title : "Error "+err.status,
          subTitle: err.message,
          buttons: ['OK']
          });
          alert.present(prompt);
        loading.dismiss();
      });
    }
  }

  stripeResponseHandler(status,response) {
    if(response.error) {
      let alert = this.alertCtrl.create({
        title : "Payment Error",
        subTitle: response.error.message,
        buttons: ['OK']
        });
      alert.present(prompt);
    } else {
      var token = response.id;
      //this.Stripe_token = response.id;
      console.log(token);
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      let payment = { source:token , plan:localStorage.getItem("subPlan")};
      this.api.subscribe(payment).subscribe(res => {
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        if(body.status == "406") {
          localStorage.setItem("SubPending","true");
          localStorage.setItem("SubActive",body.activedate.iso);
        }
        loading.dismiss();
        this.navCtrl.setRoot('SubConfirmPage');
      }, (err) => {    
        console.log(err);  
        loading.dismiss();
      });
    }
  }

  back() {
    this.navCtrl.setRoot('SubPaymentMethodPage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
