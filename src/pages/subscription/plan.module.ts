import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlanPage } from './plan';

@NgModule({
  declarations: [
    PlanPage,
  ],
  imports: [
    IonicPageModule.forChild(PlanPage),
  ],
  exports: [
    PlanPage
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PlanPageModule {}
