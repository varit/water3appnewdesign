import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubAddressPage } from './sub-address';

@NgModule({
  declarations: [
    SubAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(SubAddressPage),
  ],
  exports: [
    SubAddressPage
  ]
})
export class SubAddressPageModule {}
