import { Component, ViewChild } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams, Slides , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';

@IonicPage()
@Component({
  selector: 'page-subscription',
  templateUrl: 'welcome.html',
})

export class SubWelcomePage {
@ViewChild(Slides) slides: Slides;
  currentSlide = 0;
  isLoaded = false;
  amount;
  hasActive = false;
  hasPending = false;
  hasLast = false;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    if(this.api.checkSession()) {
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Subscription - Welcome');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }


  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

  getIndex() {
    this.currentSlide = this.slides.getActiveIndex();
    console.log("Current index is",this.currentSlide);
  }

  postPlan() {
    if(this.currentSlide == 0)
      localStorage.setItem("subPlan","15FOR$10");
    if(this.currentSlide == 1)
      localStorage.setItem("subPlan","25FOR$15");
    if(this.currentSlide == 2)
      localStorage.setItem("subPlan","35FOR$20");
    if(this.currentSlide == 3)
      localStorage.setItem("subPlan","45FOR$25");
    if(this.currentSlide == 4 || this.currentSlide == 5)
      localStorage.setItem("subPlan","55FOR$30");

    console.log("Plan ",localStorage.getItem('subPlan'));
    this.navCtrl.setRoot('SubAddressPage');
  }

  
  slidePrev() {
    this.slides.slidePrev();
  }

  slideNext() {
    this.slides.slideNext();
  }

}
