import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubPaymentMethodPage } from './sub-payment-method';

@NgModule({
  declarations: [
    SubPaymentMethodPage,
  ],
  imports: [
    IonicPageModule.forChild(SubPaymentMethodPage),
  ],
  exports: [
    SubPaymentMethodPage
  ]
})
export class SubPaymentMethodModule {}
