import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubscriptionPage } from './subscription';

@NgModule({
  declarations: [
    SubscriptionPage,
  ],
  imports: [
    IonicPageModule.forChild(SubscriptionPage),
  ],
  exports: [
    SubscriptionPage
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SubscriptionPageModule {}
