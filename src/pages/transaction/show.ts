import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Helper } from  '../../services/helper';
/**
 * Generated class for the TransactionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-transaction',
  templateUrl: 'show.html',
})
export class ShowPage {

	transaction: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public helper:Helper) {
  	this.transaction = navParams.get('transaction');
  	console.log(this.transaction);
    this.transaction.balance = parseFloat(this.transaction.balance).toFixed(2);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShowPage');
  }

  pop() {
  	this.navCtrl.pop();
  }

}
