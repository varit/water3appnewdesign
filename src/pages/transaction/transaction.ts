import { Component } from '@angular/core';
import { IonicPage, AlertController ,LoadingController, NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';
import { Helper } from  '../../services/helper';

/**
 * Generated class for the TransactionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-transaction',
  templateUrl: 'transaction.html',
})
export class TransactionPage {
  isLoaded = false
  period = 1;
  transactions;

  constructor(public navCtrl: NavController,public alertCtrl:AlertController, public loadingCtrl:LoadingController, public navParams: NavParams,public helper:Helper, public api:Api, public menuCtrl:MenuController) {
    if(this.api.checkSession()) {
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      var currentDate = new Date();
      console.log(currentDate+" Month="+currentDate.getUTCMonth());
      var endDate = this.helper.dateQuery(currentDate);
      var startDate = this.helper.dateQuery(currentDate,0,this.period);
      console.log(startDate);
      console.log(endDate);
      this.api.getTransactions(startDate,endDate).subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.transactions = body.transactions;
        this.isLoaded = true;
        loading.dismiss();
      }, (err) => {
        let alert = this.alertCtrl.create({
          title : "Error "+err.status,
          subTitle: "Cannot retrieve transaction data",
          buttons: ['OK']
          });
          alert.present(prompt);
          loading.dismiss();
      });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Transaction');
  }

  getTransactions() {

  	let loading = this.loadingCtrl.create({content : "Please wait..."});
    loading.present();

  	var currentDate = new Date();
  	var endDate = this.helper.dateQuery(currentDate);
    var startDate = this.helper.dateQuery(currentDate,0,this.period);

    if(this.period==99) {
    	endDate = "2099-01-01 00:00:00";
    	startDate = "2014-01-01 00:00:00";
    }
    console.log(startDate);
    console.log(endDate);

  	this.api.getTransactions(startDate,endDate).subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.transactions = body.transactions;
        loading.dismiss();
      }, (err) => {
        
      });

  }
  
  pushShow(t) {
    this.navCtrl.push('ShowPage', {
      transaction : t
    });
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  back() {
    this.navCtrl.setRoot('AccountPage');
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
