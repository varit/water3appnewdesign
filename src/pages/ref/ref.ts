import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Platform, MenuController, AlertController ,LoadingController} from 'ionic-angular';
import { Api } from '../../services/api';

import { Events } from 'ionic-angular';
/**

/**
 * Generated class for the AccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage({
  segment: 'ref/:userID'
 })
@Component({
  selector: 'page-account',
  templateUrl: 'ref.html',
})
export class RefPage {

  userID;

  constructor(public navCtrl: NavController,public platform:Platform,public event:Events,public alertCtrl:AlertController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    this.userID = this.navParams.get('userID');
    if(this.userID == null ) {
      this.navCtrl.setRoot('HelloIonicPage');
    }
  }

}
