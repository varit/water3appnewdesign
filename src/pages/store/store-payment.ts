import { Component } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , MenuController, AlertController} from 'ionic-angular';
import { Api } from '../../services/api';

declare var Stripe: any;
/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-address',
  templateUrl: 'store-payment.html',
})
export class StorePaymentPage {
  isLoaded = false;
  accountInfo;
  card;
  amount;
  save = false;
  stripe_pk;
  
  itemList: Array<{id:any , qty:any, isAddedToAccount:any}> = [];

  Stripe_token;
  Stripe_param = {name: '', number: '', exp_month:'', exp_year:'', cvc:''};

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public alertCtrl:AlertController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    if(this.api.checkSession()) {
      this.amount = localStorage.getItem('cartAmount');
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      if(localStorage.getItem("card_id") != null || localStorage.getItem("card_id") != "")
      this.api.getUserCreditCards().subscribe(res => {

        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        body.cards.forEach(element => {
          if(element.stripe_card_id == localStorage.getItem("card_id")) {
              this.card = element;
              console.log(this.card.stripe_card_id);
          }
        });
        this.isLoaded = true;
        loading.dismiss();
      }, (err) => {       
      });
    this.stripe_pk = this.api.getStripePK();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StorePage - payment');
    Stripe.setPublishableKey(this.stripe_pk);
  }

  postPayment() {
    console.log(this.Stripe_param);
    console.log(this.save);
    if(!this.card){
              if(this.Stripe_param.number.length > 16) {
                let alert = this.alertCtrl.create({
                    title : "Validation Error",
                    subTitle: "You credit card number exceeded digit limit",
                    buttons: ['OK']
                  });
                  alert.present(prompt);
              } else if(+this.Stripe_param.exp_month > 12) {
                let alert = this.alertCtrl.create({
                    title : "Date Error",
                    subTitle: "Invalid expiry month",
                    buttons: ['OK']
                  });
                  alert.present(prompt);
              } else if(+this.Stripe_param.exp_year < 2016) {
                let alert = this.alertCtrl.create({
                    title : "Date Error",
                    subTitle: "Invalid expiry year",
                    buttons: ['OK']
                  });
                  alert.present(prompt);
              } else if(+this.Stripe_param.cvc > 999) {
                let alert = this.alertCtrl.create({
                    title : "CVC Error",
                    subTitle: "Incorrect CVC",
                    buttons: ['OK']
                  });
                  alert.present(prompt);
              } else {

                  Stripe.card.createToken({
                  name : this.Stripe_param.name,
                  number : this.Stripe_param.number,
                  exp_month : this.Stripe_param.exp_month,
                  exp_year : this.Stripe_param.exp_year,
                  cvc : this.Stripe_param.cvc

                }, (status, response) => this.stripeResponseHandler(status,response));
              }
    }
    else {
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();

      for(var i=0;i<99;i++) {
        if(localStorage.getItem(i.toString()) != null) {
          let item = {
            id : i,
            qty : parseInt(localStorage.getItem(i.toString())),
            isAddedToAccount : false
          };

          this.itemList.push(item);
        }
      }

      let data = {
        payment : {
          source : this.card.stripe_card_id,
          amount : localStorage.getItem("cartAmount"),
          currency : "AUD",
          description : "Payment for online store",
          ip : "127.0.0.1",
        },
        order : {
          items : this.itemList 
        }
      };

      console.log(data);

      this.api.postOrder(data).subscribe(res => {
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));

        loading.dismiss();
        this.navCtrl.setRoot('StoreConfirmPage');
      }, (err) => {    
        console.log(err);  
        loading.dismiss();
      });
    }
  }

  stripeResponseHandler(status,response) {
    if(response.error) {
      let alert = this.alertCtrl.create({
        title : "Payment Error",
        subTitle: response.error.message,
        buttons: ['OK']
        });
      alert.present(prompt);
    } else {
      var token = response.id;
      //this.Stripe_token = response.id;
      console.log(token);

      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();

      for(var i=0;i<99;i++) {
        if(localStorage.getItem(i.toString()) != null) {
          let item = {
            id : i,
            qty : parseInt(localStorage.getItem(i.toString())),
            isAddedToAccount : false
          };

          this.itemList.push(item);
        }
      }

      let data = {
        payment : {
          source : token,
          amount : localStorage.getItem("cartAmount"),
          currency : "AUD",
          description : "Payment for online store",
          ip : "127.0.0.1",
          save_card : "false"
        },
        order : {
          items : this.itemList 
        }
      };

      this.api.postOrder(data).subscribe(res => {
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));

        loading.dismiss();
        this.navCtrl.setRoot('StoreConfirmPage');
      }, (err) => {    
        console.log(err);  
        loading.dismiss();

        let alert = this.alertCtrl.create({
        title : "Payment Error",
        subTitle: "There was an error. Your card has not been charged. Please contact our support",
        buttons: ['OK']
        });
      alert.present(prompt);

      });
    }
  }

  back() {
    this.navCtrl.setRoot('StorePaymentMethodPage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
