import { Component , ViewChild} from '@angular/core';
import { NavController, PopoverController, AlertController, IonicPage , Content, Platform, MenuController} from 'ionic-angular';
import { Api } from '../../services/api';
import { ENV } from '../../services/env';


//import { HTTP } from '@ionic-native/http';

@IonicPage({
  segment : 'store'
})
@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'store.html'
})
export class StorePage {
  @ViewChild(Content) content: Content;
  token : string;
  message  : string;
  displayLog : string;
  products;
  itemAmount;
  itemID="1";
  isLoaded = false;
  constructor(public navCtrl: NavController,public popCtrl: PopoverController, public alertCtrl:AlertController, public platform:Platform,public env:ENV, public api: Api, public menuCtrl: MenuController) {

    this.itemAmount = localStorage.getItem("itemAmount");
    if(this.itemAmount == null || this.itemAmount <= 0)
      this.itemAmount = 0;
    else
      this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
    console.log(this.itemAmount);

    this.api.getProducts().subscribe(res => {
      var body = res.json();
      console.log("Response : "+status+" BODY "+JSON.stringify(body));
      this.products = body;
      this.isLoaded=true;
    }, (err) => {

    });

  }


  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

  addToBag() {
      console.log("Added id "+this.itemID);
      if(localStorage.getItem(this.itemID)==null) {
        localStorage.setItem(this.itemID,"1");
        console.log("item id "+this.itemID+" is qty "+localStorage.getItem(this.itemID));
        var amount = this.itemAmount + 1;
        this.itemAmount = amount;
        localStorage.setItem("itemAmount",amount.toString());
      }
      else {
        var temp = parseInt(localStorage.getItem(this.itemID));
        temp = temp+1;
        localStorage.setItem(this.itemID,temp.toString());
        console.log("item id "+this.itemID+" is qty "+localStorage.getItem(this.itemID));
        var amount = this.itemAmount + 1;
        this.itemAmount = amount;
        localStorage.setItem("itemAmount",amount.toString());
      }

      let alert = this.alertCtrl.create({
            title : "Added to cart",
            subTitle: "You have added this item to your cart",
            buttons: ['OK']
            });
      alert.present(prompt);
  }

  toKiosk(){
    this.navCtrl.setRoot('KioskPage');
  }

  scrollTo() {
    this.content.scrollTo(0,2500,1500);
  }

}

