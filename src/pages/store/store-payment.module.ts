import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StorePaymentPage } from './store-payment';

@NgModule({
  declarations: [
    StorePaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(StorePaymentPage),
  ],
  exports: [
    StorePaymentPage
  ]
})
export class StorePaymentPageModule {}
