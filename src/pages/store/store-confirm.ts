import { Component } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';

/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-topup',
  templateUrl: 'store-confirm.html',
})
export class StoreConfirmPage {
  isLoaded = false;
  balance;
  amount;
  email;

  itemAmount;

  constructor(public navCtrl: NavController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
  	if(this.api.checkSession()) {
        let loading = this.loadingCtrl.create({content : "Please wait..."});
        loading.present();
        this.api.getAccountInfo().subscribe(res => {
        var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.balance = body.regular_credit_balance + body.subscription_credit_balance;
        this.balance = this.balance.toFixed(2);
        this.amount = localStorage.getItem("cartAmount");
        this.email = localStorage.getItem('email');

        this.itemAmount = localStorage.getItem("itemAmount");
        if(this.itemAmount == null || this.itemAmount <= 0)
          this.itemAmount = 0;
        else
          this.itemAmount = parseInt(localStorage.getItem("itemAmount"));

        for(var i=0; i<99 ; i++) {
          localStorage.removeItem(i.toString());
        }

        localStorage.setItem("itemAmount","0");

        this.isLoaded = true;
        loading.dismiss();

      }, (err) => {
        
      });
  	}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopupPage');
  }

  toFindKiosk() {
    this.navCtrl.setRoot('KioskPage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }
}
