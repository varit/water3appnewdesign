import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StoreConfirmPage } from './store-confirm';

@NgModule({
  declarations: [
    StoreConfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(StoreConfirmPage),
  ],
  exports: [
    StoreConfirmPage
  ]
})
export class StoreConfirmPageModule {}
