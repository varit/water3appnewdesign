import { Component , ViewChild} from '@angular/core';
import { NavController, PopoverController, IonicPage , Content, Platform, Events, MenuController} from 'ionic-angular';
import { Api } from '../../services/api';
import { ENV } from '../../services/env';


//import { HTTP } from '@ionic-native/http';

@IonicPage({
  segment : 'cart'
})
@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'cart.html'
})
export class CartPage {
  @ViewChild(Content) content: Content;
  token : string;
  message  : string;
  displayLog : string;

  itemList: Array<{id:any , type:any, model:any, price:any , qty:any}> = [];

  totalPrice=0;

  isLoggedin;
  itemAmount;

  isLoaded=false;

  constructor(public navCtrl: NavController,public event:Events, public popCtrl: PopoverController, public platform:Platform,public env:ENV, public api: Api, public menuCtrl: MenuController) {

    this.message = "Waiting command";
    this.token = "0";
    this.displayLog = "0";

    if(localStorage.getItem("isLoggedin")=="1")
      this.isLoggedin = true;
    else
      this.isLoggedin = false;

    this.itemAmount = localStorage.getItem("itemAmount");
    if(this.itemAmount == null || this.itemAmount <= 0)
      this.itemAmount = 0;
    else
      this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
    console.log(this.itemAmount);

    for(var i=0 ; i<99 ; i++) {
      if(localStorage.getItem(i.toString())!=null) {
        var item = i;
        this.api.getProductsByID(item).subscribe(res => {
          
          var body = res.json();
          console.log(body);
          var item_id = body.id;
          var item_qty = localStorage.getItem(item_id.toString());
          var item_type = body.type;
          var item_model = body.model;
          var item_price = body.salePrice;
          console.log("Getting item id ",item_id);
          this.totalPrice = this.totalPrice+(item_price*parseInt(item_qty));
          console.log("Total ",this.totalPrice);
          localStorage.setItem("cartAmount",this.totalPrice.toString());

          let new_item = {
            id : item_id.toString(),
            type : item_type,
            model : item_model,
            price : item_price,
            qty : item_qty
          };
          this.itemList.push(new_item);
          console.log("Cart item id "+item_id+" is qty "+item_qty);

        }, (err) => {

        });
      }
    }
    console.log(this.itemList);
    this.isLoaded = true;

  }

  removeItem(itemID:string){
    
    var newqty = parseInt(localStorage.getItem("itemAmount")) - parseInt(localStorage.getItem(itemID));

    console.log("newqty = "+parseInt(localStorage.getItem("itemAmount"))+" - "+localStorage.getItem(itemID));

    localStorage.setItem("itemAmount",newqty.toString());
    localStorage.removeItem(itemID);
    console.log("Removed ",itemID);
    this.event.publish('refreshPage',"true");
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

  toKiosk(){
    this.navCtrl.setRoot('KioskPage');
  }

  scrollTo() {
    this.content.scrollTo(0,2500,1500);
  }

}

