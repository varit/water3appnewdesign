import { Component } from '@angular/core';
import { IonicPage, LoadingController , AlertController, NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';
/**
 * Generated class for the TopupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-address',
  templateUrl: 'shipping.html',
})
export class ShippingPage {
  isLoaded = false;
  accountInfo;
  state;
  country;

  constructor(public navCtrl: NavController,public alertCtrl:AlertController, public loadingCtrl:LoadingController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    if(this.api.checkSession()) {
      let loading = this.loadingCtrl.create({content : "Please wait..."});
      loading.present();
      this.api.getShippingAddress().subscribe(res => {
        var status = res.status;
        var body = res.json();
        //console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.accountInfo = body;

        this.api.getCountry().subscribe(res => {
          var status = res.status;
          var body = res.json();
          //console.log("Response : "+status+" BODY "+JSON.stringify(body));
          this.country = body.countries;

          this.api.getState(this.accountInfo.country_id).subscribe(res => {
            var status = res.status;
            var body = res.json();
            //console.log("Response : "+status+" BODY "+JSON.stringify(body));
            this.state = body.states;

            loading.dismiss();
            this.isLoaded=true;
          }, (err) => {
          });            
        }, (err) => {
        });        
      }, (err) => {       
      });   
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopupPage - Address');
  }
  
  postAddress() {

    if(this.accountInfo.address_1 == "" || this.accountInfo.address_1 == null || this.accountInfo.city == "" || this.accountInfo.city == null || this.accountInfo.postcode == "" || this.accountInfo.postcode == null) {
      let alert = this.alertCtrl.create({
            title : "Error",
            subTitle: "Please check that all the fields are filled and try again.",
            buttons: ['OK']
          });
      alert.present(prompt);
    }
    else {

        this.api.updateShippingAddress(this.accountInfo).subscribe(res => {
            var status = res.status;
            var body = res.json();
            //console.log("Response : "+status+" BODY "+JSON.stringify(body));
            this.navCtrl.setRoot('StorePaymentMethodPage');
          }, (err) => {
          });
    }
  }

  sameAddress(){
      this.navCtrl.setRoot('StorePaymentMethodPage'); 
  }

  updateState() {

    this.api.getState(this.accountInfo.country_id).subscribe(res => {
            //var status = res.status;
            var body = res.json();

            var found;
            //console.log("Response : "+status+" BODY "+JSON.stringify(body));
            this.state = body.states;
            if(this.state != null && this.state != "") {

              //this.accountInfo.state_id = this.state[0].id;

              //console.log("check state s="+s.id+" Saved ID="+this.accountInfo.state_id);
              for( let s of this.state ) {
                console.log("check state s="+s.id+" Saved ID="+this.accountInfo.state_id);
                if(s.id == this.accountInfo.state_id) {
                  console.log("break at id "+s.id+" "+this.accountInfo.state_id);
                  found = s.id;
                  break;
                }
              }
              if(found!="" && found != null) {
                this.accountInfo.state_id = found;
              }

            } else { 
              this.accountInfo.state_id = 0;
              console.log("Reset State");
            }
          }, (err) => {
          });

  }

    stateName(state_id) {
    for(let s of this.state) {
      if(s.id == this.accountInfo.state_id) {
        return s.name;
      }
    }
  }

  countryName(country_id) {
    for(let c of this.country) {
      if(c.id == this.accountInfo.country_id) {
        return c.name;
      }
    }
  }

  back() {
    this.navCtrl.setRoot('AmountPage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }

}
