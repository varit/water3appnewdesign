import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StorePaymentMethodPage } from './store-payment-method';

@NgModule({
  declarations: [
    StorePaymentMethodPage,
  ],
  imports: [
    IonicPageModule.forChild(StorePaymentMethodPage),
  ],
  exports: [
    StorePaymentMethodPage
  ]
})
export class StorePaymentMethodModule {}
