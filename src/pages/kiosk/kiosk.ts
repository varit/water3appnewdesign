import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, LoadingController , AlertController, NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';

import { Geolocation } from '@ionic-native/geolocation';

declare var google;

/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-kiosk',
  templateUrl: 'kiosk.html',
})
export class KioskPage {

  latitude;
  longitude;
  Geo;
  keyword = "";
  action = "index";

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  markers = [];

  isLoaded = false;

  itemAmount;

  constructor(public navCtrl: NavController,public alertCtrl:AlertController, public loadingCtrl:LoadingController, public geolocation:Geolocation,public api:Api, public navParams: NavParams, public menuCtrl:MenuController) {
    this.latitude = 1;

     this.itemAmount = localStorage.getItem("itemAmount");
    if(this.itemAmount == null || this.itemAmount <= 0)
      this.itemAmount = 0;
    else
      this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
    let loading = this.loadingCtrl.create({content : "Please wait..."});
    loading.present();
    this.geolocation.getCurrentPosition().then( (resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;

      let position = new google.maps.LatLng(this.latitude,this.longitude);
      let mapOptions = {
        center : position,
        zoom : 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      this.map = new google.maps.Map(document.getElementById("map"),mapOptions);

      var userImage = "assets/images/marker-user.png";
    //var userLocation = {lat: this.latitude, lng: this.longitude };
      let user = new google.maps.LatLng(this.latitude,this.longitude);
      let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: user,
          icon : userImage
      });
      this.isLoaded=true;
      loading.dismiss();
    }).catch((error) => {

      console.log('Error getting location', error);

      let alert = this.alertCtrl.create({
            title : "Error getting location",
            subTitle: error.message,
            buttons: ['OK']
            });
      alert.present(prompt);

      let position = new google.maps.LatLng(-27.560102,152.988739);
      let mapOptions = {
        center : position,
        zoom : 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      this.map = new google.maps.Map(document.getElementById("map"),mapOptions);
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KioskPage');
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {
    this.navCtrl.setRoot('TopupPage');
  }

  postSearch() {
    let loading = this.loadingCtrl.create({content : "Please wait..."});
    loading.present();
    this.geolocation.getCurrentPosition().then( (resp) => {

    this.latitude = resp.coords.latitude;
    this.longitude = resp.coords.longitude;
    console.log("Lat",this.latitude);
    console.log("Long",this.longitude);

    var login = localStorage.getItem("isLoggedin");
    if(login!="1") {

      this.api.AdminLogin().subscribe(res => {
        var body = res.json();
        localStorage.setItem("token",body.access_token);

        this.api.findKiosk(this.latitude,this.longitude,this.keyword,70000000).subscribe(res => {
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.navCtrl.setRoot('KioskSearchPage', {
          kiosks : body.data,
          lat : this.latitude,
          long : this.longitude
        });
        loading.dismiss();
        });
      });

    }
    else{
      this.api.findKiosk(this.latitude,this.longitude,this.keyword,70000000).subscribe(res => {
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.navCtrl.setRoot('KioskSearchPage', {
          kiosks : body.data,
          lat : this.latitude,
          long : this.longitude
        });
        loading.dismiss();
        });
    }

  }).catch((error) => {
    console.log('Error getting location', error);

    let alert = this.alertCtrl.create({
          title : "Error getting location",
          subTitle: error.message,
          buttons: ['OK']
          });
    alert.present(prompt);
    
    var login = localStorage.getItem("isLoggedin");
    if(login!="1") {

      this.api.AdminLogin().subscribe(res => {
        var body = res.json();
        localStorage.setItem("token",body.access_token);

        this.api.findKiosk(152.988739,-27.560102,this.keyword,70000000).subscribe(res => {
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.navCtrl.setRoot('KioskSearchPage', {
          kiosks : body.data,
          lat : 1,
          long : 1
        });
        loading.dismiss();
        });
      });

    }
    else{
      this.api.findKiosk(152.988739,-27.560102,this.keyword,70000000).subscribe(res => {
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        this.navCtrl.setRoot('KioskSearchPage', {
          kiosks : body.data,
          lat : 1,
          long : 1
        });
        loading.dismiss();
        }, (err) => {
          loading.dismiss();
        });
    }


  });
  }

}
