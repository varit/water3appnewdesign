import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KioskSearchPage } from './search';

@NgModule({
  declarations: [
    KioskSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(KioskSearchPage),
  ],
  exports: [
    KioskSearchPage
  ]
})
export class KioskSearchPageModule {}
