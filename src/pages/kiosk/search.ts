import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , MenuController} from 'ionic-angular';
import { Api } from '../../services/api';

import { Geolocation } from '@ionic-native/geolocation';

declare var google;
/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-kiosk',
  templateUrl: 'search.html',
})
export class KioskSearchPage {

  kiosks;
  latitude;
  longitude;
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  markers = [];
  isLoaded = false;

  constructor(public navCtrl: NavController,public geo:Geolocation, public loadingCtrl:LoadingController, public geolocation:Geolocation,public api:Api, public navParams: NavParams, public menuCtrl:MenuController) {
    this.kiosks = navParams.get('kiosks');
    this.latitude = navParams.get('lat');
    this.longitude = navParams.get('long');
    console.log("Search Lat : " + this.latitude);
    console.log("Search Long : " + this.longitude);
  }

  ionViewDidLoad() {
    if(this.latitude==null || this.latitude=="" || this.longitude==null || this.longitude==""){
      this.navCtrl.setRoot('KioskPage'); 
    }
    else {
    console.log('ionViewDidLoad KioskSearch');
    this.loadMap();
    }
  }

  loadMap() {
    let loading = this.loadingCtrl.create({content : "Please wait..."});
    loading.present();

    


    if(this.latitude==1 && this.longitude==1){
      let position = new google.maps.LatLng(-27.560102,152.988739);
      let mapOptions = {
        center : position,
        zoom : 6,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
          this.map = new google.maps.Map(document.getElementById("map"),mapOptions);
    }
    else {
      let position = new google.maps.LatLng(this.latitude,this.longitude);
      let mapOptions = {
        center : position,
        zoom : 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
          this.map = new google.maps.Map(document.getElementById("map"),mapOptions);
    }

    if(this.latitude==1 && this.longitude==1){
    }
    else {
      var userImage = "assets/img/marker-user.png";
    //var userLocation = {lat: this.latitude, lng: this.longitude };
    let position = new google.maps.LatLng(this.latitude,this.longitude);
    let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: position,
        icon : userImage
      });
    }

    for(let kiosk of this.kiosks) {

      var location = {lat: kiosk.latitude, lng: kiosk.longitude };
      var image = "assets/img/kiosk_marker.png";

      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: location,
        title : kiosk.location_name,
        icon : image
      });

      let contentStr = "<h3>"+kiosk.location_name+"</h3><p>"+kiosk.address_1+"</p><p>"+kiosk.address_2+"</p><p>"+kiosk.city+", "+kiosk.state+"</p><p>"+kiosk.postcode+"</p>";          

      var info = new google.maps.InfoWindow();

      marker.addListener('click', () => {
        info.setContent(contentStr);
        info.open(this.map,marker);
      });

      this.markers.push([kiosk.kiosk_serial_no,marker]);
      //console.log("pushed marker : ",kiosk.kiosk_serial_no);
    }
    this.isLoaded = true;

    loading.dismiss();
  }

  moveMap(serial,lat,long) {

    for (var i = 0; i < this.markers.length; i++) {
          //console.log("Stopping marker :",this.markers[i][0]);
          this.markers[i][1].setAnimation(null);
    }

    let position = new google.maps.LatLng(lat,long);
    this.map.panTo(position);
    this.map.setZoom(16);

    for (var i = 0; i < this.markers.length; i++) {
      //console.log("Checking marker :",this.markers[i][0]);
      if(this.markers[i][0]==serial) {
        //console.log("Starting marker :",this.markers[i][0]);
        this.markers[i][1].setAnimation(google.maps.Animation.BOUNCE);
      }
    }
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

  toBottle() {
    this.navCtrl.setRoot('TopupPage');
  }

}
