import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, MenuController } from 'ionic-angular';

/**
 * Generated class for the FaqsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-faqs',
  templateUrl: 'faqs.html',
})
export class FaqsPage {

  itemAmount;

  constructor(public navCtrl: NavController,public platform:Platform, public navParams: NavParams,public menuCtrl:MenuController) {
     this.itemAmount = localStorage.getItem("itemAmount");
    if(this.itemAmount == null || this.itemAmount <= 0)
      this.itemAmount = 0;
    else
      this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqsPage');
  }

    openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle();
  }

}
