import { Component } from '@angular/core';
import { IonicPage, LoadingController , NavController, NavParams , AlertController, MenuController } from 'ionic-angular';


import { Api } from '../../services/api';

/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 @IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'keypad.html',
})
export class KeypadPage {

  step;
  code;
  first;
  second;
  third;
  forth;

constructor(public navCtrl: NavController, public loadingCtrl:LoadingController,public alertCtrl:AlertController, public navParams: NavParams, public api:Api, public menuCtrl:MenuController) {
    this.step = 1;
}


checkCode() {
  
}

input(n) {
  console.log("input at ",this.step);
  if(this.step == 1 && n!=99) {
    this.first = n;
    this.step++;
  }
  else if(this.step == 2  && n!=99) {
    this.second = n;
    this.step++;
  }
  else if(this.step == 3  && n!=99) {
    this.third = n;
    this.step++;
  }
  else if(this.step == 4  && n!=99) {
    this.forth = n;
    this.step++;
  }
  else if(n == 99) {
    if(this.step == 2) {
      this.first = null;
      this.step = 1;
    }
    if(this.step == 3) {
      this.second = null;
      this.step = 2;
    }
    if(this.step == 4) {
      this.third = null;
      this.step = 3;
    }
  }

  if(this.step == 5) {
    this.checkCode();
  }
}



openMenu() {
  //push another page onto the history stack
  //causing the nav controller to animate the new page in
  this.menuCtrl.toggle();
}

}
