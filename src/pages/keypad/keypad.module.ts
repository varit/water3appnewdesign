import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KeypadPage } from './keypad';

@NgModule({
  declarations: [
    KeypadPage,
  ],
  imports: [
    IonicPageModule.forChild(KeypadPage),
  ],
  exports: [
    KeypadPage
  ]
})
export class KeypadPageModule {}
