import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController, Platform, AlertController ,LoadingController} from 'ionic-angular';
import { Facebook , FacebookLoginResponse } from '@ionic-native/facebook';
import { Api } from '../../services/api';

import { Events } from 'ionic-angular';
/**
 * Generated class for the ContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  token: string;
  email: string;
  password: string;

  remember = false;

  itemAmount;

  fbEmail;
  fbFirstName;
  fbLastName;
  fbID;
  fbToken;

  constructor(public navCtrl: NavController,public platform:Platform, public fb:Facebook,public loadingCtrl:LoadingController, public alertCtrl:AlertController, public navParams: NavParams, public menuCtrl:MenuController, public api:Api, public event:Events) {
    console.log("LoginController:: construct");
    this.token=localStorage.getItem("token");
    console.log("Token ="+this.token);

     this.itemAmount = localStorage.getItem("itemAmount");
    if(this.itemAmount == null || this.itemAmount <= 0)
      this.itemAmount = 0;
    else
      this.itemAmount = parseInt(localStorage.getItem("itemAmount"));
  }

  openMenu() {
    //push another page onto the history stack
    //causing the nav controller to animate the new page in
    this.menuCtrl.toggle(); 
  }

  submitLogin() {

    //console.log("Login details : "+this.email+":"+this.password);

    localStorage.setItem('email',this.email);
    localStorage.setItem('password',this.password);
    let loading = this.loadingCtrl.create({content : "Loggin in"});
    loading.present();
    this.api.authenticate().subscribe(res => {
        //var status = res.status;
        var body = res.json();
        console.log("Response : "+status+" BODY "+JSON.stringify(body));
        localStorage.setItem("token",body.access_token);
        if(this.remember == true) {

          console.log("remember me");
          localStorage.setItem("refresh",body.refresh_token);
        }
        localStorage.setItem("fullname",body.first_name+" "+body.last_name);
        localStorage.setItem("username",body.username);
        localStorage.setItem("isLoggedin","1");
        var expiry = new Date();
        localStorage.setItem("expiry",new Date(expiry.getTime() + (this.api.expiry*1000*60)).toString());
        this.event.publish('tokenUpdated',body.access_token);
        this.event.publish('isLoggedIn',"true");
        console.log("Token = "+localStorage.getItem('token'));
        console.log("Refresher = "+localStorage.getItem('refresh'));
        console.log("Registered = "+expiry.toString());
        console.log("Expire = "+localStorage.getItem('expiry'));
        loading.dismiss();
        this.navCtrl.setRoot('AccountPage');
      }, (err) => {
        console.log("Response : "+err.status+" BODY "+JSON.stringify(err));
        if(err.status=="401" || err.status==401) {
          let alert = this.alertCtrl.create({
          title : "Failure",
          subTitle: "You have entered wrong username and/or password",
          buttons: ['OK']
          });
          alert.present(prompt);
          loading.dismiss();
        }
        else {
          let alert = this.alertCtrl.create({
          title : "Technical Error "+err.status,
          subTitle: "Some technical error has occured : "+err.message,
          buttons: ['OK']
          });
          alert.present(prompt);
          loading.dismiss();
        }
      });

  }

  toRegister() {
    this.navCtrl.setRoot('RegisterPage');
  }

  facebookLogin() {
    this.fb.login(['public_profile','email'])
    .then((res : FacebookLoginResponse) => {
      //console.log('Facebook!',res);
      this.fbToken = res.authResponse.accessToken;
      console.log("UserID",res.authResponse.userID);
      this.fb.api("/me?fields=id,email,first_name,last_name", ['public_profile','email'])
      .then((res) => { 
        //console.log(res); 
        this.fbEmail = res.email;
        localStorage.setItem("email",this.fbEmail);
        localStorage.setItem("facebook","true");
        this.fbID = res.id;
        this.fbFirstName = res.first_name;
        this.fbLastName = res.last_name;
        this.api.AdminLogin().subscribe(res => {
          var body = res.json();
          localStorage.setItem("token",body.access_token);
          this.api.facebook(this.fbEmail,this.fbFirstName,this.fbLastName,this.fbID,this.fbToken).subscribe(res => {
            var body = res.json();
            //console.log("Response : "+status+" BODY "+JSON.stringify(body));
            localStorage.setItem("token",body.access_token);
            localStorage.setItem("fullname",this.fbFirstName+" "+this.fbLastName);
            localStorage.setItem("username",body.username);
            localStorage.setItem("isLoggedin","1");
            var expiry = new Date();
            localStorage.setItem("expiry",new Date(expiry.getTime() + (this.api.expiry*1000*60)).toString());
            this.event.publish('tokenUpdated',body.access_token);
            this.event.publish('isLoggedIn',"true");
            console.log("Token = "+localStorage.getItem('token'));
            console.log("Registered = "+expiry.toString());
            console.log("Expire = "+localStorage.getItem('expiry'));
            this.navCtrl.setRoot('AccountPage');
          });
        });
      });
    })
    .catch(e => {
      let alert = this.alertCtrl.create({
          title : "Facebook Error",
          subTitle: "We could not conenct to Facebook at the moment, please try again later. ("+e.message+")",
          buttons: ['OK']
          });
      alert.present(prompt);
    });
  }

    toBottle() {

    this.navCtrl.setRoot('TopupPage');

  }


}
