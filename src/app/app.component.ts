import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, LoadingController, Nav, AlertController } from 'ionic-angular';

// import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
//import { AboutPage } from '../pages/about/about';
//import { FaqsPage } from '../pages/faqs/faqs';
//import { BottlecarePage} from '../pages/bottlecare/bottlecare';
//import { AccountPage } from '../pages/account/account';
// import { TermsPage } from '../pages/terms/terms';
// import { PrivacyPage } from '../pages/privacy/privacy';
// import { ContactPage } from '../pages/contact/contact';
// import { LoginPage } from '../pages/login/login';
// import { RegisterPage } from '../pages/register/register';
// import { KioskPage } from '../pages/kiosk/kiosk';
// import { BottlePage } from '../pages/bottle/bottle';
// import { TransactionPage } from '../pages/transaction/transaction';
// import { PaymentsPage } from '../pages/payment/payment';
// import { TopupPage } from '../pages/topup/topup';
// import { SubscriptionPage } from '../pages/subscription/subscription';

import { Api } from '../services/api'

import { StatusBar } from '@ionic-native/status-bar';
import { NFC , Ndef } from '@ionic-native/nfc';

import { Events } from 'ionic-angular';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  token:string;
  isLogin:boolean;
  rootPage = 'HelloIonicPage';
  pages: Array<{title: string, component: any, class:any}>;
  bottle_no;

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public event: Events,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public api: Api,
    public nfc:NFC
    ) {
    this.initializeApp();

    event.subscribe('refreshPage', token =>{
      //console.log("Reloading :",this.nav.getActive().component);
        this.nav.setRoot(this.nav.getActive().component);
    });

    event.subscribe('isLoggedin', token =>{
      //console.log("Reloading :",this.nav.getActive().component);
        this.isLogin = true;
        this.setFullMenu();
    });

    event.subscribe('logout', token =>{
      //console.log("Reloading :",this.nav.getActive().component);
        this.logout();

    });

    if(localStorage.getItem("isLoggedin")=="1")
    this.isLogin = true;
    this.setHalfMenu();

    event.subscribe('expire', token =>{
        let alert = this.alertCtrl.create({
          title : "Please Login",
          subTitle: "You have not logged in or your previous session is expired",
          buttons: ['OK']
        });
        alert.present(prompt);
        this.isLogin = false;
        this.setHalfMenu();
        this.nav.setRoot('LoginPage');
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {

      if(this.platform.is('ios')) {
        console.log("Platforn : iOS");
      } else if(this.platform.is('android')) {
        console.log("Platforn : android ");
      } else {
        console.log("Platforn : "+this.platform.platforms());
      }
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.nfc.addTagDiscoveredListener(() => {
          console.log('successfully attached tag listener');
      }, (err) => {
          console.log('error attaching tag listener', err);
      }).subscribe((event) => {
        let loading = this.loadingCtrl.create({content : "Receiving NFC data"});
        loading.present();
        let tag = this.nfc.bytesToHexString(event.tag.id);

        this.api.decodeBottleTag(tag).subscribe(res => {

              var body = res.json();
              this.bottle_no = body.bottle_no;
              this.api.getBottle(body.bottle_no).subscribe(res => {
                let body = res.json();
                let alert = this.alertCtrl.create({
                  title : "Bottle Already Registered",
                  subTitle: "This bottle is already registered under your account. ("+body.bottle_no+")",
                  buttons: ['OK']
                });
                alert.present(prompt);
              }, (err) => {
                if(err.status == 404) {
                  this.nav.setRoot('BottleCreatePage', {
                    bottle_no : this.bottle_no
                  });
                }
                if(err.status == 401) {
                  let alert = this.alertCtrl.create({
                  title : ":login Required",
                  subTitle: "You have to login before registering your bottle",
                  buttons: ['OK']
                  });
                  alert.present(prompt);
                }
              });

        }, (err) => {
            let alert = this.alertCtrl.create({
              title : "Error",
              subTitle: "Tag data does not exist.",
              buttons: ['OK']
            });
            alert.present(prompt);
        });

      }, (err) => {

      });


      this.statusBar.styleDefault();
      //console.log("Water3 app version 1.0.3");
    });


  }

  setFullMenu() {
      this.pages = [
      { title: 'Home', component: 'HelloIonicPage', class:'icon icon-house'},
      { title: 'My Account', component: 'AccountPage', class:'icon icon-account'},
      { title: 'Shop', component: 'StorePage', class:'icon icon-transaction'},
      { title: 'Find Kiosks', component: 'KioskPage', class:'icon icon-location' },
      { title: 'Why Water3', component: 'WhyPage', class:'icon icon-information'},
      { title: 'FAQ', component: 'FaqsPage', class:'icon icon-information'},
      { title: 'Contact', component: 'ContactPage', class:'icon icon-phone'}
    ];

  }

  setHalfMenu() {
      this.pages = [
      { title: 'Home', component: 'HelloIonicPage', class:'icon icon-house'},
      { title: 'Shop', component: 'StorePage', class:'icon icon-transaction'},
      { title: 'Find Kiosks', component: 'KioskPage', class:'icon icon-location' },
      { title: 'Why Water3', component: 'WhyPage', class:'icon icon-information'},
      { title: 'FAQ', component: 'FaqsPage', class:'icon icon-information'},
      { title: 'Contact', component: 'ContactPage', class:'icon icon-phone'}
    ];

  }


  logout() {
    this.isLogin = false;
    localStorage.setItem("token","");
    localStorage.setItem("isLoggedin","0");
    localStorage.clear();
    this.event.publish('tokenUpdated',localStorage.getItem('token'));
    let alert = this.alertCtrl.create({
          title : "Logged out",
          subTitle: "You have successfully logged out",
          buttons: ['OK']
        });
    alert.present(prompt);
    this.nav.setRoot('HelloIonicPage');
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }


  goToLogin() {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot('LoginPage');
  }

  goToRegister() {
    this.menu.close();
    this.nav.setRoot('RegisterPage', {
      'referral' : 'standard'
    });
  }
}
