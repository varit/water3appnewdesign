import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule, ErrorHandler, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { Api } from '../services/api';
import { ENV } from '../services/env';
import { Helper } from '../services/helper';

import { ItemDetailsPage } from '../pages/item-details/item-details';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { Geolocation } from '@ionic-native/geolocation';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { NFC , Ndef } from '@ionic-native/nfc';
import { Clipboard } from '@ionic-native/clipboard';

//import { HTTP } from '@ionic-native/http';

@NgModule({
  declarations: [
    MyApp,
    ItemDetailsPage,
    ListPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    FormsModule,
    CommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ItemDetailsPage,
    ListPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    UniqueDeviceID,
    Geolocation,
    Api,
    ENV,
    Helper,
    Facebook,
    NFC,
    Ndef,
    Clipboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}